#include "EventTest.h"

EventTest::EventTest()
{
	setNumOfTests( 2 );

	//
	_events.addListener( "event1",&_listener1 );
	_events.addListener( "event2",&_listener1 );
	_events.addListener( "event1",&_listener2 );

	_listener1.setInstance( this );
	_listener2.setInstance( this );
}

void EventTest::_eventCallBackTest()
{
	Debug::getInstance() -> log (UNITTEST, "Event call back test." );

	_events.dispatch( &_event1 );
	_events.dispatch( &_event2 );
}

void EventTest::_removeListenerTest()
{
	Debug::getInstance() -> log( UNITTEST, "Remove listener test." );

	_events.removeListener( "event1", &_listener1 );
	_events.dispatch( &_event1 );
	_events.dispatch( &_event2 );
}


void EventTest::setup()
{
	registerTest( static_cast< Func >( &EventTest::_eventCallBackTest ) );
	registerTest( static_cast< Func >( &EventTest::_removeListenerTest ) );
}
