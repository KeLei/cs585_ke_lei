#include "HashmapTest.h"

HashmapTest::HashmapTest()
{
	setNumOfTests( 1 );
}

HashmapTest::~HashmapTest(){}

void HashmapTest::_storeRetriveTest()
{
	std::cout << std::endl << "Store and retrive test." << std::endl;

	_map.insert( "health", 100 );
	_pass = _map[ "health" ] == 100;
	ASSERT( _pass, "insert health=100 and retrive test", "Fail" );

	_map.insert( "height", 180 );
	_map.insert( "weight", 150 );
	_map.insert( "magic", 300 );
	_map.insert( "armor", 90 );
	_map.insert( "skill", 200 );

	_pass = _map[ "height" ] == 180;
	_pass &= _map[ "weight" ] == 150;
	_pass &= _map[ "magic" ] == 300;
	_pass &= _map[ "armor" ] == 90;
	_pass &= _map[ "skill" ] == 200;
	ASSERT( _pass, "insert multiple keys and values and retrive test", "Fail" );
}

void HashmapTest::setup()
{
	registerTest( static_cast< Func >( &HashmapTest::_storeRetriveTest ) );
}