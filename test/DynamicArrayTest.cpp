#include "DynamicArrayTest.h"

DynamicArrayTest::DynamicArrayTest()
{
	setNumOfTests( 4 );
	for( int i = 0; i < 10; i++ )
	{
		_array[ i ] = i;
	}
}

DynamicArrayTest::~DynamicArrayTest() {}

void DynamicArrayTest::_sizeTest()
{
	std::cout << std::endl << "size test." << std::endl;

	_pass = _arrayA.size() == 0;
	ASSERT( _pass, "size retrive test", "size not as excepted" );

	_arrayA.resize( 10, 1 );
	_pass = _arrayA.size() == 10;
	ASSERT( _pass, "Resize test", "size not as excepted" );

	_pass = _arrayA.popBack() == 1;
	ASSERT( _pass, "Resize test && PopBack test", "New elemens are not added to the new spaces in the array" );

}

void DynamicArrayTest::_constructorTest()
{
	std::cout << std::endl << "Consructor test." << std::endl;

	DynamicArray< int > a( _array, sizeof( _array ) / 4 );
	_pass = a.size() == 10;
	ASSERT( _pass, "Construct from an existing array test", "Constructor fail, size mismatch");

	DynamicArray< int > b = a;
	_pass = b.size() == 10;
	ASSERT( _pass, "Copy constructor test", "Constructor fail, size mismatch");

	DynamicArray< int > c( 26 );
	_pass = c.size() == 0 && c.capacity() == 26; 
	ASSERT( _pass, "Construct with defined capacity test", "Constructor fail, size or capacity mismatch");

	DynamicArray< int > d;
	_pass = d.size() == 0 && d.capacity() == 0; 
	ASSERT( _pass, "Empty constructor test", "Constructor fail, size or capacity mismatch");
}

void DynamicArrayTest::_operatorOverloadTest()
{
	std::cout << std::endl << "Operator overload test." << std::endl;

	DynamicArray< int > a( _array, sizeof( _array ) / 4 );
	_arrayA = a;
	_pass = _arrayA.size() == 10;
	ASSERT( _pass, " = operator overload test", "Overload failed");
	_pass = _arrayA[ 5 ] == 5;
	ASSERT( _pass, " [] operator overload test", "Can't get the value in certain index");

	_arrayA[ 3 ] = 5;
	_pass = _arrayA[ 3 ] == 5;
	ASSERT( _pass, " [] operator overload test", "Can't change the value in certain index");
}

void DynamicArrayTest::_operationTest()
{
	std::cout << std::endl << "Operation test." << std::endl;

	_arrayB = _arrayA;
	_arrayA.assign( 6, 12 );
	_pass = _arrayA[ 6 ] == 12;
	ASSERT( _pass, "assign function test", "assign failed");

	_arrayA.pushBack( 30 );
	_pass = _arrayA[ _arrayA.size() - 1 ] == 30;
	ASSERT( _pass, "PushBack function test", "Failed to push into the back of the array");

	_arrayA.pushFront( 100 );
	_pass = _arrayA[ 0 ] == 100;
	ASSERT( _pass, "PushFront function test", "Failed to push into the front of the array");

	_arrayA.insert( 5, 70 );
	_pass = _arrayA[ 5 ] == 70;
	ASSERT( _pass, "Insert function test", "Failed to insert into certain index of the array");

	_arrayA.erase( 5 );
	_pass = _arrayA[ 5 ] != 70;
	ASSERT( _pass, "Erase function test", "Failed to erase from certain index of the array");
}

void DynamicArrayTest::setup()
{
	registerTest( static_cast< Func >( &DynamicArrayTest::_sizeTest ) );
	registerTest( static_cast< Func >( &DynamicArrayTest::_constructorTest ) );
	registerTest( static_cast< Func >( &DynamicArrayTest::_operatorOverloadTest ) );
	registerTest( static_cast< Func >( &DynamicArrayTest::_operationTest ) );
}