#pragma once

#include "../engine/test_suite/suite.h"
#include "../engine/event/Dispatcher.h"
#include "../engine/event/ICallback.h"

class EventTest : public Test::Suite
{
public:

	/*
	Function:	Constructor, initialize number of tests to be done,
	*/
	EventTest();

	/*
	*/
	~EventTest(){}

	/*
	Function:	Overload Setup() function in base class, setup the test,
				register all functions to be tested.
	*/
	void setup();

private:

	Dispatcher	_events; // Event dispatcher

	class Event1 : public IEvent
	{
		std::string getType()
		{
			return "event1";
		}
	}_event1;

	class Event2 : public IEvent
	{
		std::string getType()
		{
			return "event2";
		}
	}_event2;

	// Listener class for event1 and event2
	class EventListener1 : public ICallback
	{
	private:

		EventTest* _testInstance;

	public:

		EventListener1(){}
		void setInstance( EventTest* testInstance )
		{
			_testInstance = testInstance ;
		}
		~EventListener1(){}

		virtual void execute( IEvent* event )
		{
			if( event -> getType() == "event1" )
			{
				ASSERT( true, "Listener1 event1 test success", "Fail" );
			}
			if( event -> getType() == "event2" )
			{
				ASSERT( true, "Listener1 event2 test success", "Fail" );
			}
		}

	} _listener1;

	// Listener class for event1
	class EventListener2 : public ICallback
	{
	private:

		EventTest* _testInstance;

	public:

		EventListener2(){}
		void setInstance( EventTest* testInstance )
		{
			_testInstance = testInstance ;
		}
		~EventListener2(){}

		virtual void execute( IEvent* event )
		{
			if( event -> getType()  == "event1" )
			{
				ASSERT( true, "Listener2 event1 test success", "Fail" );
			}
		}

	} _listener2;

	/*
	Function:	Test open and close channel
	Return:		void
	Parameters:	void
	*/
	void _eventCallBackTest();

	/*
	Function:	Remove listener test
	Return:		void
	Parameters:	void
	*/
	void _removeListenerTest();
};
