
#include "../test/DynamicArrayTest.h"
#include "../test/QueueTest.h"
#include "../test/StackTest.h"
#include "../test/HashmapTest.h"
#include "../test/JsonTest.h"
#include "../test/DebugTest.h"
#include "../test/EventTest.h"
#include "../test/StateMachineTest.h"
#include "../engine/debug/Debug.h"

/*
Tests
*/
int main1()
{
	// Initialize debug channel
	Debug::getInstance();

	DebugTest debugTest;
	debugTest.runTest();

	Debug::getInstance() -> closeSystem();
	Debug::getInstance() -> openSystem();

	Debug::getInstance() -> openChannel( UNITTEST );

	DynamicArrayTest arrayTest;
	arrayTest.runTest();

	QueueTest queueTest;
	queueTest.runTest();

	StackTest stackTest;
	stackTest.runTest();

	HashmapTest hashmapTest;
	hashmapTest.runTest();

	// Require json file in "assets/json/test.json", otherwise abort
	//JsonTest jsonTest;
	//jsonTest.runTest();

	EventTest eventTest;
	eventTest.runTest();

	StateMachineTest stateMachineTest;
	stateMachineTest.runTest();

	return 0;
}
