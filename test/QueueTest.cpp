#include "QueueTest.h"

QueueTest::QueueTest()
{
	setNumOfTests( 3 );
}

QueueTest::~QueueTest(){}

void QueueTest::_sizePushPopTest()
{
	std::cout << std::endl << "size, push and pop test." << std::endl;

	_pass = _queue.size() == 0;
	ASSERT( _pass, "Empty size test", "size not equal to 0" );
	
	_queue.push( 10 );
	_pass = _queue.size() == 1;
	ASSERT( _pass, "Queue with 1 element test", "size not equal to 1 or push fail" );

	for( int i = 0; i < 100; i++ )
	{
		_queue.push( i );
	}
	_pass = _queue.size() == 101;
	ASSERT( _pass, "Queue with 101 element test", "size not equal to 101 or push fail" );

	_queue.pop();
	_queue.pop();
	_pass = _queue.size() == 99;
	ASSERT( _pass, "Queue with 9s element test", "size not equal to 99 or pop fail" );
}

void QueueTest::_elementAccessTest()
{
	std::cout << std::endl << "Element access test." << std::endl;

	_pass = _queue.front() == 1;
	ASSERT( _pass, "Queue head equals 1 test", "Queue head is not 1" );

	_queue.pop();
	_pass = _queue.front() == 2;
	ASSERT( _pass, "Queue head equals 2 test", "Queue head is not 2" );

	_pass = _queue.back() == 99;
	ASSERT( _pass, "Queue tail equals 99 test", "Queue tail is not 99" );

	_queue.push( 100 );
	_pass = _queue.back() == 100;
	ASSERT( _pass, "Queue tail equals 100 test", "Queue tail is not 100" );
}

void QueueTest::_emptyTest()
{
	std::cout << std::endl << "Queue empty test." << std::endl;

	_pass = !_queue.empty();
	ASSERT( _pass, "Queue not empty test", "Queue is empty" );


	for( int i = 0; i < 99; i++ )
	{
		_queue.pop();
	}
	_pass = _queue.empty();
	ASSERT( _pass, "Queue empty test", "Queue is not empty" );

}

void QueueTest::setup()
{
	registerTest( static_cast< Func >( &QueueTest::_sizePushPopTest ) );
	registerTest( static_cast< Func >( &QueueTest::_elementAccessTest ) );
	registerTest( static_cast< Func >( &QueueTest::_emptyTest ) );
}