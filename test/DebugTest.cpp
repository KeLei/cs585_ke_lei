#include "DebugTest.h"

DebugTest::DebugTest()
{
	setNumOfTests( 1 );
}

DebugTest::~DebugTest(){}

void DebugTest::_channelValidationTest()
{
	std::cout << std::endl << "Channel validation test." << std::endl;

	Debug::getInstance() -> openChannel( GAMEPLAY );
	Debug::getInstance() -> openChannel( UNITTEST );
	Debug::getInstance() -> log( GAMEPLAY, "GAMEPLAY channel enabled" );
	Debug::getInstance() -> log( UNITTEST, "UNITTEST channel enabled" );

	Debug::getInstance() -> log( GAMEPLAY, "Closing GAMEPLAY channel" );
	Debug::getInstance() -> closeChannel( GAMEPLAY );
	Debug::getInstance() -> log( UNITTEST, "GAMEPLAY channel closed" );
	Debug::getInstance() -> log( GAMEPLAY, "This message should not appear in terminal" );

	Debug::getInstance() -> log( UNITTEST, "Closing system, there should be no more logging" );
	Debug::getInstance() -> closeSystem();

	Debug::getInstance() -> log( GAMEPLAY, "This message should not appear in terminal" );
	Debug::getInstance() -> log( UNITTEST, "This message should not appear in terminal" );

	Debug::getInstance() -> openSystem();

	Debug::getInstance() -> openChannel( UNITTEST );
	Debug::getInstance() -> log( UNITTEST, "System re-open, UNITTEST channel enabled" );

}

void DebugTest::setup()
{
	registerTest( static_cast< Func >( &DebugTest::_channelValidationTest ) );
}
