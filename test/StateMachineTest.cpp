#include "StateMachineTest.h"

StateMachineTest::StateMachineTest()
{
	setNumOfTests( 2 );

	_machine.addState( "state1", &_state1 );
	_machine.addState( "state2", &_state2 );
	_machine.setInitialState( "state1" );
}

void StateMachineTest::_stateTransitionTest()
{
	_machine.tick( 0.0 );
	_pass = _machine.getCurrentState() -> getName() == "state2";
	ASSERT(  _pass, "Transfer from state1 to state2 test", "Failed" ); 

	_machine.tick( 0.0 );
	_pass = _machine.getCurrentState() -> getName() == "state1";
	ASSERT( _pass, "Transfer from state2 to state1 test", "Failed" ); 

	_machine.tick( 0.0 );
	_pass = _machine.getCurrentState() -> getName() == "state2";
	ASSERT( _pass, "Transfer from state1 to state2 test", "Failed" ); 
}

void StateMachineTest::setup()
{
	registerTest( static_cast< Func >( &StateMachineTest::_stateTransitionTest ) );
}