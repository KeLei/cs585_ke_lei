#include "../engine/state/IStateMachine.h"
#include "../engine/test_suite/suite.h"
#include "../engine/event/Dispatcher.h"


class StateMachineTest : public Test::Suite
{
public:

	/*
	Function:	Constructor, initialize number of tests to be done,
	*/
	StateMachineTest();

	/*
	Destructor
	*/
	~StateMachineTest(){}

	/*
	Function:	Overload Setup() function in base class, setup the test,
	register all functions to be tested.
	*/
	void setup();

private:

	bool _pass;			// Hold the expression result for a test

	// Basic state machine
	class TestStateMachine : public IStateMachine
	{
	public:
		void onStateTransition( IEvent* event )
		{
			if( event -> getType() == "state1")
			{
				_currentState = _stateMap[ "state2" ];
			}
			if( event -> getType() == "state2")
			{
				_currentState = _stateMap[ "state1" ];
			}
		}
	}_machine;

	// Events
	class Event1 : public IEvent
	{
	public:
		std::string getType()
		{
			return "state1";
		}
	};

	class Event2 : public IEvent
	{
		std::string getType()
		{
			return "state2";
		}
	};

	// State class
	class State1 : public State
	{
		Event1 _event1;

	public:

		std::string getName()
		{
			return "state1";
		}

		void tick( float dt )
		{
			_dispatcher.dispatch( &_event1 );
		}

		void preTick( float dt ){}
        void postTick( float dt ){}
	}_state1;

	// State class
	class State2 : public State
	{

		Event2 _event2;

	public:

		std::string getName()
		{
			return "state2";
		}

		void tick( float dt )
		{
			_dispatcher.dispatch( &_event2 );
		}

		void preTick( float dt ){}
        void postTick( float dt ){}
	}_state2;

	void _stateTransitionTest();
};
