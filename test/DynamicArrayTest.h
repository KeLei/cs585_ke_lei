#pragma once

#include "../engine/test_suite/suite.h"
#include "../engine/structures/DynamicArray.h"

/*
Class inherit from Test::Suite for testing dynamic array
*/
class DynamicArrayTest : public Test::Suite
{
public:

	/*
	Function:	Constructor, initialize number of tests to be done,
				initialize an int array for testing.
	*/
	DynamicArrayTest();

	/*
	*/
	~DynamicArrayTest();

	/*
	Function:	Overload Setup() function in base class, setup the test,
				register all functions to be tested.
	*/
	void setup();

private:

	DynamicArray< int > _arrayA;	// Dynamic int array used for testing
	DynamicArray< int > _arrayB;	// Dynamic int array used for testing

	int _array[10];					// Hard coded int array for testing
	bool _pass;						// Hold the expression result for a test


	/*
	Function:	Test if the array's size function work well
	Return:		void
	Parameters:	void
	*/
	void _sizeTest();

	/*
	Function:	Test all 4 constructors to construct dynamic arrays
	Return:		void
	Parameters:	void
	*/
	void _constructorTest();

	/*
	Function:	Test overloaded operators
	Return:		void
	Parameters:	void
	*/
	void _operatorOverloadTest();

	/*
	Function:	Test array operation: push, pop, insert, erase
	Return:		void
	Parameters:	void
	*/
	void _operationTest();

};