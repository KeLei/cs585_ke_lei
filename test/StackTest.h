#pragma once

#include "../engine/test_suite/suite.h"
#include "../engine/structures/Stack.h"

/*
Class inherit from Test::Suite for testing stack
*/
class StackTest : public Test::Suite
{
public:

	/*
	Function:	Constructor, initialize number of tests to be done,
	*/
	StackTest();

	/*
	*/
	~StackTest();

	/*
	Function:	Overload Setup() function in base class, setup the test,
				register all functions to be tested.
	*/
	void setup();

private:

	Stack< int >	_stack;	// Int queue used for tesing
	bool			_pass;	// Hold the expression result for a test

	/*
	Function:	Test the stack's Size, Push and Pop function
	Return:		void
	Parameters:	void
	*/
	void _sizePushPopTest();

	/*
	Function:	Test Pop function
	Return:		void
	Parameters:	void
	*/
	void _getTopTest();

	/*
	Function:	Test empty function
	Return:		void
	Parameters:	void
	*/
	void _emptyTest();

};