#include "StackTest.h"

StackTest::StackTest()
{
	setNumOfTests( 3 );
}

StackTest::~StackTest(){}

void StackTest::_sizePushPopTest()
{
	std::cout << std::endl << "size, push and pop test." << std::endl;

	_pass = _stack.size() == 0;
	ASSERT( _pass, "Empty size test", "size not equal to 0" );
	
	_stack.push( 10 );
	_pass = _stack.size() == 1;
	ASSERT( _pass, "Stack with 1 element test", "size not equal to 1 or push fail" );

	for( int i = 0; i < 100; i++ )
	{
		_stack.push( i );
	}
	_pass = _stack.size() == 101;
	ASSERT( _pass, "Stack with 101 element test", "size not equal to 101 or push fail" );

	_stack.pop();
	_stack.pop();
	_pass = _stack.size() == 99;
	ASSERT( _pass, "Stack with 99 element test", "size not equal to 99 or pop fail" );
}

void StackTest::_getTopTest()
{
	std::cout << std::endl << "Element access test." << std::endl;

	_pass = _stack.top() == 97;
	ASSERT( _pass, "Stack tail equals 97 test", "Stack tail is not 97" );

	_stack.push( 100 );
	_pass = _stack.top() == 100;
	ASSERT( _pass, "Stack tail equals 100 test", "Stack tail is not 100" );
}

void StackTest::_emptyTest()
{
	std::cout << std::endl << "Stack empty test." << std::endl;

	_pass = !_stack.empty();
	ASSERT( _pass, "Stack not empty test", "Stack is empty" );


	for( int i = 0; i < 100; i++ )
	{
		_stack.pop();
	}
	_pass = _stack.empty();
	ASSERT( _pass, "Stack empty test", "Stack is not empty" );

}

void StackTest::setup()
{
	registerTest( static_cast< Func >( &StackTest::_sizePushPopTest ) );
	registerTest( static_cast< Func >( &StackTest::_getTopTest ) );
	registerTest( static_cast< Func >( &StackTest::_emptyTest ) );
}