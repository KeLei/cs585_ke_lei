#include "JsonTest.h"

JsonTest::JsonTest()
{
	setNumOfTests( 6 );
	JsonParser parser( "assets/json/test.json" );
	_jsonObj = parser.parse();
}

JsonTest::~JsonTest(){}

void JsonTest::_readNumberTest()
{
	_pass = ( _jsonObj -> getValueMap()[ "number" ] -> getNumberValue() == 3.1415926f );
	ASSERT( _pass, "Read 3.1415926 from Json", "Number not equal to 3.1415926" );
}

void JsonTest::_readBooleanTest()
{
	_pass = ( _jsonObj -> getValueMap()[ "bool" ] -> getBooleanValue() == true );
	ASSERT( _pass, "Read true statement from Json", "Statement is wrong" );
}

void JsonTest::_readStringTest()
{
	_pass = ( _jsonObj -> getValueMap()[ "string" ] -> getStringValue() == "test" );
	ASSERT( _pass, "Read string 'test' from Json", "String is not 'test'" );
}

void JsonTest::_readFromArrayTest()
{
	_pass = ( _jsonObj -> getValueMap()[ "array" ] -> getValueArray()[0] -> getNumberValue() == 1.0f &&
		_jsonObj -> getValueMap()[ "array" ] -> getValueArray()[5] -> getNumberValue() == 6.0f );
	ASSERT( _pass, "Read array from Json", "Array is not valid" );
}

void JsonTest::_readFromJsonObjectTest()
{
	_pass = ( _jsonObj -> getValueMap()[ "jsonObject" ]
			-> getValueMap()[ "number" ]
			-> getNumberValue() == 100.0f );
	ASSERT( _pass, "Read number from json object inside json object", "Number is not 100" );

	_pass = ( _jsonObj -> getValueMap()[ "jsonObject" ]
			-> getValueMap()[ "array" ]
			-> getValueArray()[ 0 ]
			-> getStringValue() == "1" );
	ASSERT( _pass, "Read string from array of json object inside json object inside another json object", "Number is not 100" );
}

void JsonTest::_readFromComplexObjectTest()
{
	_pass = ( _jsonObj -> getValueMap()[ "arrayOfJsonObject" ]
			-> getValueArray()[ 0 ]
			-> getValueMap()[ "number" ]
			-> getNumberValue() == 200.0f );
	ASSERT( _pass, "Read number 200 from complex json object", "Number is not 200" );

	_pass = ( _jsonObj -> getValueMap()[ "arrayOfJsonObject" ]
			-> getValueArray()[ 1 ]
			-> getValueMap()[ "array" ]
			-> getValueArray()[ 0 ]
			-> getValueMap()[ "boss" ]
			-> getStringValue() == "FinalBoss" );
	ASSERT( _pass, "Read string 'FinalBoss' from complex json object", "String is not FinalBoss" );
}

void JsonTest::setup()
{
	registerTest( static_cast< Func >( &JsonTest::_readNumberTest ) );
	registerTest( static_cast< Func >( &JsonTest::_readBooleanTest ) );
	registerTest( static_cast< Func >( &JsonTest::_readStringTest ) );
	registerTest( static_cast< Func >( &JsonTest::_readFromArrayTest ) );
	registerTest( static_cast< Func >( &JsonTest::_readFromJsonObjectTest ) );
	registerTest( static_cast< Func >( &JsonTest::_readFromComplexObjectTest ) );
}
