#pragma once

#include "../engine/test_suite/suite.h"
#include "../engine/structures/Hashmap.h"

/*
Class inherit from Test::Suite for testing queue
*/
class HashmapTest : public Test::Suite
{
public:

	/*
	Function:	Constructor, initialize number of tests to be done,
	*/
	HashmapTest();

	/*
	*/
	~HashmapTest();

	/*
	Function:	Overload Setup() function in base class, setup the test,
				register all functions to be tested.
	*/
	void setup();

private:

	Hashmap< int >	_map;	// Int queue used for tesing
	bool			_pass;	// Hold the expression result for a test

	/*
	Function:	Test Insert & operator[] overload
	Return:		void
	Parameters:	void
	*/
	void _storeRetriveTest();

};