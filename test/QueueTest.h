#pragma once

#include "../engine/test_suite/suite.h"
#include "../engine/structures/Queue.h"

/*
Class inherit from Test::Suite for testing queue
*/
class QueueTest : public Test::Suite
{
public:

	/*
	Function:	Constructor, initialize number of tests to be done,
	*/
	QueueTest();

	/*
	*/
	~QueueTest();

	/*
	Function:	Overload Setup() function in base class, setup the test,
				register all functions to be tested.
	*/
	void setup();

private:

	Queue< int >	_queue;	// Int queue used for tesing
	bool			_pass;	// Hold the expression result for a test

	/*
	Function:	Test the queue's Size, Push and Pop function
	Return:		void
	Parameters:	void
	*/
	void _sizePushPopTest();

	/*
	Function:	Test Front & Back function
	Return:		void
	Parameters:	void
	*/
	void _elementAccessTest();

	/*
	Function:	Test empty function
	Return:		void
	Parameters:	void
	*/
	void _emptyTest();

};