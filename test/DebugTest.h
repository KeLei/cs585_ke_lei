#pragma once

#include "../engine/test_suite/suite.h"
#include "../engine/debug/Debug.h"

/*
Class inherit from Test::Suite for testing queue
*/
class DebugTest : public Test::Suite
{
public:

	/*
	Function:	Constructor, initialize number of tests to be done,
	*/
	DebugTest();

	/*
	*/
	~DebugTest();

	/*
	Function:	Overload Setup() function in base class, setup the test,
				register all functions to be tested.
	*/
	void setup();

private:

	/*
	Function:	Test open and close channel
	Return:		void
	Parameters:	void
	*/
	void _channelValidationTest();

};