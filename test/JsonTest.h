#include "../engine/json/JsonParser.h"
#include "../engine/test_suite/suite.h"

class JsonTest : public Test::Suite
{

public:

	/*
	Function:	Constructor, initialize number of tests to be done,
				initialize an int array for testing.
	*/
	JsonTest();

	/*
	*/
	~JsonTest();

	/*
	Function:	Overload setup() function in base class, setup the test,
				register all functions to be tested.
	Return:		void
	Parameters:	void
	*/
	void setup();

private:

	JsonValue*	_jsonObj;	// Hold the json object parsed	
	bool		_pass;		// Hold the expression result for a test


	/*
	Function:	Test read number from json file
	Return:		void
	Parameters:	void
	*/
	void _readNumberTest();

	/*
	Function:	Test read bool from json file
	Return:		void
	Parameters:	void
	*/
	void _readBooleanTest();

	/*
	Function:	Test read string from json file
	Return:		void
	Parameters:	void
	*/
	void _readStringTest();

	/*
	Function:	Test read arry from json file
	Return:		void
	Parameters:	void
	*/
	void _readFromArrayTest();

	/*
	Function:	Test read json object from json file
	Return:		void
	Parameters:	void
	*/
	void _readFromJsonObjectTest();

	/*
	Function:	Test read from complex json ojbect from json file
	Return:		void
	Parameters:	void
	*/
	void _readFromComplexObjectTest();
};
