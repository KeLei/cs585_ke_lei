
#pragma once
#include <ncurses.h>
#include "../scene/WindowManager.h"
#include "../structures/Hashmap.h"
#include "../json/JsonParser.h"
#include "../debug/Debug.h"

class AssetManager
{
public:

    /*
    Function:	Get instance of AssetManager
    Return:		Instance of the AssetManager
    Parameters:	void
    */
    static AssetManager* getInstance();

    /*
    Function:	Read asset from file
    Return:		void
    Parameters:	assetPath - path of the asset file
    */
    void readAsset( std::string assetPath );

    /*
    Function:	Get configurations of a certain key
    Return:		JsonValue of that configuration
    Parameters:	key - key of the object
    */
    JsonValue* getConfig( std::string key );

    /*
    Function:	Get attribute from key
    Return:		attribute of the certain key
    Parameters:	attribute used for NCURSES
    */
    int getAttribute( std::string key );

    ~AssetManager();

private:

    AssetManager();

    static AssetManager*    _instance;    // Instance of this singleton class
    Hashmap< JsonValue* >   _configs;     // Map of configurations
    Hashmap< int >          _attributes;  // Map of attributes
    JsonValue*              _assets;      // Json object of asset file
    JsonParser              _parser;      // Parser of json object

    /*
    Function:	Read configurations from file
    Return:		void
    Parameters:	void
    */
    void _readConfigs();

    /*
    Function:	Read colors from file
    Return:		void
    Parameters:	void
    */
    void _readColors();

    /*
    Function:	Read windows from file
    Return:		void
    Parameters:	void
    */
    void _readWindows();

    /*
    Function:	Read attributes from file
    Return:		void
    Parameters:	void
    */
    void _readAttributes();
};
