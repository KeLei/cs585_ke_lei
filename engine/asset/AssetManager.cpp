#include "AssetManager.h"

AssetManager* AssetManager::_instance = NULL;

AssetManager::AssetManager()
{
    _assets = NULL;
}

AssetManager::~AssetManager()
{
    delete _assets;
}

AssetManager* AssetManager::getInstance()
{
    if( _instance == NULL )
        _instance = new AssetManager;

    return _instance;
}

void AssetManager::readAsset( std::string assetPath )
{
    _assets = _parser.parseFrom( assetPath.c_str() );

    _readConfigs();
    _readColors();
    _readAttributes();
    _readWindows();

    Debug::getInstance() -> log( "asset", ASSET, "Loaded successfully" );
}

JsonValue* AssetManager::getConfig( std::string key )
{
    return _configs[ key ];
}

int AssetManager::getAttribute( std::string key )
{
    return _attributes[ key ];
}

void AssetManager::_readConfigs()
{
    int configs = _assets -> getValueMap()[ "configs" ] -> getValueArray().size();
    std::string name;
    std::string path;
    for( int i = 0; i < configs; i++ )
    {
        name = _assets ->
                getValueMap()[ "configs" ] ->
                getValueArray()[ i ] ->
                getValueMap()[ "name" ] ->
                getStringValue();
        path = _assets ->
                getValueMap()[ "configs" ] ->
                getValueArray()[ i ] ->
                getValueMap()[ "path" ] ->
                getStringValue();

        _configs.insert( name, _parser. parseFrom( path ) );
        Debug::getInstance() -> log( "asset", ASSET, "Loaded %s configuration", name.c_str() );
    }
}

void AssetManager::_readColors()
{
    unsigned colorPairs = _assets -> getValueMap()[ "color_pairs" ] -> getValueArray().size();
    short index;
    short foreground;
    short background;
    /*
        COLOR_BLACK   0
        COLOR_RED     1
        COLOR_GREEN   2
        COLOR_YELLOW  3
        COLOR_BLUE    4
        COLOR_MAGENTA 5
        COLOR_CYAN    6
        COLOR_WHITE   7
    */
    for( unsigned i = 0; i < colorPairs; i++ )
    {
        index = ( short )_assets ->
                getValueMap()[ "color_pairs" ] ->
                getValueArray()[ i ] ->
                getValueMap()[ "pair_index" ] ->
                getNumberValue();
        foreground = ( short )_assets ->
                    getValueMap()[ "color_pairs" ] ->
                    getValueArray()[ i ] ->
                    getValueMap()[ "foreground" ] ->
                    getNumberValue();
        background = ( short )_assets ->
                    getValueMap()[ "color_pairs" ] ->
                    getValueArray()[ i ] ->
                    getValueMap()[ "background" ] ->
                    getNumberValue();
        init_pair( index, foreground, background );
        Debug::getInstance() -> log( "asset", ASSET, "Loaded color #%d", index );
    }
}

void AssetManager::_readAttributes()
{
    unsigned attributes = _assets ->
                    getValueMap()[ "attributes" ] ->
                    getValueArray().size();
    int attr;
    std::string name;
    for( unsigned i = 0; i < attributes; i++ )
    {
        attr = 0;
        name = _assets ->
                getValueMap()[ "attributes" ] ->
                getValueArray()[ i ] ->
                getValueMap()[ "name" ] ->
                getStringValue();
        unsigned numAttr = _assets ->
                    getValueMap()[ "attributes" ] ->
                    getValueArray()[ i ] ->
                    getValueMap()[ "attributes" ] ->
                    getValueArray().size();
        std::string attrName;
        for( unsigned j = 0; j < numAttr; j++ )
        {
            attrName = _assets ->
                    getValueMap()[ "attributes" ] ->
                    getValueArray()[ i ] ->
                    getValueMap()[ "attributes" ] ->
                    getValueArray()[ j ] -> getStringValue();
            if( attrName == "normal" )
            {
                attr |= A_NORMAL;
            }
            else if( attrName == "standout" )
            {
                attr |= A_STANDOUT;
            }
            else if( attrName == "underline" )
            {
                attr |= A_UNDERLINE;
            }
            else if( attrName == "reverse" )
            {
                attr |= A_REVERSE;
            }
            else if( attrName == "blink" )
            {
                attr |= A_BLINK;
            }
            else if( attrName == "dim" )
            {
                attr |= A_DIM;
            }
            else if( attrName == "bold" )
            {
                attr |= A_BOLD;
            }
            else if( attrName == "protect" )
            {
                attr |= A_PROTECT;
            }
            else if( attrName == "invisible" )
            {
                attr |= A_INVIS;
            }
            else if( attrName == "alt_char" )
            {
                attr |= A_ALTCHARSET;
            }
            else if( attrName == "bitmask" )
            {
                attr |= A_CHARTEXT;
            }

        }
        _attributes.insert( name, attr );
        Debug::getInstance() -> log( "asset", ASSET, "Loaded attribute %s", name.c_str() );
    }
}

void AssetManager::_readWindows()
{
    unsigned numWins = _assets -> getValueMap()[ "windows" ] -> getValueArray().size();
    std::string name;
    int height, width, startX, startY;

    for( unsigned i = 0; i < numWins; i++ )
    {
        name = _assets ->
                getValueMap()[ "windows" ] ->
                getValueArray()[ i ] ->
                getValueMap()[ "name" ] ->
                getStringValue();
        height = ( int )_assets ->
                    getValueMap()[ "windows" ] ->
                    getValueArray()[ i ] ->
                    getValueMap()[ "height" ] ->
                    getNumberValue();
        width = ( int )_assets ->
                    getValueMap()[ "windows" ] ->
                    getValueArray()[ i ] ->
                    getValueMap()[ "width" ] ->
                    getNumberValue();
        startX = ( int )_assets ->
                    getValueMap()[ "windows" ] ->
                    getValueArray()[ i ] ->
                    getValueMap()[ "x" ] ->
                    getNumberValue();
        startY = ( int )_assets ->
                    getValueMap()[ "windows" ] ->
                    getValueArray()[ i ] ->
                    getValueMap()[ "y" ] ->
                    getNumberValue();
        WindowManager::getInstance() -> addWin( name, height, width, startX, startY );
        Debug::getInstance() -> log( "asset", ASSET, "Loaded window %s", name.c_str() );
    }
}
