#pragma once

#include "../event/IEvent.h"
#include "../event/ICallback.h"
#include "../scene/ITickable.h"
#include "../structures/Hashmap.h"
#include "IState.h"

class IStateMachine : public ITickable, public ICallback
{

protected:

	State*				_currentState;		// Hold the pointer to the instance of current state
	Hashmap< State* >	_stateMap;			// Store the states in map
	Hashmap< float >	_behaviorConfig;	// Store configuration in different states

public:

	/*
	Function:	Constructor, set currentState to NULL
	Parameters:	void
	*/
	IStateMachine()
	{
		_currentState = NULL;
	}

	/*
	Function:	Add a state to state machine
	Return:		void
	Parameters:	stateName - Name of the state
	state - pointer to the state
	*/
	virtual void addState( std::string stateName, State* state )
	{
		_stateMap.insert( stateName, state );
	}

	/*
	Function:	Called when an event happen and needs to change its state
	Return:		void
	Parameters:	event - pointer to an event
	*/
	virtual void onStateTransition( IEvent* event ) = 0;

	/*
	Function:	Set the state machine's initial state
	Return:		void
	Parameters:	state - Initial state
	*/
	void setInitialState( std::string stateName )
	{
		if( _currentState == NULL )
		{
			_currentState = _stateMap[ stateName ];
		}
	}

	/*
	Function:	Get current state name
	Return:		void
	Parameters:	state - Initial state
	*/
	State* getCurrentState()
	{
		return _currentState;
	}

	void tick( float dt )
	{
		_currentState -> tick( dt );
	}

	void preTick( float dt )
	{
		_currentState -> preTick( dt );
	}

	void postTick( float dt )
	{
		_currentState -> postTick( dt );
	}


	void execute( IEvent* event )
	{
		onStateTransition( event );
	}
};
