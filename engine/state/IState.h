#pragma once

#include <string>
#include "../event/Dispatcher.h"
#include "../scene/ITickable.h"

// Inter face for state
class State : public ITickable
{
public:

	/*
	Function:	Get the name of the state
	Return:		Name of the state
	Parameters:	void
	*/
	std::string getName()
	{
        return _name;
	}

	/*
	Function:	Get the dispatcher of the state
	Return:		Dispatcher of the state
	Parameters:	void
	*/
	Dispatcher* getDispatcher()
	{
        return &_dispatcher;
	}

	virtual ~State(){}

protected:

   	Dispatcher  _dispatcher;	// Dispatch state change events
   	std::string _name;          // Name of the state

};
