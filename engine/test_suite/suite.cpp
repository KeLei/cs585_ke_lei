#include "suite.h"

namespace Test
{
	Suite::Suite()
	{
		testCount = 0;
		testTotal = 0;
	}

	Suite::~Suite() {}

	void Suite::runTest()
	{
		setup();
		Debug::getInstance() -> log( UNITTEST, "Loaded main tests, %d test(s) in total.", testCount );
		Timer timer;
		timer.start();

		// Run all test functions in the test array
		for( unsigned i = 0; i < testCount; i++ )
		{
			( this ->*_testFuncs[ i ] )();
		}

		float runTimer = ( float )timer.getElapsedTime().count() / 1000.0f;
		Debug::getInstance() -> log( UNITTEST, "End test, %.3f seconds used.\n\n", runTimer);
	}

	void Suite::setNumOfTests( unsigned n )
	{
		testTotal = n;
		_testFuncs = new Func[ n ];
	}

	void Suite::registerTest( Func f )
	{
		testCount++;

		if( testCount > testTotal )
		{
			std::cout << "Reach max tests in this suite!" << std::endl;
			testCount--;
		}
		else
		{
			_testFuncs[ testCount - 1 ] = f;
		}
	}
}
