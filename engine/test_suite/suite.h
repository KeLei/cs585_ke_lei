#pragma once

#include <cassert>
#include <iostream>
#include "assertment.h"
#include "../timer/Timer.h"

namespace Test
{

	// Base class for all test suites
	// Test suites are derived from this class
	class Suite
	{

	protected:

		//Define pointer to a function
		typedef void (Suite::*Func)();

		unsigned	testCount;		// Number of tests to be done
		unsigned	testTotal;		// Max number of tests in this suite

	private:

		Func*		_testFuncs;		// Array of functions to be tested

	public:

		Suite();
		~Suite();

		/*
		Function:	Run the whole test
		Return:		void
		Parameters:	void
		*/
		void runTest();

		/* 
		Function:	Allocate memory for tests, set number of tests to be executed
		Return:		void
		Parameters:	numTest - number of tests
		*/
		void setNumOfTests( unsigned numTest );

		/* 
		Function:	Add a function to the function array, will do nothing if the array reachs its limit
		Return:		void
		Parameters:	functionPtr - pointer to a certain function to be tested
		*/
		void registerTest( Func functionPtr );

		// Interface to set up a test
		virtual void setup() {}

	};

}