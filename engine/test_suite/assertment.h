#pragma once

#include <iostream>
#include "../debug/Debug.h"


/*
Macro ASSERT for unit test

Paraments:	expr - expression to be verified
			name - Current test name
			errMsg - Error message if the expression value is false

*/
#define ASSERT( expr, name, errMsg )												\
{																					\
	if( !expr )																		\
	{																				\
		Debug::getInstance() -> log( UNITTEST, "[Failed] %s: %s", name, errMsg );	\
	}																				\
	else																			\
	{																				\
		Debug::getInstance() -> log( UNITTEST, "[Passed] %s", name );				\
	}																				\
}																	

#define ASSERTFILE( file, expr, name, errMsg )											\
{																						\
	if( !expr )																			\
	{																					\
		Debug::getInstance() -> log( file, UNITTEST, "[Failed] %s: %s", name, errMsg );	\
	}																					\
	else																				\
	{																					\
		Debug::getInstance() -> log( file, UNITTEST, "[Passed] %s", name );				\
	}																					\
}																	
