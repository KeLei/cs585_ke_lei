#pragma once

#include <string>

/*
Interface of events
*/
class IEvent
{

public:

	// Get the type of the event
	virtual std::string getType() = 0;

	virtual ~IEvent(){}
};