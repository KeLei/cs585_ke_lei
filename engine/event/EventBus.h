#include "Dispatcher.h"

class EventBus
{
public:

	static Dispatcher* getInstance();
	~EventBus();

private:

	static Dispatcher* _instance;
	EventBus(){}

};