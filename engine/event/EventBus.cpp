#include "EventBus.h"

Dispatcher* EventBus::_instance = NULL;

EventBus::~EventBus()
{
	delete _instance;
}

Dispatcher* EventBus::getInstance()
{
	if( _instance == NULL )
	{
		_instance = new Dispatcher;
	}
	return _instance;
}