#include "Dispatcher.h"

void Dispatcher::addListener( std::string eventType, ICallback* callBack )
{
    DynamicArray< ICallback* > insCallback;
	if( _eventListeners[ eventType ].size() == 0 )
	{
		_eventListeners.insert( eventType, insCallback );
	}

	_eventListeners[ eventType ].pushBack( callBack );
}

void Dispatcher::removeListener( std::string eventType, ICallback* callBack )
{
	unsigned size = _eventListeners[ eventType ].size();

	for( unsigned i = 0; i < size; i++ )
	{
		if( _eventListeners[ eventType ][ i ] == callBack )
		{
			_eventListeners[ eventType ].erase( i );
			break;
		}
	}
}

void Dispatcher::dispatch( IEvent* event )
{
	unsigned size = _eventListeners[ event -> getType() ].size();

	for( unsigned i = 0; i < size; i++ )
	{
		_eventListeners[ event -> getType() ][ i ] -> execute( event );
	}
}