#pragma once

#include "IEvent.h"

/*
Interface of event listener callbacks
*/
class ICallback
{

public:

	/*
	Function:	Execute something given an event
	Return:		void
	Parameters:	eventType - The type of event
	*/
	virtual void execute( IEvent* event ) = 0;

	virtual ~ICallback(){}
};