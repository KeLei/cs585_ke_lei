#pragma once

#include "ICallback.h"
#include "../structures/Hashmap.h"
#include "../scene/ITickable.h"

/*
Definition of Dispatcher class that handle events
*/
class Dispatcher
{

private:

	Hashmap< DynamicArray< ICallback* > >	_eventListeners;	// A hashmap of arrays holding list of listeners

public:

	/*
	Function:	Add listener to an event type
	Return:		void
	Parameters:	eventType - The type of event to listen
				callBack - Pointer to the listener
	*/
	void addListener( std::string eventType, ICallback* callBack );

	/*
	Function:	Remove listener from an event type
	Return:		void
	Parameters:	eventType - The type of event to listen
				callBack - Pointer to the listener
	*/
	void removeListener( std::string eventType, ICallback* callBack );

	/*
	Function:	Dispatch an event to its listeners
	Return:		void
	Parameters:	eventType - The type of event dispatch
	*/
	void dispatch( IEvent* event );
};