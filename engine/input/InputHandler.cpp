#include "InputHandler.h"

InputHandler* InputHandler::_instance = NULL;

InputHandler::InputHandler()
{
    _mode = false;
}

InputHandler* InputHandler::getInstance()
{
    if( _instance == NULL )
    {
        _instance = new InputHandler;
    }

    return _instance;
}

void InputHandler::stringMode()
{
    _mode = true;
}

void InputHandler::characterMode()
{
    _mode = false;
}

std::string InputHandler::getIputStirng()
{
    if( _mode )
    {
        return _str;
    }
    else
    {
        return "";
    }
}

int InputHandler::getCurrentKey()
{
    if( !_mode )
    {
        return _c;
    }
    else
    {
        return 0;
    }
}

void InputHandler::update()
{
    if( _mode )
    {
    }
    else
    {
        _c = getch();
    }
}
