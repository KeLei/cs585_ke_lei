#pragma once
#include <ncurses.h>
#include <string>

class InputHandler
{

public:

    /*
    Function:	Get instance of InputHandler
    Return:		Instance of the InputHandler
    Parameters:	void
    */
    static InputHandler* getInstance();

    /*
    Function:	Set input mode to receive string
    Return:		void
    Parameters:	void
    */
    void stringMode();

    /*
    Function:	Set input mode to receive single character
    Return:		void
    Parameters:	void
    */
    void characterMode();

    /*
    Function:	Update input
    Return:		void
    Parameters:	void
    */
    void update();

    /*
    Function:	Get input string
    Return:		string to be inputed
    Parameters:	void
    */
    std::string getIputStirng();

    /*
    Function:	Get the key that is pressed
    Return:		key pressed
    Parameters:	void
    */
    int getCurrentKey();

private:

    InputHandler();

    static InputHandler* _instance; // Instance of the handler
    bool                 _mode;     // True for string input, false for character
    std::string          _str;      // Hold the string that is received
    int                  _c;        // Hold the character that is pressed
};
