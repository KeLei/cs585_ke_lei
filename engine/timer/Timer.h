#pragma once

#include <time.h>
#include <chrono>

/*
Timer class for calculating time intervals between actions
*/
class Timer
{

private:

	std::chrono::steady_clock::time_point	_startTime;		// Time when timer is started
	std::chrono::milliseconds				_timeInterval;	// Time interval limit
	std::chrono::milliseconds				_timeElapsed;	// Time interval between current time and timer started
	bool									_started;		// If the timer is started
	bool									_paused;		// If the timer is paused

public:

	Timer();
	Timer( unsigned interval );
	~Timer();

	/*
	Function:	Set time interval in millionseconds
	Return:		void
	Parameters:	interval - time interval in millionseconds
	*/
	void setInterval( unsigned interval );

	/*
	Function:	Set start time when this function is called
	Return:		void
	Parameters:	void
	*/
	void start();

	/*
	Function:	Restart timer, use current time to calculate time interval
	Return:		void
	Parameters:	void
	*/
	void restart();

	/*
	Function:	Stop timer and set everything to initial
	Return:		void
	Parameters:	void
	*/
	void stop();

	/*
	Function:	Pause timer
	Return:		void
	Parameters:	void
	*/
	void pause();

	/*
	Function:	Test if time is out
	Return:		true if time out, else false
	Parameters:	void
	*/
	bool isTimeout();

	/*
	Function:	Check if the timer is started
	Return:		true if started, else false
	Parameters:	void
	*/
	bool isStarted();

    /*
	Function:	Get the time remaining.
	Return:		true if started, else false
	Parameters:	void
	*/
	std::chrono::milliseconds getRemainingTime();

	/*
	Function:	Get time interval between start time and the time this function is called
	Return:		Time interval in millionseconds
	Parameters:	void
	*/
	std::chrono::milliseconds getElapsedTime();

	/*
	Function:	Stop running for a certain amount of time
	Return:		void
	Parameters:	interval - time to stop in millionseconds
	*/
	static void sleepFor( unsigned interval );
};

