#include "Timer.h"


Timer::Timer() : _started( false ), _paused( false )
{
}

Timer::Timer( unsigned interval ) : _started( false ), _paused( false )
{
	_timeInterval = std::chrono::milliseconds( interval );
}

Timer::~Timer()
{
}

void Timer::setInterval( unsigned interval )
{
	_timeInterval = std::chrono::milliseconds( interval );
}

void Timer::start()
{
	if( _started == false )
	{
		_started = true;
		_startTime = std::chrono::steady_clock::now();
	}
	else if( _paused == true )
	{
        _startTime = std::chrono::steady_clock::now() - _timeElapsed;
        _paused = false;
	}
}

void Timer::restart()
{
	_started = true;
	_startTime = std::chrono::steady_clock::now();
}

void Timer::stop()
{
	_started = false;
}

void Timer::pause()
{
    _paused = true;
    _timeElapsed = getElapsedTime();
}

bool Timer::isTimeout()
{
	if ( _started && !_paused )
	{
		_timeElapsed = getElapsedTime();
		if( _timeElapsed >= _timeInterval )
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	return false;
}

bool Timer::isStarted()
{
	return _started;
}

std::chrono::milliseconds Timer::getRemainingTime()
{
    if( _paused )
        return _timeInterval - _timeElapsed;
    else
        return _timeInterval - getElapsedTime();
}

std::chrono::milliseconds Timer::getElapsedTime()
{
    if( _paused )
    {
        return _timeElapsed;
    }
	else
	{
        return std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::steady_clock::now() - _startTime );
    }
}

void Timer::sleepFor( unsigned interval )
{
	std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();
	while ( true )
	{
		if( interval <= std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::steady_clock::now() - startTime ).count() )
			break;
	}
}
