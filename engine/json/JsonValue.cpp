#include "JsonValue.h"

JsonValue::JsonValue():
	_type( UNDEFINED ),
	_numberValue( 0.0f ),
	_booleanValue( false )
{
}

JsonValue::JsonValue( float value ):
	_type( NUMBER ),
	_numberValue( value ),
	_booleanValue( false )
{
}

JsonValue::JsonValue( std::string value ):
	_type( STRING ),
	_numberValue( 0.0f ),
	_booleanValue( false ),
	_stringValue( value )
{
}

JsonValue::JsonValue( bool value ):
	_type( BOOLEAN ),
	_numberValue( 0.0f ),
	_booleanValue( value )
{
}

JsonValue::JsonValue( DynamicArray< JsonValue* > value ):
	_type( ARRAY ),
	_numberValue( 0.0f ),
	_booleanValue( false ),
	_valueArray( value )
{
}

JsonValue::JsonValue( Hashmap< JsonValue* > value ):
	_type( JSON_OBJECT ),
	_numberValue( 0.0f ),
	_booleanValue( false ),
	_valueMap( value )
{
}

JsonValue::~JsonValue()
{
	if( _type == ARRAY )
	{
		for( unsigned i = 0; i < _valueArray.size(); i++ )
		{
			delete _valueArray[ i ];
		}
	}

	if( _type == JSON_OBJECT )
	{
		_valueMap.clear();
	}
}

void JsonValue::setType( JsonType type )
{
	_type = type;
}

float JsonValue::getNumberValue()
{
	assert( _type == NUMBER );
	return _numberValue;
}

bool JsonValue::getBooleanValue()
{
	assert( _type == BOOLEAN );
	return _booleanValue;
}

std::string JsonValue::getStringValue()
{
	assert( _type == STRING );
	return _stringValue;
}

DynamicArray< JsonValue* >& JsonValue::getValueArray()
{
	assert( _type == ARRAY );
	return _valueArray;
}

Hashmap< JsonValue* >& JsonValue::getValueMap()
{
	assert( _type == JSON_OBJECT );
	return _valueMap;
}

JsonValue& JsonValue::operator[] ( unsigned index )
{
	assert( _type == ARRAY );
	return *( _valueArray[ index ] );
}

JsonValue& JsonValue::operator[] ( std::string key )
{
	assert( _type == JSON_OBJECT );
	return *( _valueMap[ key ] );
}
