#pragma once

#include <fstream>
#include <string>
#include <cassert>
#include <cstdlib>
#include "JsonValue.h"

class JsonParser
{
private:

	std::ifstream	_jsonFile;	// json file stream

	/*
	Function:	Move file stream to next valid charator
	Return:		void
	Parameters:	void
	*/
	void _nextValidChar();

	/*
	Function:	Get a string value from the file stream
	Return:		The string value between two double quotes
	Parameters:	void
	*/
	std::string _getNextString();

	/*
	Function:	Parse a json value
	Return:		The json value
	Parameters:	void
	*/
	JsonValue* _parseJsonNumber();
	JsonValue* _parseJsonBool();
	JsonValue* _parseJsonString();
	JsonValue* _parseJsonArray();
	JsonValue* _parseJsonObject();

public:

	/*
	Function:	Default constructor
	Parameters:	void
	*/
	JsonParser(){}

	/*
	Function:	Constructor, open the json file in the path
	Parameters:	filePath - Path of that json file
	*/
	JsonParser( const std::string& filePath );

	/*
	Function:	Parse the json file
	Return:		The json value of that file
	Parameters:	void
	*/
	JsonValue* parse();

	/*
	Function:	Parse the json file in certain path
	Return:		The json value of that file
	Parameters:	filePath - Path of that json file
	*/
	JsonValue* parseFrom( const std::string& filePath );

	/*
	Function:	Destructor
	*/
	~JsonParser(){}
};
