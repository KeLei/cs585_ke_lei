#pragma once

#include <string>
#include <cassert>
#include "../structures/DynamicArray.h"
#include "../structures/Hashmap.h"


enum JsonType
{
	UNDEFINED,
	NUMBER,
	STRING,
	BOOLEAN,
	ARRAY,
	JSON_OBJECT,
};


class JsonValue
{

private:

	JsonType					_type;			// Type of this Json object

	float						_numberValue;	// number value
	bool						_booleanValue;	// boolean value
	std::string					_stringValue;	// string value
	DynamicArray< JsonValue* >	_valueArray;	// array value
	Hashmap< JsonValue* >		_valueMap;		// object value

public:

	/*
	Function:	Constructors, initialize the json value with existing data
	*/
	JsonValue();
	JsonValue( float value );
	JsonValue( bool value );
	JsonValue( std::string value );
	JsonValue( DynamicArray< JsonValue* > value );
	JsonValue( Hashmap< JsonValue* > value  );

	/*
	Function:	Destructor, delete array or hash map
	*/
	~JsonValue();

	/*
	Function:	Set the type of the json value
	Return:		void
	Parameters:	type - type of the value
	*/
	void setType( JsonType type );

	/*
	Function:	Get the number value of the json value
	Return:		Number value
	Parameters:	void
	*/
	float getNumberValue();

	/*
	Function:	Get the bool value of the json value
	Return:		Bool value
	Parameters:	void
	*/
	bool getBooleanValue();

	/*
	Function:	Get the string value of the json value
	Return:		String value
	Parameters:	void
	*/
	std::string getStringValue();

	/*
	Function:	Get the array of the json value
	Return:		Array
	Parameters:	void
	*/
	DynamicArray< JsonValue* >& getValueArray();

	/*
	Function:	Get the json object of the json value
	Return:		Json object( hash map )
	Parameters:	void
	*/
	Hashmap< JsonValue* >& getValueMap();

	/*
	Function:	Access operator overload
	Return:		JsonValue in the array
	Parameters:	index - index of the value in that array
	*/
	JsonValue& operator[] ( unsigned index );

	/*
	Function:	Access operator overload
	Return:		JsonValue in the hash map
	Parameters:	key - keyof the value in the map
	*/
	JsonValue& operator[] ( std::string key );
};

