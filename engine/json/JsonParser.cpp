#include "JsonParser.h"

JsonParser::JsonParser( const std::string& filePath )
{
	_jsonFile.open( filePath );
}

void JsonParser::_nextValidChar()
{
	char symbol;
	do
	{
		symbol = _jsonFile.get();
	}
	while( symbol == ' ' ||
		symbol == '\n' ||
		symbol == '\t' ||
		symbol == '\r' );
	_jsonFile.unget();
}

JsonValue* JsonParser::_parseJsonObject()
{
	_nextValidChar();

	char symbol = _jsonFile.get();
	assert( symbol == '{' );
	JsonValue* jsonValue = new JsonValue;
	jsonValue -> setType( JSON_OBJECT );

	while ( true )
	{
		_nextValidChar();
		symbol = _jsonFile.get();
		assert( symbol == '\"' );
		std::string key = _getNextString();

		_nextValidChar();
		symbol = _jsonFile.get();
		assert( symbol == ':' );

		_nextValidChar();
		symbol = _jsonFile.get();
		switch( symbol )
		{
			// value is a std::string
		case '\"' :	jsonValue -> getValueMap().insert( key, _parseJsonString() ); break;

			// value is a array
		case '[' :	jsonValue -> getValueMap().insert( key, _parseJsonArray() ); break;

			// value is a bool
		case 't' :
		case 'f' :	jsonValue -> getValueMap().insert( key, _parseJsonBool() ); break;

			// value is a number
		case '-' :
		case '0' :
		case '1' :
		case '2' :
		case '3' :
		case '4' :
		case '5' :
		case '6' :
		case '7' :
		case '8' :
		case '9' : jsonValue -> getValueMap().insert( key, _parseJsonNumber() ); break;

			// value is a json object
		case '{' : _jsonFile.unget();
			jsonValue -> getValueMap().insert( key, _parseJsonObject() ); break;

			// invalid format
		default : assert( false ); break;
		}

		_nextValidChar();
		symbol = _jsonFile.get();
		if( symbol == ',' )
		{
			continue;
		}
		else if( symbol == '}' )
		{
			return jsonValue;
		}
		assert( false );
	}
	return NULL;
}

std::string JsonParser::_getNextString()
{
	std::string key;
	char symbol = _jsonFile.get();
	do
	{
		key += symbol;
		symbol = _jsonFile.get();
	}while ( symbol != '\"');
	return key;
}

JsonValue* JsonParser::_parseJsonNumber()
{
	_jsonFile.unget();
	std::string value;
	char symbol = _jsonFile.get();
	do
	{
		value += symbol;
		symbol = _jsonFile.get();
	}while ( isdigit( symbol ) || symbol == '.' );
	_jsonFile.unget();
	JsonValue* jValue = new JsonValue( ( float )atof( value.c_str() ) );
	return jValue;
}

JsonValue* JsonParser::_parseJsonBool()
{
	_jsonFile.unget();
	if( _jsonFile.get() == 't' )
	{
		return new JsonValue( true );
	}
	else
	{
		return new JsonValue( false );
	}
}

JsonValue* JsonParser::_parseJsonString()
{
	std::string value = _getNextString();

	return new JsonValue( value );
}

JsonValue* JsonParser::_parseJsonArray()
{
	char symbol;
	bool endOfArray = false;

	JsonValue* jsonValue = new JsonValue;
	jsonValue -> setType( ARRAY );

	while( !endOfArray )
	{
		_nextValidChar();
		symbol = _jsonFile.get();
		switch( symbol )
		{
			// value is a std::string
		case '\"' :	jsonValue -> getValueArray().pushBack( _parseJsonString() ); break;

			// value is a array
		case '[' :	jsonValue -> getValueArray().pushBack( _parseJsonArray() ); break;

			// value is a bool
		case 't' :
		case 'f' :	jsonValue -> getValueArray().pushBack( _parseJsonBool() ); break;

			// value is a number
		case '-' :
		case '0' :
		case '1' :
		case '2' :
		case '3' :
		case '4' :
		case '5' :
		case '6' :
		case '7' :
		case '8' :
		case '9' : jsonValue -> getValueArray().pushBack( _parseJsonNumber() ); break;

			// value is a json object
		case '{' : _jsonFile.unget();
					jsonValue -> getValueArray().pushBack( _parseJsonObject() );
					break;

			// invalid format
		default: assert( false ); break;
		}

		_nextValidChar();
		symbol = _jsonFile.get();
		if( symbol == ',' )
		{
			continue;
		}
		else if( symbol == ']' )
		{
			// End of array
			return jsonValue;
		}

		// Format error
		assert( false );
	}
	return NULL;
}


JsonValue* JsonParser::parse()
{
	assert( _jsonFile.is_open() == true );

	return _parseJsonObject();
}

JsonValue* JsonParser::parseFrom( const std::string& filePath )
{
	if( _jsonFile.is_open() )
	{
		_jsonFile.close();
	}

	_jsonFile.open( filePath );

	assert( _jsonFile.is_open() == true );

	return _parseJsonObject();
}
