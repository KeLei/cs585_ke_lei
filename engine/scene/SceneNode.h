#pragma once

#include "../structures/AABB.h"
#include "../camera/Camera.h"
#include "ICollidable.h"
#include "IDrawable.h"

class SceneNode
{

private:

	unsigned	 _id;		  // Id of the scene node
	AABB		 _rect;		  // AABB area of the node
	Point2D		 _center;	  // Center of the node
	ICollidable* _collidable; // Collidable part of the scene node
	IDrawable*   _drawable;   // Drawable part of the scene node

public:

	/*
	Function:	Constructor, set _collidable to NULL
	Parameters:	void
	*/
	SceneNode();

	/*
	Function:	Destructor, delete collidable
	Parameters:	void
	*/
	~SceneNode();

	/*
	Function:	Get the rectangle of this SceneNode
	Return:		The rectangle of this node
	Parameters:	void
	*/
	AABB getRect();

	/*
	Function:	Get top-left point of the node
	Return:		Centre point of the node
	Parameters:	void
	*/
	const Point2D getTopLeft();

	/*
	Function:	Get center point of the node
	Return:		Centre point of the node
	Parameters:	void
	*/
	const Point2D getCenter();

	/*
	Function:	Get id of the node
	Return:		Id of the node
	Parameters:	void
	*/
	const unsigned getId();

	/*
	Function:	Test the node is collide with other node
	Return:		true if the node colide with other node, else false
	Parameters:	void
	*/
	const bool isCollide();

	/*
	Function:	Get collidable part of hte node
	Return:		Collidable part of hte node
	Parameters:	void
	*/
	ICollidable* getCollidable();

	/*
	Function:	Get collidable part of hte node
	Return:		Collidable part of hte node
	Parameters:	void
	*/
	IDrawable* getDrawable();

	/*
	Function:	Set this node to a new rectangle
	Return:		void
	Parameters:	rect - new rectangle to set
	*/
	void setNode( AABB rect );

	/*
	Function:	Move the center of the rectangle to a new position
	Return:		void
	Parameters:	center - new center of the node
	*/
	void setCenter( Point2D center );

	/*
	Function:	Set the scene node's collidable part
	Return:		void
	Parameters:	center - new center of the node
	*/
	void setCollidable( ICollidable* collidable );

	/*
	Function:	Set the scene node's drawable part
	Return:		void
	Parameters:	center - new center of the node
	*/
	void setDrawable( IDrawable* drawable );

	/*
	Function:	Set scene node's id
	Return:		void
	Parameters:	id - id of the charator
	*/
	void setId( unsigned id );

	/*
	Function:	Render the node
	Return:		void
	Parameters:	void
	*/
	void render( Camera camera );
};
