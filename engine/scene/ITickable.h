#pragma once

/*
Interface for tickables
*/
class ITickable
{

public:

	/*
	Function:	Pure virtual function that will do things that happen each time it is called
	Return:		void
	Paraments:	dt - time of the game loop
	*/
	virtual void preTick( float dt ) = 0;

	/*
	Function:	Pure virtual function that will do things that happen each time it is called
	Return:		void
	Paraments:	dt - time of the game loop
	*/
	virtual void tick( float dt ) = 0;

	/*
	Function:	Pure virtual function that will do things that happen each time it is called
	Return:		void
	Paraments:	dt - time of the game loop
	*/
	virtual void postTick( float dt ) = 0;

	/*
    Virtual destructor
    */
	virtual ~ITickable(){}

};
