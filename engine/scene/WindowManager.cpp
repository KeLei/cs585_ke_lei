#include "WindowManager.h"

WindowManager* WindowManager::_instance = NULL;

WindowManager::~WindowManager()
{
    for( unsigned i = 0; i < _windows.getKeys().size(); i++ )
    {
        _destoryWin( _windows[ _windows.getKeys()[ i ] ] );
    }
}


void WindowManager::addWin( std::string name, int height, int width, int startX, int startY )
{
    WINDOW* w = newwin( height, width, startY, startX );
    _windows.insert( name, w );
}

WindowManager* WindowManager::getInstance()
{
    if( _instance == NULL )
    {
        _instance = new WindowManager;
    }

    return _instance;
}

WINDOW* WindowManager::getWin( std::string name )
{
    return _windows[ name ];
}

void WindowManager::drawWin( std::string name )
{
    box( _windows[ name ], 0, 0 );
}

void WindowManager::clearWin( std::string name )
{
    werase( _windows[ name ] );
}

void WindowManager::displayWin( std::string name )
{
    wrefresh( _windows[ name ] );
}

void WindowManager::removeWin( std::string name )
{
    WINDOW* w = _windows[ name ];
    _destoryWin( w );
    _windows.erase( name );
}

void WindowManager::_destoryWin( WINDOW* win )
{
    wborder( win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	wrefresh( win );
	delwin( win );
}

void WindowManager::updateWin( std::string name, int height, int width, int startX, int startY )
{
    // Destory old window
    _destoryWin( _windows[ name ] );

    _windows[ name ] = newwin( height, width, startY, startX );
}

