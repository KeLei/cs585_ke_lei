#include "ASCIIDrawable.h"

ASCIIDrawable::ASCIIDrawable()
{
    _win = NULL;
    _attribute = 0;
    _color = 0;
    _character = '\0';
}

ASCIIDrawable::~ASCIIDrawable()
{
}

ASCIIDrawable::ASCIIDrawable( ASCIIDrawable& drawable )
{
    _win = drawable._win;
    _attribute = drawable._attribute;
    _color = drawable._color;
    _character = drawable._character;
}

int ASCIIDrawable::getColor()
{
    return _color;
}

void ASCIIDrawable::setColor(int color)
{
    _color = color;
}

char ASCIIDrawable::getCharacter()
{
    return _character;
}

void ASCIIDrawable::setCharacter(char character )
{
    _character = character;
}

void ASCIIDrawable::setAttribute( int attribute )
{
    _attribute = attribute;
}

void ASCIIDrawable::setWindow( WINDOW* win )
{
    _win = win;
}

void ASCIIDrawable::setPosition( int x, int y )
{
    _position.x = x;
    _position.y = y;
}

void ASCIIDrawable::render( Camera camera )
{
    wattron( _win, _color | _attribute );
    mvwprintw( _win, _position.y - camera.getPosition().y, _position.x - camera.getPosition().x, "%c", _character );
    wattroff( _win, _color | _attribute );
}

void ASCIIDrawable::attributeOn( int attribute )
{
    _attribute |= attribute;
}

void ASCIIDrawable::attributeOff( int attribute )
{
    _attribute &= ~attribute;
}
