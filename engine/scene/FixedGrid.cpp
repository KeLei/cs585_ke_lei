#include "FixedGrid.h"

FixedGrid::FixedGrid()
{
	leftIndex = 0;
	rightIndex = 0;
	topIndex = 0;
	bottomIndex = 0;
}

FixedGrid::~FixedGrid(){}

void FixedGrid::setup( AABB sceneArea, unsigned partition )
{
	_scene = sceneArea;
	_partition = partition;

	DynamicArray< SceneNode* > emptyArray;
	_grid.resize( partition * partition, emptyArray );

	_gridRect.topLeft().x = 0;
	_gridRect.topLeft().y = 0;
	_gridRect.lengthX() = sceneArea.lengthX() / partition;
	_gridRect.lengthY() = sceneArea.lengthY() / partition;
}

void FixedGrid::addSceneNode( SceneNode* node )
{
	getNodeIndexs( node, leftIndex, rightIndex, topIndex, bottomIndex );

	for( int i = leftIndex; i <= rightIndex; i++ )
	{
		for( int j = topIndex; j <= bottomIndex; j++ )
		{
			_grid[ i + j * _partition ].pushBack( node );
		}
	}
}

void FixedGrid::removeSceneNode( SceneNode* node )
{
	getNodeIndexs( node, leftIndex, rightIndex, topIndex, bottomIndex );

	for( int i = leftIndex; i <= rightIndex; i++ )
	{
		for( int j = topIndex; j <= bottomIndex; j++ )
		{
			for( unsigned k = 0; k < _grid[ i + j * _partition ].size(); k++)
			{
				if( _grid[ i + j * _partition ][ k ] == node )
				{
					_grid[ i + j * _partition ].erase( k );
					break;
				}
			}
		}
	}
}

void FixedGrid::updateSceneNode( SceneNode* node, Point2D position )
{
	removeSceneNode( node );

	node -> setCenter( position );

	addSceneNode( node );
}

void FixedGrid::getNodeIndexs( SceneNode* node, int& leftIndex, int& rightIndex, int& topIndex, int& bottomIndex )
{
	AABB rect = node -> getRect();

	leftIndex = ( int )( rect.leftBound() / _gridRect.lengthX() );
	rightIndex = ( int )( rect.rightBound() / _gridRect.lengthX() );
	topIndex = ( int )( rect.upperBound() / _gridRect.lengthY() );
	bottomIndex = ( int )( rect.lowerBound() / _gridRect.lengthY() );

	if ( leftIndex <= ( int )_partition - 1 &&
		rightIndex >= 0 &&
		topIndex <= ( int )_partition - 1 &&
		bottomIndex >= 0 )
	{
		leftIndex = Math::cast( 0, ( int )_partition - 1, leftIndex );
		rightIndex = Math::cast( 0, ( int )_partition - 1, rightIndex );
		topIndex = Math::cast( 0, ( int )_partition - 1, topIndex );
		bottomIndex = Math::cast( 0, ( int )_partition - 1, bottomIndex );
	}
	else
	{
		leftIndex = 0;
		topIndex = 0;
		rightIndex = -1;
		bottomIndex = -1;
	}
}

DynamicArray< SceneNode* >& FixedGrid::getColliders( SceneNode* node )
{
	_colliders.clear();

	getNodeIndexs( node, leftIndex, rightIndex, topIndex, bottomIndex );

	for( int i = leftIndex; i <= rightIndex; i++ )
	{
		for( int j = topIndex; j <= bottomIndex; j++ )
		{
			_colliders += _grid[ i + j * _partition ];
		}
	}
	return _colliders;
}

DynamicArray< SceneNode* >& FixedGrid::getColliders( Point2D position )
{
	int xIndex = ( int )( position.x / _gridRect.lengthX() );
	int yIndex = ( int )( position.y / _gridRect.lengthY() );

	return _grid[ xIndex + yIndex * _partition ];
}

DynamicArray< SceneNode* >& FixedGrid::getColliders( Point2D center, int radius )
{
	_colliders.clear();
	return _colliders;
}

DynamicArray< SceneNode* >& FixedGrid::getColliders( Point2D topLeft, Point2D bottomRight )
{
	_colliders.clear();

	leftIndex = ( int )( topLeft.x / _gridRect.lengthX() );
	rightIndex = ( int )( bottomRight.x / _gridRect.lengthX() );
	topIndex = ( int )( topLeft.y / _gridRect.lengthY() );
	bottomIndex = ( int )( bottomRight.y / _gridRect.lengthY() );

	for( int i = leftIndex; i <= rightIndex; i++ )
	{
		for( int j = topIndex; j <= bottomIndex; j++ )
		{
			_colliders += _grid[ i + j * _partition ];
		}
	}
	return _colliders;
}

void FixedGrid::render( Camera camera )
{
	for( unsigned i = 0; i < _grid.size(); i++ )
	{
		for( unsigned j = 0; j < _grid[ i ].size(); j++ )
		{
			_grid[ i ][ j ] -> render( camera );
		}
	}
}
