#include "SceneManager.h"

SceneManager* SceneManager::_instance = NULL;

SceneManager::SceneManager()
{
	_sceneGraph = NULL;
}

SceneManager::~SceneManager()
{
    while( _tickables.size() > 0 )
    {
        delete _tickables[ 0 ];
        _tickables.erase( 0 );
    }
    delete _sceneGraph;
}

SceneManager* SceneManager::getInstance()
{
	if( _instance == NULL )
	{
		_instance = new SceneManager;
	}

	return _instance;
}

void SceneManager::tick( float dt )
{
    if( !_timer.isStarted() )
    {
        int interval = ( int )( dt * 1000 );
        _timer.setInterval( interval );
        _timer.start();
    }

    if( _timer.isTimeout() )
    {
        for( unsigned i = 0; i < _tickables.size(); i++)
        {
            _tickables[ i ] -> preTick( dt );
        }

        for( unsigned i = 0; i < _tickables.size(); i++)
        {
            _tickables[ i ] -> tick( dt );
        }

        for( unsigned i = 0; i < _tickables.size(); i++)
        {
            _tickables[ i ] -> postTick( dt );
        }

        while( !_rmTickables.empty() )
        {
            for( unsigned i = 0; i < _tickables.size(); i++)
            {
                if( _tickables[ i ] == _rmTickables.front() )
                {
                    _rmTickables.pop();
                    delete _tickables[ i ];
                    _tickables.erase( i );
                    if( _rmTickables.empty() )
                    {
                        break;
                    }
                    i--;
                }
            }
        }

        _timer.stop();
    }
}

void SceneManager::render( Camera camera )
{
    _sceneGraph -> render( camera );
}

void SceneManager::addTickable( ITickable* tickable )
{
	_tickables.pushBack( tickable );
}

void SceneManager::addSceneNode( SceneNode* node )
{
	_sceneGraph -> addSceneNode( node );
}

void SceneManager::removeSceneNode( SceneNode* node )
{
	_sceneGraph -> removeSceneNode( node );
}

void SceneManager::removeTickable( ITickable* tickable )
{
    _rmTickables.push( tickable );
}

void SceneManager::setSceneGraph( ISceneGraph* sceneGraph )
{
	_sceneGraph = sceneGraph;
}

ISceneGraph* SceneManager::getSceneGraph()
{
	return _sceneGraph;
}
