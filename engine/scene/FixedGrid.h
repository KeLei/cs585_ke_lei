#pragma once

#include "ISceneGraph.h"
#include "../math/Math.h"

class FixedGrid : public ISceneGraph
{

private:

	AABB										_scene;		// Area of the scene
	AABB										_gridRect;	// AABB area of each grid
	DynamicArray< DynamicArray< SceneNode* > >	_grid;		// Fixed grid of the scene
	DynamicArray< SceneNode* >	                _colliders;	// Colliders of a certain area
	unsigned									_partition;	// Partition of the scene( i.e. the scene is seperated into partition * partition cells)

	// Variable indexs for inserting node and search colliders
	int leftIndex;
	int rightIndex;
	int topIndex;
	int bottomIndex;

public:

	FixedGrid();

	~FixedGrid();

	/*
	Function:	Set up the scene
	Return:		void
	Parameters:	sceneArea - range of the scene
				partition - partition of the scene
	*/
	void setup( AABB sceneArea, unsigned partition );

	void addSceneNode( SceneNode* node );

	void removeSceneNode( SceneNode* node );

	void updateSceneNode( SceneNode* node, Point2D position );

	void getNodeIndexs( SceneNode* node, int& leftIndex, int& rightIndex, int& topIndex, int& bottomIndex );

	DynamicArray< SceneNode* >& getColliders( SceneNode* node );

	DynamicArray< SceneNode* >& getColliders( Point2D position );

	DynamicArray< SceneNode* >& getColliders( Point2D center, int radius );

	DynamicArray< SceneNode* >& getColliders( Point2D topLeft, Point2D bottomRight );

	void render( Camera camera );
};
