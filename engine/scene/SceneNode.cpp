#include "SceneNode.h"

SceneNode::SceneNode()
{
	_collidable = NULL;
}

SceneNode::~SceneNode()
{
	if( _collidable != NULL )
	{
		delete _collidable;
	}
}

AABB SceneNode::getRect()
{
	return _rect;
}

const Point2D SceneNode::getTopLeft()
{
    return _rect.topLeft();
}

const Point2D SceneNode::getCenter()
{
	_center.x = _rect.topLeft().x + _rect.lengthX() / 2.0f;
	_center.y = _rect.topLeft().y + _rect.lengthY() / 2.0f;

	return _center;
}

const unsigned SceneNode::getId()
{
	return _id;
}

ICollidable* SceneNode::getCollidable()
{
	return _collidable;
}

IDrawable* SceneNode::getDrawable()
{
    return _drawable;
}

void SceneNode::setNode( AABB rect )
{
	_rect = rect;
}

void SceneNode::setCenter( Point2D center )
{
	getCenter();

	float offsetX = center.x - _center.x;
	float offsetY = center.y - _center.y;

	_rect.topLeft().x += offsetX;
	_rect.topLeft().y += offsetY;
	_center = center;
}

void SceneNode::setId( unsigned id )
{
	_id = id;
}

void SceneNode::setCollidable( ICollidable* collidable )
{
	_collidable = collidable;
}

void SceneNode::setDrawable( IDrawable* drawable )
{
    _drawable = drawable;
}

void SceneNode::render( Camera camera )
{
    _drawable -> setPosition( _rect.topLeft().x, _rect.topLeft().y );
    _drawable -> render( camera );
}
