#pragma once

#include "IDrawable.h"
#include "../structures/Vector2D.h"
#include "../camera/Camera.h"

class ASCIIDrawable : public IDrawable
{

public:

    ASCIIDrawable();
    ASCIIDrawable( ASCIIDrawable& drawable );
    ~ASCIIDrawable();

    /*
    Function:	Get color index of this drawable
    Return:		Color index of the drawable
    Parameters:	void
    */
    int getColor();

    /*
    Function:	Get character of this drawable
    Return:		Character of this drawable
    Parameters:	void
    */
    char getCharacter();

    /*
    Function:	Set character
    Return:		void
    Parameters:	character - character of this drawable
    */
    void setCharacter(char charator );

    /*
    Function:	Set color index
    Return:		void
    Parameters:	color - color index of this drawable
    */
    void setColor(int color );

    /*
    Function:	Set attribute
    Return:		void
    Parameters:	attribute - attribute of this drawable
    */
    void setAttribute( int attribute );

    /*
    Function:	Set window
    Return:		void
    Parameters:	win - window of this drawable
    */
    void setWindow( WINDOW* win );

    /*
    Function:	Set position
    Return:		void
    Parameters:	x - x-coordinate
                y - y-coordinate
    */
    void setPosition( int x, int y );

    /*
    Function:	Add an attribute to this drawable
    Return:		void
    Parameters:	attribute - attribute to be added
    */
    void attributeOn( int attribute );

    /*
    Function:	Remove an attribute from this drawable
    Return:		void
    Parameters:	attribute - attribute to be removed
    */
    void attributeOff( int attribute );

    /*
    Function:	Render ths drawable
    Return:		void
    Parameters:	camera - the camera offset
    */
    void render( Camera camera );

private:

    int         _color;     // Color of the charator
    char        _character; // Character to be rendered
    int         _attribute; // Property of the character
    WINDOW*     _win;       // Window to be rendered to
    Vector2D    _position;  // Position to be rendered
};
