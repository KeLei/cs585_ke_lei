#pragma once

#include "SceneNode.h"
#include "../structures/DynamicArray.h"
#include "../structures/Vector2D.h"

class ISceneGraph
{

public:

	/*
	Function:	Add a scene node to the scene graph
	Return:		void
	Paraments:	node - node to be added to the scene
	*/
	virtual void addSceneNode( SceneNode* node ) = 0;

	/*
	Function:	Remove a node from the scene graph
	Return:		void
	Paraments:	node - node to be removed
	*/
	virtual void removeSceneNode( SceneNode* node ) = 0;

	/*
	Function:	Update a node to its new position
	Return:		void
	Paraments:	node - node to be updated
				position - new position of the node
	*/
	virtual void updateSceneNode( SceneNode* node, Point2D position ) = 0;

	/*
	Function:	Get colliders around a certain node
	Return:		An array of the nodes that may have collision to the node
	Paraments:	node - The node that may involve in collision
	*/
	virtual DynamicArray< SceneNode* >& getColliders( SceneNode* node ) = 0;

	/*
	Function:	Get colliders around a certain position
	Return:		An array of the nodes that may have collision at the position
	Paraments:	position - The point in the collision area
	*/
	virtual DynamicArray< SceneNode* >& getColliders( Point2D position ) = 0;

	/*
	Function:	Get colliders in a circle
	Return:		An array of the nodes that may have collision in the circle
	Paraments:	center - center of the circle
				radius - radius of the circle
	*/
	virtual DynamicArray< SceneNode* >& getColliders( Point2D center, int radius ) = 0;

	/*
	Function:	Get colliders in a rectangle
	Return:		An array of the nodes that may have collision in the rectangle area
	Paraments:	topLeft - top-left point of the rectangle
				bottomRight - bottom-right point of hte rectangle
	*/
	virtual DynamicArray< SceneNode* >& getColliders( Point2D topLeft, Point2D bottomRight ) = 0;

	/*
	Function:	Render the scene
	Return:		void
	Paraments:	void
	*/
	virtual void render( Camera camera ) = 0;

	virtual ~ISceneGraph(){}
};
