#pragma once


#include "ISceneGraph.h"
#include "ITickable.h"
#include "FixedGrid.h"
#include "../structures/Queue.h"
#include "../timer/Timer.h"
//#include "../event/Dispatcher.h"

class SceneManager
{

public:

	static SceneManager* getInstance();

	/*
	Function:	Do logic
	Return:		void
	Parameters:	dt - game speed control
	*/
	void tick( float dt );

	/*
	Function:	Draw the scene
	Return:		void
	Parameters:
	*/
	void render( Camera camera );

	/*
	Function:	Add a tickable class to the list
	Return:		void
	Parameters:	tickable - tickable to be added
	*/
	void addTickable( ITickable* tickable );

	/*
	Function:	Add a node to a scene
	Return:		void
	Parameters:	node to be added
	*/
	void addSceneNode( SceneNode* node );

	/*
	Function:	Remove a scene node from the scene
	Return:		void
	Parameters:	node - the node to be removed
	*/
	void removeSceneNode( SceneNode* node );

	/*
	Function:	Remove tickable from the scene
	Return:		void
	Parameters:	tickable - tickable to be removed
	*/
	void removeTickable( ITickable* tickable );

	/*
	Function:	Set scene's graph
	Return:		void
	Parameters:	sceneGraph - scene graph of the scene
	*/
	void setSceneGraph( ISceneGraph* sceneGraph );

	/*
	Function:	Get scene graph
	Return:		scene graph of the scene
	Parameters:	void
	*/
	ISceneGraph* getSceneGraph();

	~SceneManager();

private:

	static SceneManager*		    _instance;		// instance of the scene manager
	DynamicArray< ITickable* >	    _tickables;		// Array of tickables in the scene
	Queue< ITickable* >	            _rmTickables;   // Queue of tickables to be removed in the scene
	//DynamicArray< Dispatcher* >	_dispatchers;	// Array of dispatchers in the scene
	ISceneGraph*				    _sceneGraph;	// Scene graph of the scene
	Timer                           _timer;         // Timer of the ticks


	SceneManager();


};
