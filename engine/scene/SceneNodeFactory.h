#pragma once

#include "SceneNode.h"

class SceneNodeFactory
{
public:

	~SceneNodeFactory(){}

	/*
	Function:	Get a scene node
	Return:		The new node
	Parameters:	void
	*/
	SceneNode* getSceneNode();

	/*
	Function:	Get a scene node with certain area
	Return:		The new node
	Parameters:	AABB of the scene node
	*/
	SceneNode* getSceneNode( int x, int y, int width, int height );

	// Get instance of the Factory
	static SceneNodeFactory* getInstance();

private:

	static SceneNodeFactory*	_instance;	// Instance of the factory
	static unsigned				_id;		// Id of a node

	SceneNodeFactory(){}

};
