#include "SceneNodeFactory.h"

SceneNodeFactory* SceneNodeFactory::_instance = NULL;
unsigned SceneNodeFactory::_id = 0;

SceneNodeFactory* SceneNodeFactory::getInstance()
{
	if( _instance == NULL )
	{
		_instance = new SceneNodeFactory;
	}
	return _instance;
}

SceneNode* SceneNodeFactory::getSceneNode()
{
	SceneNode* node = new SceneNode;
	node -> setId( _id );
	_id++;
	return node;
}

SceneNode* SceneNodeFactory::getSceneNode( int x, int y, int width, int height )
{
	SceneNode* node = new SceneNode;
	node -> setNode( AABB( x, y, width, height ) );
	node -> setId( _id );
	_id++;
	return node;
}
