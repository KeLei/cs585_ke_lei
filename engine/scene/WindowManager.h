
#include <ncurses.h>
#include "../structures/Hashmap.h"


class WindowManager
{
public:


    /*
    Function:	Get instance of WindowManager
    Return:		Instance of the WindowManager
    Parameters:	void
    */
    static WindowManager* getInstance();

    /*
    Function:	Add a window to the manager
    Return:		void
    Parameters:	name - name of the window
                height - height of the window
                width - width of the window
                startX - top left corner x-coordinate of the window
                startY - top left corner y-coordinate of the window
    */
    void addWin( std::string name, int height, int width, int startX, int startY );

    /*
    Function:	Update a window to a new position
    Return:		void
    Parameters:	name - name of the window
                height - height of the window
                width - width of the window
                startX - top left corner x-coordinate of the window
                startY - top left corner y-coordinate of the window
    */
    void updateWin( std::string name, int height, int width, int startX, int startY );

    /*
    Function:	Remove a window from the manager
    Return:		void
    Parameters:	name - name of the window
    */
    void removeWin( std::string name );

    /*
    Function:	Get a window
    Return:		window
    Parameters:	name - name of the window
    */
    WINDOW* getWin( std::string name );

    /*
    Function:	Display a window and its content
    Return:		void
    Parameters:	name - name of the window
    */
    void displayWin( std::string name );

    /*
    Function:	Draw the window frame
    Return:		void
    Parameters:	name - name of the window
    */
    void drawWin( std::string name );

    /*
    Function:	Clear window content
    Return:		void
    Parameters:	name - name of the window
    */
    void clearWin( std::string name );

    ~WindowManager();


private:

    WindowManager() {}

    /*
    Function:	Destory a window
    Return:		void
    Parameters:	win - window to be destoried
    */
    void _destoryWin( WINDOW* win );

    static WindowManager*   _instance;  // Instance of the WindowManager
    Hashmap< WINDOW* >      _windows;   // Hashmap pf the windows
};
