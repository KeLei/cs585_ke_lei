#pragma once

#include <ncurses.h>
#include "../camera/Camera.h"

class IDrawable
{
public:

    /*
    Function:	Render the drawable with camera location
    Return:		Character of this drawable
    Parameters:	void
    */
	virtual void render( Camera camera ) = 0;

    /*
    Function:	Set position
    Return:		void
    Parameters:	x - x-coordinate
                y - y-coordinate
    */
	virtual void setPosition( int x, int y ) = 0;

    /*
    Function:	Set attribute
    Return:		void
    Parameters:	attribute - attribute of this drawable
    */
    virtual void setWindow( WINDOW* win ) = 0;

    /*
    Function:	Add an attribute to this drawable
    Return:		void
    Parameters:	attribute - attribute to be added
    */
    virtual void attributeOn( int attribute ) = 0;

    /*
    Function:	Remove an attribute from this drawable
    Return:		void
    Parameters:	attribute - attribute to be removed
    */
    virtual void attributeOff( int attribute ) = 0;

	virtual ~IDrawable(){}
};
