#include "Debug.h"

Debug* Debug::_instance = NULL;

Debug* Debug::getInstance()
{
	if( _instance == NULL )
	{
		_instance = new Debug;
	}
	return _instance;
}

Debug::Debug()
{
	_channels = new bool[ NUM_OF_CHANNELS ];
	for( unsigned i = 0; i < NUM_OF_CHANNELS; i++)
	{
		_channels[ i ] = false;
	}

	_channelNames = new std::string[ NUM_OF_CHANNELS ];
	_channelNames[ 0 ] = "Game play";
	_channelNames[ 1 ] = "Asset";
	_channelNames[ 2 ] = "Input";
	_channelNames[ 3 ] = "Unit test";

	_systemOn = true;
}

Debug::~Debug()
{
	delete _instance;
	delete _channels;
}

void Debug::openChannel( const int channel )
{
	_channels[ channel ] = true;
}

void Debug::closeChannel( const int channel )
{
	_channels[ channel ] = false;
}

void Debug::addSource( std::string sourceName, std::string path )
{
    _fileSources.insert( sourceName, path );

	std::ofstream file( path );
	assert( file.is_open() );
}

void Debug::removeSource( std::string sourceName )
{
    _fileSources.erase( sourceName );
}

void Debug::log( const int channel, const char* message, ... )
{
	if( _channels[ channel ] && _systemOn )
	{
		va_list args;
		va_start( args, message );
		std::cout << "[" << _channelNames[ channel ] << "]";
		vprintf( message, args );
		std::cout << std::endl;
		va_end( args );
	}
}

void Debug::openSystem()
{
	_systemOn = true;
}

void Debug::closeSystem()
{
	_systemOn = false;

	for( unsigned i = 0; i < NUM_OF_CHANNELS; i++)
	{
		_channels[ i ] = false;
	}
}

void Debug::log( std::string sourceName, const int channel, const char* message, ... )
{
	std::ofstream file;
	file.open( _fileSources[ sourceName ], std::ios::app );

	assert( file.is_open() );

	if( _channels[ channel ] && _systemOn )
	{
		va_list args;
		va_start( args, message );
        unsigned count = vsnprintf( NULL, 0, message, args );
        char* cBuf = new char[ count + 1 ];
		vsnprintf( cBuf, count + 1, message, args );
		va_end( args );

		file << "[" << _channelNames[ channel ] << "]";
		file.write( cBuf, count );
		file << std::endl;
	}
}
