#pragma once

#include "../structures/Hashmap.h"
#include <iostream>
#include <fstream>
#include <stdarg.h>
#include <string>

/*
Debug class for logging
*/

class Debug
{

// Macro definition of channels
#define GAMEPLAY	0
#define ASSET		1
#define INPUT		2
#define UNITTEST	3

#define NUM_OF_CHANNELS 4


public:

	/*
	Function:	Get an instance of the debug channel
	Return:		Pointer to the instance this class
	Parameters:	void
	*/
	static Debug* getInstance();

	/*
	Function:	Log message through a channel
	Return:		void
	Parameters:	channel - channel to be logged
				message - formated message to log
	*/
	void log( const int channel, const char* message, ... );

	/*
	Function:	Log message through a channel to a file source
	Return:		void
	Parameters:	fileName - source for logging
				channel - channel to be logged
				message - formated message to log
	*/
	void log( std::string sourceName, const int channel, const char* message, ... );

	/*
	Function:	Add a source for logging
	Return:		void
	Parameters:	sourceName - Name of the source to be logged
                path - path of the file source
	*/
	void addSource( std::string sourceName, std::string path );

	/*
	Function:	Remove a source for logging
	Return:		void
	Parameters:	sourceName - Name of the source to be removed
	*/
	void removeSource( std::string sourceName );

	/*
	Function:	Open a channel for logging
	Return:		void
	Parameters:	channel - channel name
	*/
	void openChannel( const int channel );

	/*
	Function:	Close a channel for logging
	Return:		void
	Parameters:	channel - channel name
	*/
	void closeChannel( const int channel );

	/*
	Function:	Open debug system
	Return:		void
	Parameters:	void
	*/
	void openSystem();

	/*
	Function:	Close debug system
	Return:		void
	Parameters:	void
	*/
	void closeSystem();

	/*
	Function:	Destructor, delete instance
	Parameters:	void
	*/
	~Debug();

private:

	/*
	Function:	Contructor, initialize all channels
	Parameters:	void
	*/
	Debug();

	static Debug*				_instance;		// Pointer to a unique instance of this singleton class
	bool*						_channels;		// An array for channels
	bool						_systemOn;		// Bool value determine whether the debugging system is working
	std::string*				_channelNames;	// An array of channel names
	Hashmap< std::string >      _fileSources;   // Map of log file sources
};
