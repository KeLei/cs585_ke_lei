#include "Random.h"

Random* Random::_instance = NULL;
unsigned Random::_seed = ( unsigned )std::chrono::system_clock::now().time_since_epoch().count();
std::minstd_rand0 Random::_generator( _seed );

Random::Random()
{}

Random* Random::getInstance()
{
	if( _instance == NULL )
	{
		_instance = new Random;
	}
	return _instance;
}

float Random::getRandom( float range )
{
	return ( float )( _generator() ) / ( float ) _generator.max() * range;
}

float Random::getRandom( float from, float to )
{
	float range = to - from;
	return ( float )( _generator() ) / ( float ) _generator.max() * ( float )range + from;
}

float Random::getRandom( float from, float to, float step )
{
	assert( step != 0 );

	// How many steps are there
	int count = ( int )( to - from ) / ( int )step;

	// Get integer ranging in [0, count)
	int stepCount = ( int )( ( float )( ( _generator() ) * count ) / ( float ) _generator.max() );

	return from + step * stepCount;
}
