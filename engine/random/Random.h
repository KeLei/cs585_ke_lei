#pragma once

#include <chrono>
#include <iostream>
#include <cassert>
#include <random>

/*
Random class for generating random floats
*/
class Random
{

private:

	Random();
	static Random*				_instance;	// Instance of the class
	static unsigned				_seed;		// Seed of randoms
	static std::minstd_rand0	_generator;	// Generator of randoms

public:

	static Random* getInstance();

	/*
	Function:	Get random float from 0 to range
	Return:		random float between [0, range)
	Parameters:	range - range of the random
	*/
	float getRandom( float range );							

	/*
	Function:	Get random float range in [from,to)
	Return:		random float between [from, to)
	Parameters:	from - start range of the random
				to - end range of the random
	*/
	float getRandom( float from, float to );				
	
	/*
	Function:	Get random float range in [from,to) with step
	Return:		random float
	Parameters:	from - start range of the random
				to - end range of the random
				step - step of the randoms
	*/
	float getRandom( float from, float to, float step );	
};