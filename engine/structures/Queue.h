#pragma once

#include "DynamicArray.h"

// Queue based on linked list
template< class T >
class Queue
{

private:

	// List node for linked list
	struct _Node
	{
		T _data;
		_Node* _next;

		// Constructor
		_Node( T data ): _data( data ), _next( NULL ){}
	};

	_Node*		_head;	// Head of the queue
	_Node*		_tail;	// Tail of the queue
	unsigned	_size;	// Size of the queue

public:

	/*
	Constructor:Initialize all pointers to null, size to 0
	Parameters:	void
	*/
	Queue();

	/*
	Destructor:	Do nothing
	*/
	~Queue();

	/*
	Function:	Add a node with data to the tail of the queue
	Return:		void
	Parameters:	data - data to be added
	*/
	void push( T data );

	/*
	Function:	Pop a node from head of the queue
	Return:		void
	Parameters:	void
	*/
	void pop();

	/*
	Function:	Get the size of te queue
	Return:		Size of the queue
	Parameters:	void
	*/
	unsigned size();

	/*
	Function:	Get the value of the node from the head of the queue
	Return:		Value stored in front of the queue
	Parameters:	void
	*/
	T& front();

	/*
	Function:	Get the value of the node from the tail of the queue
	Return:		Value stored in tail of the queue
	Parameters:	void
	*/
	T& back();

	/*
	Function:	Test whether the queue is empty
	Return:		true if it's empty, otherwise false
	Parameters:	void
	*/
	bool empty();

};


template< class T >
Queue< T >::Queue()
{
	_head = NULL;
	_tail = NULL;
	_size = 0;
}

template< class T >
Queue< T >::~Queue()
{
	while( _head != NULL )
	{
		pop();
	}
}

template< class T >
void Queue< T >::push( T data )
{
	if( _head == NULL )
	{
		_head = new _Node( data );
		_tail = _head;
	}
	else
	{
		_tail -> _next = new _Node( data );
		_tail = _tail -> _next;
	}

	_size++;
}

template< class T >
void Queue< T >::pop()
{
	assert( _head != NULL );
	if( _head != _tail )
	{
		_Node* temp = _head;
		_head= _head-> _next;
		delete temp;
		_size--;
	}
	else
	{
		delete _head;
		_head = NULL;
		_tail = _head;
		_size = 0;
	}
}

template< class T >
T& Queue< T >::front()
{
	assert( _head != NULL );
	return _head -> _data;
}

template< class T >
T& Queue< T >::back()
{
	assert( _tail != NULL );
	return _tail -> _data;
}

template< class T >
unsigned Queue< T >::size()
{
	return _size;
}

template< class T >
bool Queue< T >::empty()
{
	return( _size == 0 );
}
