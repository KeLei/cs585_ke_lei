#pragma once

#include "DynamicArray.h"

// Stack based on dynamic array
template< class T >
class Stack
{
private:

	DynamicArray< T > _stack;	// Dynamic array to store the stack

public:

	// Constructor: Do nothing
	Stack(){}

	// Destructor: Do nothing
	~Stack(){}

	/*
	Function:	Push a element into the stack
	Return:		void
	Parameters:	element - element to be pushed
	*/
	void push( T element );

	/*
	Function:	Pop element front top of the stack
	Return:		void
	Parameters:	void
	*/
	void pop();

	/*
	Function:	Get the value from the top pf the stack
	Return:		void
	Parameters:	void
	*/
	T top();

	/*
	Function:	Test whether the stack is empty
	Return:		void
	Parameters:	true if its empty, otherwise false
	*/
	bool empty();

	/*
	Function:	Get size of the stack
	Return:		Size of the stack
	Parameters:	void
	*/
	unsigned size();
};


template< class T >
void Stack<T>::push( T element )
{
	_stack.pushBack( element );
}

template< class T >
bool Stack< T >::empty()
{
	return _stack.isEmpty();
}

template< class T >
void Stack< T >::pop()
{
	_stack.popBack();
}

template< class T >
unsigned Stack< T >::size()
{
	return _stack.size();
}

template< class T >
T Stack< T >::top()
{
	return _stack[ _stack.size() - 1 ];
}
