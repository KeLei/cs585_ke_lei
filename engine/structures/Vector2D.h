#pragma once

#include <iostream>

class Vector2D
{

public:

	// x and y coordinate of the vector
	float x;
	float y;

	/*
	Function:	Constructor, initialize vector to point ( 0, 0 )
	Paraments:	void
	*/
	Vector2D();

	/*
	Function:	Constructor, initialize vector to point ( x, y )
	Paraments:	x - x coordinate of the point
				y - y coordinate of the point
	*/
	Vector2D( float x, float y );

	/*
	Function:	Destructor, do nothing
	Paraments:	void
	*/
	~Vector2D();

	/*
	Function:	Set a vector position
	Return:		void
	Paraments:	x - x coordinate of the new position
				y - y coordinate of the new position
	*/
	void setVector2D( float x, float y );

	/*
	Function:	Add two vectors
	Return:		Result of the addition of two vectors
	Paraments:	rhsVec - vector to add
	*/
	Vector2D operator+ ( Vector2D rhsVec );

	/*
	Function:	Minus two vectors
	Return:		Result of this minus rhsVec
	Paraments:	rhsVec - subtrahend
	*/
	Vector2D operator- ( Vector2D rhsVec );

	/*
	Function:	Let this vector equals to another vector
	Return:		void
	Paraments:	vecObj - the vector will be used
	*/
	void operator= ( Vector2D& vecObj );

	/*
	Function:	Test if two vectors are the same
	Return:		true - two vectors are the same, else false
	Paraments:	vecObj - the vector will be used for test
	*/
	bool operator== ( const Vector2D vecObj );

	/*
	Function:	Compute the cross product of two vectors
	Return:		result of the cross computation
	Paraments:	vecObj - another vector in the cross computation
	*/
	float CrossProduct( Vector2D vecObj );
};

// Give Vector2D a different meaning
typedef Vector2D Point2D;
