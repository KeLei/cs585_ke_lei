#pragma once

#include "Vector2D.h"

/*
Axis alined bounding box
*/
class AABB
{

private:

	Point2D	_topLeft;	// Top left point of the AABB
	float	_xLength;	// Width of the AABB
	float	_yLength;	// Height of the AABB

public:

	/*
	Function:	Constructor
	Parameters:	void
	*/
	AABB();

	/*
	Function:	Constructor
	Parameters:	topLeft - top left point of the AABB
				xLength - width of the AABB
				yLength - height of the AABB
	*/
	AABB( Point2D topLeft, float xLength, float yLength  );

	/*
	Function:	Constructor
	Parameters:	cornerX1 - x axis of top left point of the AABB
				cornerY1 - y axis of top left point of the AABB
				xLength - width of the AABB
				yLength - height of the AABB
	*/
	AABB( float cornerX1, float cornerY1, float xLength, float yLength );

	~AABB(){}

	/*
	Function:	Get the top left point of the AABB
	Return:		Reference to the top left point
	Parameters:	void
	*/
	Point2D& topLeft();

	/*
	Function:	Get the bottom right point of the AABB
	Return:		Bottom right point
	Parameters:	void
	*/
	Point2D bottomRight();

	/*
	Function:	Get the upper bound of the AABB
	Return:		Upper bound
	Parameters:	void
	*/
	float upperBound();

	/*
	Function:	Get the lower bound of the AABB
	Return:		Lower bound
	Parameters:	void
	*/
	float lowerBound();

	/*
	Function:	Get left bound of the AABB
	Return:		Left bound
	Parameters:	void
	*/
	float leftBound();

	/*
	Function:	Get right bound of the AABB
	Return:		Right bound
	Parameters:	void
	*/
	float rightBound();

	/*
	Function:	Get width of the AABB
	Return:		Width of the AABB
	Parameters:	void
	*/
	float& lengthX();

	/*
	Function:	Get height of the AABB
	Return:		Height of AABB
	Parameters:	void
	*/
	float& lengthY();

	/*
	Function:	Assign operator
	Return:		void
	Parameters:	rect - AABB to be assigned
	*/
	void operator= ( AABB rect );
};