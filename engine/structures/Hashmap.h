#pragma once

#include "DynamicArray.h"
#include <string>

// Hashmap based on Trie structure, use string as index, store type not limited
template< class T >
class Hashmap
{

private:

	// Node structure for each node in the trie.
	struct _Node
	{
		char		index;		// Charator index of this node
		unsigned	size;		// Size of children
		T			data;		// Data stored in this node
		_Node**		children;	// Child nodes stored in a dynamic array
		bool		hasData;	// Whether the node has a valid data

		// Constructors
		_Node() : index( '\0' ), size( 0 ), hasData( false )
		{
			children = new _Node*[ 128 ];
			for( unsigned i = 0; i < 128; i++ )
			{
				children[ i ] = NULL;
			}
		}
		_Node( char index ) : index( index ), size( 0 ), hasData( false )
		{
			children = new _Node*[ 128 ];
			for( unsigned i = 0; i < 128; i++ )
			{
				children[ i ] = NULL;
			}
		}
		_Node( char index, T data ) : index( index ), size( 0 ), data( data ), hasData( true )
		{
			children = new _Node*[ 128 ];
			for( unsigned i = 0; i < 128; i++ )
			{
				children[ i ] = NULL;
			}
		}

		// Destructor
		~_Node()
		{
			delete children;
		}
	};

	_Node*						_root; // Root node of the trie
	DynamicArray< std::string >	_keys; // Array of keys in the map

	/*
	Function:	Search value using key as index under node and its children
	Return:		Key value
	Parameters:	key - key of the value
	node - root of sub-tree to be searched
	*/
	T& _getValue( const char* key, _Node* node );

	/*
	Function:	Insert value using key as index to node or its children
	Return:		void
	Parameters:	key - key of the value
	value - value to be inserted
	node - root of sub-tree to be inserted
	*/
	void _insertToNode( const char* key, T value, _Node* node );

	/*
	Function:	Erase value from node or its children
	Return:		void
	Parameters:	key - character used to search
	node - child list to be searched
	*/
	void _eraseFromNode( const char* key, _Node* node );

	/*
	Function:	Search if charator c exist in the child list
	Return:		void
	Parameters:	node - node to be cleared
	*/
	void _clearNode( _Node* node );

public:

	/*
	Constructor:Initialize root of trie
	Parameters:	void
	*/
	Hashmap();

	/*
	Destructor:	Clear the whole trie
	*/
	~Hashmap();

	/*
	Function:	Insert a value into the tire with key
	Return:		void
	Parameters:	key - key of the value
	value - value to be inserted
	*/
	void insert( std::string key, T value );

	/*
	Function:	Get a value from its key
	Return:		Key value
	Parameters:	key - key of the value
	*/
	T& operator[]( std::string key );

	/*
	Function:	Erase value with key from the trie
	Return:		void
	Parameters:	key - key of the value
	*/
	void erase( std::string key );

	/*
	Function:	Erase value with key from the trie
	Return:		void
	Parameters:	key - key of the value
	*/
	DynamicArray< std::string >& getKeys();

	/*
	Function:	Destory the trie
	Return:		void
	Parameters:	void
	*/
	void clear();
};

template< class T >
Hashmap< T >::Hashmap()
{
	_root = new _Node;
}

template< class T >
Hashmap< T >::~Hashmap()
{
	clear();
}

template< class T >
void Hashmap< T >::insert( std::string key, T value )
{
	_insertToNode( key.c_str(), value, _root );
	_keys.pushBack( key );
}

template< class T >
void Hashmap< T >::_insertToNode( const char* key, T value, _Node* node )
{
	_Node* currentNode = ( node -> children )[ ( int )*key ];
	if( currentNode != NULL)
	{
		if( *( key + 1 ) == '\0')
		{
			( node -> children )[ ( int )*key ] -> data = value;
			( node -> children )[ ( int )*key ] -> hasData = true;
		}
		else
		{
			_insertToNode( key + 1, value, currentNode );
		}
	}
	else
	{
		node -> size += 1;
		if( *( key + 1 ) == '\0')
		{
			( node -> children )[ ( int )*key ] = new _Node( *key, value );
		}
		else
		{
			( node -> children )[ ( int )*key ] = new _Node( *key );
			_insertToNode( key + 1, value, ( node -> children )[ ( int )*key ] );
		}
	}
}

template< class T >
T& Hashmap< T >::operator[]( std::string key )
{
	return _getValue( key.c_str(), _root );
}

template< class T >
T& Hashmap< T >::_getValue( const char* key, _Node* node )
{
	assert( node != NULL || key != '\0' );

	// Find current node
	_Node* currentNode = ( node -> children )[ ( int )*key ];

	if( currentNode != NULL )
	{
		// The current charator is a child of the node
		if( *( key + 1 ) == '\0' )
		{
			// If at end of the string
			if( currentNode -> hasData )
			{
				// If the node has a valid data
				return currentNode -> data;
			}
			else
			{
				// Not found, just return a invalid value
				return _root -> data;
			}
		}
		else
		{
			return _getValue( key + 1, currentNode );
		}
	}
	else
	{
		// Not found, just return a invalid value
		return _root -> data;
	}
}

template< class T >
void Hashmap< T >::erase( std::string key )
{
	_eraseFromNode( key.c_str(), _root );
	for( int i = 0 ; i < _keys.size(); i++ )
	{
		if( _keys[ i ] == key )
		{
			_keys.erase( i );
			break;
		}
	}
}

template< class T >
void Hashmap< T >::_eraseFromNode( const char* key, _Node* node )
{
	assert( node != NULL || key != '\0' );

	_Node* currentNode = ( node -> children )[ ( int )*key ];

	if( currentNode != NULL )
	{
		if( *( key + 1 ) == '\0' )
		{
			if( currentNode -> size <= 1 )
			{
				delete currentNode;
				( node -> children )[ ( int )*key ] = NULL;
				node -> size--;
			}
			else
			{
				currentNode -> hasData = false;
			}

		}
		else
		{
			_eraseFromNode( key + 1, currentNode );

			if( currentNode -> size == 0 &&
				currentNode -> hasData == false )
			{
				delete currentNode;
				( node -> children )[ ( int )*key ] = NULL;
				node -> size--;
			}
		}
	}

}

template< class T >
DynamicArray< std::string >& Hashmap< T >::getKeys()
{
	return _keys;
}

template< class T >
void Hashmap< T >::clear()
{
	_clearNode( _root );
	_keys.clear();

	_root = new _Node;
}

template< class T >
void Hashmap< T >::_clearNode( _Node* node )
{
	for( unsigned i = 0; i < 128; i++ )
	{
		if( ( node -> children )[ i ] != NULL )
		{
			_clearNode( ( node -> children )[ i ] );
		}
	}
	delete node;
}
