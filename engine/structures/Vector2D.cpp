#include "Vector2D.h"

Vector2D::Vector2D()
{
	x = 0;
	y = 0;
}

Vector2D::Vector2D( float x, float y )
{
	this -> x = x;
	this -> y = y;
}

Vector2D::~Vector2D()
{}

Vector2D Vector2D::operator+( Vector2D rhsObj )
{
	return Vector2D( x + rhsObj.x, y + rhsObj.y );
}

Vector2D Vector2D::operator-( Vector2D rhsObj )
{
	return Vector2D( x - rhsObj.x, y - rhsObj.y );
}

bool Vector2D::operator==( const Vector2D rhsObj )
{
	return x == rhsObj.x && y == rhsObj.y;
}

void Vector2D::operator=( Vector2D& vecObj )
{
	x = vecObj.x;
	y = vecObj.y;
}

void Vector2D::setVector2D( float x, float y )
{
	this -> x = x;
	this -> y = y;
}

float Vector2D::CrossProduct( Vector2D vecObj )
{
	return (x * vecObj.y - vecObj.x * y );
}
