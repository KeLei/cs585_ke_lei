#pragma once

#include <iostream>
#include <cassert>

template< class T >
class DynamicArray
{

private:

	unsigned	_arraySize;				//size of the array
	unsigned	_arrayCapacity;			//capacity of the array
	T*			_array;					//the array head pointer


public:

	DynamicArray();

	/*
	Constructor
	Function:	pre-allocate a certain amount of memory
	Parameters:	arrayCapacity - capacity of the array to be pre-allocated
	*/
	DynamicArray( unsigned arrayCapacity );

	/*
	Constructor
	Function:	Copy a existing array to dynamic array
	Parameters:	originArray - array to be copied
	arraySize - size of the array to be cpoied
	*/
	DynamicArray( T* originArray, unsigned arraySize);

	/*
	Copy Constructor
	Function:	Copy a existing dynamic array to current dynamic array
	Parameters:	arrayObj - dynamic array object to be copied
	*/
	DynamicArray( DynamicArray< T >& arrayObj);

	/*
	Destructor
	Function:	delete all memory uesd.
	*/
	~DynamicArray();

	/*
	Function:	Get Size of the array
	Return:		Size of the array
	Parameters:	void
	*/
	unsigned size();

	/*
	Function:	Get Capacity of the array
	Return:		capacity of the array
	Parameters:	void
	*/
	unsigned capacity();

	/*
	Function:	Determine whether the array is empty
	Return:		true - array is empty
	false - array is not empty
	Parameters:	void
	*/
	bool isEmpty();

	/*
	Function:	resize the array, add 0s if Size is larger; else delete element from tail
	Return:		void
	Parameters:	newSize - excepted
	element - if the size exceed current size, what element will be added to the additional space
	*/
	void resize( unsigned newSize, T element );

	/*
	Function:	reserve space for more elements
	Return:		void
	Parameters:	newCapacity - space needed
	*/
	void reserve( unsigned newCapacity );

	/*
	Function:	Assign value to specified index of array
	Return:		void
	Parameters:	index - which element in the array to be assigned
	element - new value of the index in array
	*/
	void assign( unsigned index, T element );

	/*
	Function:	Add new element to the end of array
	Return:		void
	Parameters:	newElement - element to be added
	*/
	void pushBack( T newElement );

	/*
	Function:	Add new element to the front of array
	Return:		void
	Parameters:	newElement - element to be added
	*/
	void pushFront( T newElement );

	/*
	Function:	Pop out the last element of the array
	Return:		Value of the last element
	Parameters:	void
	*/
	T popBack();

	/*
	Function:	Insert element into specified index
	Return:		void
	Parameters:	index - where in the array to insert
	element - element to be inserted
	*/
	void insert( unsigned index, T element );

	/*
	Function:	Erase element from specified index
	Return:		void
	Parameters:	index - which element in the array to be erased
	*/
	void erase( unsigned index );

	/*
	Function:	Erase element from ranging from fIndex to bIndex
	Return:		void
	Parameters:	fIndex - Elements in array will be erased from this index
	*/
	void erase( unsigned fIndex, unsigned bIndex );


	/*
	Function:	Clear all elements in array
	Return:		void
	Parameters:	void
	*/
	void clear();

	/*
	Function:	Retrive an element in certain index
	Return:		void
	Parameters:	index - index of element to be retrived
	*/
	T at( unsigned index );
	T& operator[] ( unsigned index );
	T operator[] ( unsigned index ) const;

	/*
	Function:	Copy a existing dynamic array to current dynamic array
	Return:		void
	Parameters:	arrayObj - dynamic array object to be copied
	*/
	DynamicArray< T >& operator= ( DynamicArray< T > arrayObj );

	/*
	Function:	Add a dynamic array to the end of this array
	Return:		void
	Parameters:	arrayObj - dynamic array object to be added
	*/
	void operator+= ( DynamicArray< T > & arrayObj );
};



template< class T >
DynamicArray< T >::DynamicArray( )
{
	_arraySize = _arrayCapacity = 0;
	_array = NULL;
}

template< class T >
DynamicArray< T >::DynamicArray( unsigned capacity )
{
	_arraySize = 0;
	_arrayCapacity = capacity;
	_array = new T[ _arrayCapacity ];
}

template< class T >
DynamicArray< T >::DynamicArray( T* originArray, unsigned arraySize )
{
	_arraySize = _arrayCapacity = arraySize;
	_array = new T[ _arrayCapacity ];
	for( unsigned i = 0; i < arraySize; i++)
	{
		_array[ i ] = originArray[ i ];
	}
}

template< class T >
DynamicArray< T >::DynamicArray( DynamicArray< T >& arrayObj)
{
	_arraySize = _arrayCapacity = arrayObj.size();
	_array = new T[ _arrayCapacity ];
	for( unsigned i = 0; i < _arraySize; i++ )
	{
		_array[ i ] = arrayObj._array[ i ];
	}
}

template< class T >
unsigned DynamicArray< T >::size()
{
	return _arraySize;
}

template< class T >
unsigned DynamicArray< T >::capacity()
{
	return _arrayCapacity;
}

template< class T >
bool DynamicArray< T >::isEmpty()
{
	if( _arraySize == 0 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

template< class T >
void DynamicArray< T >::resize( unsigned newSize, T element )
{
	assert( newSize >= 0);
	if( newSize != _arraySize )
	{
		T* oldArray = _array;
		_arrayCapacity = ( _arrayCapacity > newSize ) ? _arrayCapacity : newSize ;
		_array = new T[ _arrayCapacity ];
		for( unsigned i = 0; i < newSize; i++)
		{
			if(i < _arraySize)
			{
				_array[ i ] = oldArray[ i ];
			}
			else
			{
				_array[ i ] = element;
			}
		}
		delete []oldArray;
		_arraySize = newSize;
	}
}

template< class T >
void DynamicArray< T >::reserve( unsigned newCapacity )
{
	assert( newCapacity >= _arraySize );
	if ( newCapacity != _arrayCapacity )
	{
		T* oldArray = _array;
		_array = new T[ newCapacity ];
		for( unsigned i = 0; i < _arraySize; i++)
		{
			_array[ i ] = oldArray[ i ];
		}
		_arrayCapacity = newCapacity;
		delete []oldArray;
	}
}

template< class T >
void DynamicArray< T >::assign( unsigned index, T element )
{
	assert( index >= 0 && index <= _arraySize );
	_array[ index ] = element;
}

template< class T >
void DynamicArray< T >::pushBack( T newElement )
{
	if ( _arrayCapacity == _arraySize )
	{
		reserve( _arrayCapacity * 2 + 1);
	}
	_arraySize++ ;
	_array[ _arraySize- 1 ] = newElement;
}

template< class T >
void DynamicArray< T >::pushFront( T newElement )
{
	if ( _arrayCapacity == _arraySize )
	{
		reserve( _arrayCapacity * 2 + 1);
	}
	_arraySize++ ;

	T* oldArray = _array;
	_array = new T[ _arrayCapacity ];

	for( unsigned i = 1; i < _arraySize; i++)
	{
		_array[ i ] = oldArray[ i - 1 ];
	}

	_array[ 0 ] = newElement;

	delete []oldArray;
}


template< class T >
T DynamicArray< T >::popBack()
{
	assert( _arraySize > 0 );
	T temp = _array[ _arraySize - 1 ];
	resize( --_arraySize, 0 );
	return temp;
}

template< class T >
void DynamicArray< T >::insert( unsigned index, T element )
{
	assert( index <= _arraySize && index >= 0 );
	_arraySize++ ;
	for( unsigned i = _arraySize - 1; i > index; i-- )
	{
		_array[ i ] = _array[ i - 1 ];
	}
	_array[ index ] = element;
}

template< class T >
void DynamicArray< T >::erase( unsigned index )
{
	assert( index <= _arraySize && index >= 0 );
	_arraySize--;
	for( unsigned i = index; i < _arraySize; i++)
	{
		_array[ i ] =  _array[ i + 1 ];
	}
}

template< class T >
void DynamicArray< T >::erase( unsigned fIndex, unsigned bIndex )
{
	assert( fIndex >= 0 && bIndex < _arraySize && fIndex <= bIndex );
	_arraySize = _arraySize - ( bIndex - fIndex );
	for( unsigned i = fIndex; i < _arraySize; i++)
	{
		_array[ i ] =  _array[ i + bIndex - fIndex ];
	}
}

template< class T >
void DynamicArray< T >::clear()
{
	if( _array != NULL )
	{
		delete []_array;
		_array = NULL;
	}
	_arraySize = 0;
	_arrayCapacity = 0;
}

template< class T >
T DynamicArray< T >::at( unsigned index )
{
	assert( index < _arraySize && index >= 0 );
	return _array[ index ];
}

template< class T >
T& DynamicArray< T >::operator[] ( unsigned index )
{
	assert( index < _arraySize && index >= 0 );
	return _array[ index ];
}

template< class T >
T DynamicArray< T >::operator[] ( unsigned index )const
{
	assert( index < _arraySize && index >= 0 );
	return _array[ index ];
}

template< class T >
DynamicArray< T >& DynamicArray< T >::operator = ( DynamicArray< T > arrayObj )
{
	delete []_array;
	_arraySize = arrayObj.size();
	_arrayCapacity = arrayObj.capacity();
	_array = new T[ _arrayCapacity ];
	for( unsigned i = 0; i < _arraySize; i++)
	{
		this -> _array[ i ] = arrayObj._array[ i ];
	}
	return *this;
}

template< class T >
void DynamicArray< T >::operator+= ( DynamicArray< T > & arrayObj )
{
	int _newSize =  _arraySize + arrayObj.size();
	int _newCapacity = _arrayCapacity + arrayObj.capacity();
	T* _oldArray = _array;
	_array = new T[ _newCapacity ];

	for( unsigned i = 0; i < _arraySize; i++)
	{
		_array[ i ] = _oldArray[ i ];
	}
	for( unsigned i = 0; i < arrayObj.size(); i++)
	{
		_array[ i + _arraySize ] = arrayObj._array[ i ];
	}

	_arraySize = _newSize;
	_arrayCapacity = _newCapacity;
}

template< class T >
DynamicArray< T >::~DynamicArray()
{
	delete []_array;
}
