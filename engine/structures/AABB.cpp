#include "AABB.h"

AABB::AABB(){}

AABB::AABB( Point2D tl, float xLength, float yLength )
{
	_topLeft.x = tl.x;
	_topLeft.y = tl.y;
	_xLength = xLength;
	_yLength = yLength;
}

AABB::AABB(  float cornerX1, float cornerY1, float xLength, float yLength )
{
	_topLeft.x = cornerX1;
	_topLeft.y = cornerY1;
	_xLength = xLength;
	_yLength = yLength;
}

Point2D& AABB::topLeft()
{
	return _topLeft;
}

Point2D AABB::bottomRight()
{
	return Point2D( _topLeft.x + _xLength, _topLeft.y + _yLength );
}

float AABB::upperBound()
{
	return _topLeft.y;
}

float AABB::lowerBound()
{
	return _yLength + _topLeft.y;
}

float AABB::leftBound()
{
	return _topLeft.x;
}

float AABB::rightBound()
{
	return  _topLeft.x + _xLength;
}

float& AABB::lengthX()
{
	return _xLength;
}

float& AABB::lengthY()
{
	return _yLength;
}

void AABB::operator= ( AABB rect )
{
	_topLeft = rect.topLeft();
	_xLength = rect._xLength;
	_yLength = rect._yLength;
}
