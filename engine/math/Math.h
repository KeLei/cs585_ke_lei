#pragma once
#include <cassert>

#define MAX_FLOAT 1E+37

namespace Math{

    /*
    Function:	Cast the value into a given range
    Return:		Casted value
    Parameters:	leftBound - left bound of the range
                rightBound - right bound of the range
                value - value to be casted
    */
	template< typename T >
	T cast( T leftBound, T rightBound, T value )
	{
		assert( leftBound <= rightBound );

		if( value < leftBound )
		{
			value = leftBound;
		}

		if( value > rightBound)
		{
			value = rightBound;
		}

		return value;
	}

    /*
    Function:	Get absolute value of a number
    Return:		Absolute value of the number
    Parameters:	value - the number
    */
	template< typename T >
	T abs( T value )
	{
        if( value < ( T )0 )
        {
            value *= ( T )-1;
        }
        return value;
	}
}
