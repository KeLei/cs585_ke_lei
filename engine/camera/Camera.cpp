#include "Camera.h"


void Camera::setCameraRange( AABB cameraRange )
{
    _cameraRange = cameraRange;
}

void Camera::setViewport( AABB visiopnRange )
{
    _viewport = visiopnRange;
}

void Camera::setPosition( Point2D position )
{
    _viewport.topLeft().x = position.x;
    _viewport.topLeft().y = position.y;
}

void Camera::moveCamera( int x, int y )
{
    _viewport.topLeft().x += x;
    _viewport.topLeft().y += y;

    _viewport.topLeft().x = Math::cast( _cameraRange.leftBound(), _cameraRange.rightBound() - _viewport.lengthX(), _viewport.topLeft().x );
    _viewport.topLeft().y = Math::cast( _cameraRange.upperBound(), _cameraRange.lowerBound() - _viewport.lengthY() , _viewport.topLeft().y );
}

AABB Camera::getCameraRange()
{
    return _cameraRange;
}

AABB Camera::getViewport()
{
    return _viewport;
}

const Point2D Camera::getPosition()
{
    return  _viewport.topLeft();
}
