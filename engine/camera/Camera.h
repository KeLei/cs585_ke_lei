#pragma once
#include "../structures/AABB.h"
#include "../math/Math.h"

class Camera
{
public:

    /*
    Function:	Set the range of the camera
    Return:		void
    Parameters:	cameraRange - range box of the camera
    */
    void setCameraRange( AABB cameraRange );

    /*
    Function:	Set viewport of the camera
    Return:		void
    Parameters:	viewport - viewport of the camera
    */
    void setViewport( AABB viewport );

    /*
    Function:	Set position of the camera
    Return:		void
    Parameters:	position - top left corner position of the camera
    */
    void setPosition( Point2D position );

    /*
    Function:	Move camera
    Return:		void
    Parameters:	x - distance to be moved on x axis
                y - distance to be moved on y axis
    */
    void moveCamera( int x, int y );

    /*
    Function:	Get camera range
    Return:		Range of the camera
    Parameters:	void
    */
    AABB getCameraRange();

    /*
    Function:	Get viewport of the camera
    Return:		Viewport of the camera
    Parameters:	void
    */
    AABB getViewport();

    /*
    Function:	Get position of the camera
    Return:		Top eft point of the camera
    Parameters:	void
    */
    const Point2D getPosition();


private:

    AABB     _cameraRange;  // Range of the camera
    AABB     _viewport;     // Viewport of the camera
};
