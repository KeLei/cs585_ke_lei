# CS 585 - Intro to Game Development

## To run:

* Clone the repo, make sure NCURSES is installed in your system

* Make

* Copy assets to your Release directory

* Run

## Style Guide:

### Space and Enter

* All operators should appear with one space on both side.

* Leave one space at each side of the parentheses, unless there's no argument between parentheses.

* Braces should always use a new line which contains only the braces.

### Class, function and variable names

* Classes begin with uppercase alphabet, variables and functions begin with lowercase.

* Private members begin with underscore

* Macro definitions capitalize ALL charators.

* Satisfied all conditions above, for each semantic part of a name, capitalize the first charactor.


### Comments

* Functions should have comments describing their input, output and functionality.

* Variables should habe comments describing what they represent for.

* Comments that starts a new line should use "slash-star-star-slash" style, else use "slash-slash" style. 



## Objections

* Dynamic Array
* Stack
* Queue
* Hashmap
* Unit Test Frame
* Debug Channel
* Scene Manger
* Event Bus and Dispatcher
* State Machine


## Contact

Ke Lei: leikerex91@gmail.com
