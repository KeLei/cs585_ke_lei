#pragma once

#include "../engine/debug/Debug.h"
#include "../engine/scene/SceneManager.h"
#include "../engine/scene/FixedGrid.h"
#include "../engine/scene/SceneNodeFactory.h"
#include "../engine/timer/Timer.h"
#include "../engine/asset/AssetManager.h"
#include "../engine/input/InputHandler.h"
#include "../engine/camera/Camera.h"
#include "../engine/event/ICallback.h"
#include "scene/Terrain.h"
#include "scene/Menu.h"
#include "factory/CharacterFactory.h"
#include "factory/BuildingFactory.h"
#include "event/GameplayEvent.h"
#include "event/SelectionEvent.h"
#include "event/InteractionEvent.h"
#include "event/DeathEvent.h"
#include "event/TransactionEvent.h"
#include "event/BuildEvent.h"
#include "event/MenuSelectionEvent.h"
#include <ncurses.h>

class Level : public ICallback
{
public:

	Level(){}
	~Level(){}

	/*
	Function:	Run the game
	Return:		void
	Parameters:	void
	*/
	void run();

	/*
	Function:	Initialize the game
	Return:		void
	Parameters:	levelPath - configuration fie of the game
	*/
	void initialize( std::string levelPath );

	/*
	Function:	Add a character into the game
	Return:		void
	Parameters:	character - character to be added
	*/
	void addCharacter( Character* character );

	/*
	Function:	Get camera of the scene
	Return:		Pointer to the camera of the scene
	Parameters:	void
	*/
	Camera* getCamera();

	/*
	Function:	Get size of the world
	Return:		Size of the world
	Parameters:	void
	*/
	Vector2D getWorldSize();

	/*
	Function:	Get terrain of the world
	Return:		Terrain of the world
	Parameters:	void
	*/
	Terrain* getTerrain();

	/*
	Function:	Get treasure of the world
	Return:		Treasure of the world
	Parameters:	void
	*/
	int getTreasure();

	/*
	Function:	Change the amount of the treasure
	Return:		void
	Parameters:	amount - amount to be changed
	*/
	void changeTreasure( int amount );

	/*
	Function:	Get a building in the game
	Return:		Pointer to the building
	Parameters:	name - name of the building
	*/
	Building* getBuilding( std::string name );

    // Event call back
	void execute( IEvent* event );


private:

	Terrain        _terrain;        // Holds the terrain
	bool	       _isRunning;	     // Is the game running
	Camera         _camera;         // Camera of the scene
    Vector2D       _worldSize;      // Size of the world
    Vector2D       _viewportSize;   // Size of the viewport
    WINDOW*        _mainWindow;     // Main window of the characters
    WindowManager* _winManager;     // Instance of window manager
    int            _treasure;       // Treasure of the kingdom
    float          _taxRate;

    Hashmap< AABB >             _areas;     // Map of areas in game
    Hashmap< Building* >        _buildings; // Buildings in game
    DynamicArray< Character* >  _dwarves;   // List of dwaves



    // Game states
    class GameplayState : public State
    {

    public:

        GameplayState()
        {
            _name = "gameplay";
            _owner = NULL;
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt ) {}

        void setOwner( Level* owner )
        {
            _owner = owner;
        }

    private:

        Level* _owner;

    }_gameplayState;


    class SelectionState : public State
    {

    public:

        SelectionState()
        {
            _name = "selection";
            _owner = NULL;
            _actor = NULL;
            _tile = NULL;
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt ) {}

        void setOwner( Level* owner )
        {
            _owner = owner;
        }

        void setMode( std::string mode );

    private:

        Level*      _owner;      // Owner of the state
        Actor*      _actor;      // Selected actor
        Actor*      _tile;       // Selected tile
        Point2D     _worldPos;   // World position of cursor
        Point2D     _cursorPos;  // Window position of cursor
        std::string _mode;       // Mode of the selection

    }_selectionState;

    class InteractionState : public State
    {

    public:

        InteractionState()
        {
            _name = "interaction";
            _target = NULL;
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt ) {}

        void setOwner( Level* owner )
        {
            _owner = owner;
        }

        void setTarget( Actor* actor )
        {
            _target = actor;
        }

    private:

        Level*  _owner;
        Actor*  _target;

    }_interactionState;


    class GameStateMachine : public IStateMachine
    {
    public:

        Level* _owner;

        void setOwner( Level* owner )
        {
            _owner = owner;
        }
        void onStateTransition( IEvent* event );
    }_gameStateMachine;
};
