#include "LevelManager.h"

/*
Main game loop
*/
int main()
{
	Level game;
	LevelManager::getInstance() -> setLevel( &game );

    Debug::getInstance() -> openSystem();
    Debug::getInstance() -> openChannel( GAMEPLAY );
    Debug::getInstance() -> openChannel( INPUT );
    Debug::getInstance() -> openChannel( ASSET );
    Debug::getInstance() -> addSource( "main", "gameplay.txt" );
    Debug::getInstance() -> addSource( "input", "input_record.txt" );
    Debug::getInstance() -> addSource( "asset", "asset_loading.txt" );
    Debug::getInstance() -> log( "main", GAMEPLAY, "Loading game" );

	game.initialize( "assets/json/levels/level1.json" );
	game.run();

	return 0;
}
