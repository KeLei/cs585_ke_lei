#include "Level.h"

void Level::run()
{
	// Setup timer
	Timer ogreTimer;
	ogreTimer.setInterval( 4000 );


	// Game loop
	while( _isRunning )
	{
        refresh();
        InputHandler::getInstance() -> update();

        // Refresh game play window
        _winManager -> clearWin( "main" );

        // Draw window border
        _winManager -> drawWin( "info" );
        _winManager -> drawWin( "help" );

        // Render terrain
        _terrain.render( _camera );

        // Run game logic, draw
        _gameStateMachine.preTick( 0.0f );

        // Draw scene
		SceneManager::getInstance() -> render( _camera );

        //mvwprintw( _winManager -> getWin( "info" ), 1, 1, "Ogre: %d    ", ogreTimer.getRemainingTime() / 1000 );

        // Handle user input/change game states
        _gameStateMachine.tick( 0.0f );

        // Display kingdom info
        mvwprintw( _winManager -> getWin( "info" ), 1, 1, "Treasure:%d  ", _treasure );
        mvwprintw( _winManager -> getWin( "info" ), 2, 1, "Dwarf#:%d  ", _dwarves.size() );

        // Print window content into screen
        _winManager -> displayWin( "main" );
        _winManager -> displayWin( "info" );
        _winManager -> displayWin( "help" );
        _winManager -> displayWin( "log" );

		if( InputHandler::getInstance() -> getCurrentKey() == 'q' )
		{
            // Exit game
            _isRunning = false;
		}

	}
    endwin();
}

void Level::initialize( std::string levelPath )
{
	// Game init
	initscr();
    noecho();
    cbreak();
    keypad( stdscr, TRUE );
    nodelay( stdscr, TRUE );
    curs_set( FALSE );
    start_color();

    // Read level config file
    Debug::getInstance() -> log( "asset", ASSET, "Loading level configurations" );
    JsonParser parser;
    JsonValue* level = parser.parseFrom( levelPath );

    // Read terrain and assets
    Debug::getInstance() -> log( "asset", ASSET, "Loading assets" );
    AssetManager::getInstance() -> readAsset( level -> getValueMap()[ "assets" ] -> getStringValue() );

    // Set window manager
    _winManager = WindowManager::getInstance();

    // Set main window
    _mainWindow = _winManager -> getWin( level -> getValueMap()[ "world_window" ] -> getStringValue() );

    // Set up terrain
    _terrain.setTerrain( level -> getValueMap()[ "terrain" ] -> getStringValue(), _mainWindow );

    // Set world size
    _worldSize.x = ( float )_terrain.getWidth();
    _worldSize.y = ( float )_terrain.getHeight();

    // Get main window size, set as viewport size
    getmaxyx( _mainWindow, _viewportSize.y, _viewportSize.x );

	// Setup scene
	if( level -> getValueMap()[ "grid" ] -> getValueMap()[ "type" ] -> getStringValue() == "fixed_grid" )
	{
        FixedGrid* fixedGrid = new FixedGrid();
        fixedGrid -> setup( AABB( 0.0f, 0.0f, _worldSize.x, _worldSize.y ),
                            ( int )level -> getValueMap()[ "grid" ] -> getValueMap()[ "partition" ] -> getNumberValue() );
        SceneManager::getInstance() -> setSceneGraph( fixedGrid );
	}
    Debug::getInstance() -> log( "asset", ASSET, "Setup grid" );

    _treasure = level -> getValueMap()[ "initial_gold" ] -> getNumberValue();
    _taxRate = level -> getValueMap()[ "tax_rate" ] -> getNumberValue();
    Debug::getInstance() -> log( "asset", ASSET, "Read game configurations" );


    // Set camera properties
    _camera.setCameraRange( AABB( 0.0f, 0.0f, _worldSize.x, _worldSize.y ) );
    _camera.setViewport( AABB( 0.0f, 0.0f, _viewportSize.x, _viewportSize.y ) );

	//Setup game states
	_gameStateMachine.setOwner( this );
    _gameStateMachine.addState( "selection", &_selectionState );
    _gameStateMachine.addState( "gameplay", &_gameplayState );
    _gameStateMachine.addState( "interaction", &_interactionState );

    _selectionState.setOwner( this );
    _selectionState.getDispatcher() -> addListener( "gameplay", &_gameStateMachine );
    _selectionState.getDispatcher() -> addListener( "interaction", &_gameStateMachine );

    _gameplayState.setOwner( this );
    _gameplayState.getDispatcher() -> addListener( "selection", &_gameStateMachine );
    _gameplayState.getDispatcher() -> addListener( "interaction", &_gameStateMachine );

    _interactionState.setOwner( this );
    _interactionState.getDispatcher() -> addListener( "selection", &_gameStateMachine );
    _interactionState.getDispatcher() -> addListener( "gameplay", &_gameStateMachine );

    _gameStateMachine.setInitialState( "gameplay" );
    Debug::getInstance() -> log( "asset", ASSET, "Set up game states" );


    // Generate buildings
    unsigned buildingCount = level -> getValueMap()[ "buildings" ] -> getValueArray().size();
    for( unsigned i = 0; i < buildingCount; i++ )
    {
        std::string name = level -> getValueMap()[ "buildings" ] -> getValueArray()[ i ] -> getValueMap()[ "type" ] -> getStringValue();
        Building* building = BuildingFactory::spawnBuilding( name,
            level -> getValueMap()[ "buildings" ] -> getValueArray()[ i ] -> getValueMap()[ "location" ] -> getValueMap()[ "x" ] -> getNumberValue(),
            level -> getValueMap()[ "buildings" ] -> getValueArray()[ i ] -> getValueMap()[ "location" ] -> getValueMap()[ "y" ] -> getNumberValue(),
            _mainWindow );
        _buildings.insert( name, building );
        building -> getDispatcher() -> addListener( "transaction", this );
        building -> getDispatcher() -> addListener( "build", this );
        Debug::getInstance() -> log( "asset", ASSET, "Added building %s", name.c_str() );
    }

	_isRunning = true;

    Debug::getInstance() -> log( "main", GAMEPLAY, "Game Initialized" );

    // Set up log window
    scrollok( _winManager -> getWin( "log" ), true );
}

void Level::addCharacter( Character* character )
{
    if( character -> getType() == "dwarf" )
    {
        _dwarves.pushBack( character );
    }
}

Camera* Level::getCamera()
{
    return &_camera;
}

Vector2D Level::getWorldSize()
{
    return _worldSize;
}

Terrain* Level::getTerrain()
{
    return &_terrain;
}

Building* Level::getBuilding( std::string name )
{
    return _buildings[ name ];
}

int Level::getTreasure()
{
    return _treasure;
}

void Level::changeTreasure( int amount )
{
    _treasure += amount;
}

void Level::execute( IEvent* event )
{
    if( event -> getType() == "death" )
    {
        DeathEvent* death = dynamic_cast< DeathEvent* >( event );
        Actor* owner = death -> owner;
        wprintw( _winManager -> getWin( "log" ),
                "%s %s died at ( %.0f, %.0f )\n", owner -> getType().c_str(),
                owner -> getName().c_str(),
                owner -> getSceneNode() -> getCenter().x,
                owner -> getSceneNode() -> getCenter().y );
        Debug::getInstance() -> log( "main", GAMEPLAY, "%s %s died at ( %.0f, %.0f )", owner -> getType().c_str(),
                owner -> getName().c_str(),
                owner -> getSceneNode() -> getCenter().x,
                owner -> getSceneNode() -> getCenter().y );

        if( owner -> getType() == "dwarf" )
        {
            Dwarf* dwarf = dynamic_cast< Dwarf* >( owner );
            for( unsigned i = 0; i < _dwarves.size(); i++ )
            {
                if( dwarf == _dwarves[ i ] )
                {
                    _dwarves.erase( i );
                    break;
                }
            }
        }
    }
    else if( event -> getType() == "transaction" )
    {
        TransactionEvent* transaction = dynamic_cast< TransactionEvent* >( event );
        if( transaction -> transactionType == "purchase" )
        {
            float income = _taxRate * ( float )transaction -> amount;
            _treasure += ( int )income;
        }
        else if( transaction -> transactionType == "upgrade" || transaction -> transactionType == "build" )
        {
            _treasure -= transaction -> amount;
        }
    }
    else if( event -> getType() == "build" )
    {
        BuildEvent* build = dynamic_cast< BuildEvent* >( event );
        Building* building =
            BuildingFactory::spawnBuilding( build -> buildingName, build -> location.x, build -> location.y, _mainWindow );
        _buildings.insert( build -> buildingName, building );
        building -> getDispatcher() -> addListener( "transaction", this );
        Debug::getInstance() -> log( "asset", ASSET, "Added building %s", build -> buildingName.c_str() );
    }
}






// Game states
void Level::GameplayState::tick( float dt )
{
    char c = InputHandler::getInstance() -> getCurrentKey();
    IEvent* event = NULL;
    if( c == 's' )
    {
        wclear( WindowManager::getInstance() -> getWin( "help" ) );
        event = new SelectionEvent;
        dynamic_cast< SelectionEvent* >( event ) -> selectionMode = "select";
        _dispatcher.dispatch( event );
    }
    if( event != NULL )
    {
        delete event;
    }
}

void Level::GameplayState::preTick( float dt )
{
    int c = InputHandler::getInstance() -> getCurrentKey();

    int positionX, positionY;

    if( c == 'o' )
    {
        positionX = ( int )Random::getInstance() -> getRandom( _owner -> getWorldSize().x );
        positionY = ( int )Random::getInstance() -> getRandom( _owner -> getWorldSize().y );
        Character* ogre = CharacterFactory::spawnCharacter( "ogre",
        positionX, positionY,
        WindowManager::getInstance() -> getWin( "main" ) );

        ogre -> setGold( ( int )Random::getInstance() -> getRandom( 1000.0f ) );
        wprintw( WindowManager::getInstance() -> getWin( "log" ), "Ogre %s spawned at ( %d, %d )\n", ogre -> getName().c_str(), positionX, positionY );
        Debug::getInstance() -> log( "main", GAMEPLAY, "Ogre %s spawned at ( %d, %d )", ogre -> getName().c_str(), positionX, positionY );

        ogre -> getDispatcher() -> addListener( "death", _owner );
        _owner -> addCharacter( ogre );
        //ogreTimer.restart();
    }

    // Do game logic
    SceneManager::getInstance() -> tick( 0.1f );

    if( c == KEY_UP ) _owner -> getCamera() -> moveCamera( 0, -1 );
    if( c == KEY_DOWN ) _owner -> getCamera() -> moveCamera( 0, 1 );
    if( c == KEY_LEFT ) _owner -> getCamera() -> moveCamera( -1, 0 );
    if( c == KEY_RIGHT ) _owner -> getCamera() -> moveCamera( 1, 0 );

     // Display help info
     mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 1, 1, "[s]:selection mode [q]:quit" );
     mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 2, 1, "[d]:spawn dwarf  [o]:spawn ogre" );
}


void Level::SelectionState::tick( float dt )
{
    char c = InputHandler::getInstance() -> getCurrentKey();
    IEvent* event = NULL;
    if( c == 'g' && _mode == "select" )
    {
        if( _actor != NULL )
        {
            _actor -> getDrawable() -> attributeOff( A_BLINK | A_REVERSE );
            _actor = NULL;
        }
        if( _tile != NULL )
        {
            _tile -> getDrawable() -> attributeOff( A_BLINK | A_REVERSE );
            _tile = NULL;
        }
        event = new GameplayEvent;
        _dispatcher.dispatch( event );
        wclear( WindowManager::getInstance() -> getWin( "help" ) );
    }
    else if( c == 'i' && _mode == "select" )
    {
        if( _actor != NULL )
        {
            event = new InteractionEvent;
            dynamic_cast< InteractionEvent* >( event ) -> target = _actor;
            _dispatcher.dispatch( event );
        }
        wclear( WindowManager::getInstance() -> getWin( "help" ) );
    }
    else if( c == 'm' && _mode == "interact" )
    {
        if( _actor != NULL && _actor -> getType() == "dwarf" )
        {
            Character* character = dynamic_cast< Character* >( _actor );
            character -> moveTo( _worldPos.x, _worldPos.y );
            event = new InteractionEvent;
            dynamic_cast< InteractionEvent* >( event ) -> target = _actor;
            _dispatcher.dispatch( event );
        }
    }
    if( event != NULL )
    {
        delete event;
    }
}

void Level::SelectionState::preTick( float dt )
{
    int c = InputHandler::getInstance() -> getCurrentKey();

    if( _mode == "select" )
    {
        mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 1, 1, "[g]:game mode      [q]:quit" );
    }
    else
    {
        mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 1, 1, "[m]:move           [q]:quit" );
    }

    WINDOW* infoWin = WindowManager::getInstance() -> getWin( "info" );
    if( c != -1 )
    {
        WindowManager::getInstance() -> clearWin( "info" );

        if( _actor != NULL )
        {
            mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 2, 1, "            " );
            _actor -> getDrawable() -> attributeOff( A_BLINK | A_REVERSE );
        }
        if( _tile != NULL )
        {
            _tile -> getDrawable() -> attributeOff( A_BLINK | A_REVERSE );
        }

        if( c == KEY_UP ) _cursorPos.y--;
        if( c == KEY_DOWN ) _cursorPos.y++;
        if( c == KEY_LEFT ) _cursorPos.x--;
        if( c == KEY_RIGHT ) _cursorPos.x++;

        _cursorPos.x = Math::cast( 0, ( int )_owner -> getCamera() -> getViewport().lengthX() - 1, ( int )_cursorPos.x );
        _cursorPos.y = Math::cast( 0, ( int )_owner -> getCamera() -> getViewport().lengthY() - 1, ( int )_cursorPos.y );

        _worldPos.x = _cursorPos.x + _owner -> getCamera() -> getPosition().x;
        _worldPos.y = _cursorPos.y + _owner -> getCamera() -> getPosition().y;

        DynamicArray< SceneNode* > nodeList =
            SceneManager::getInstance() ->
            getSceneGraph() ->
            getColliders( _worldPos );

        for( unsigned i = 0; i < nodeList.size(); i++ )
        {
            if( ( int )_worldPos.x >= ( int )nodeList[ i ] -> getRect().leftBound() &&
                    ( int )_worldPos.y >= ( int )nodeList[ i ] -> getRect().upperBound() &&
                    ( int )_worldPos.x < ( int )nodeList[ i ] -> getRect().rightBound() &&
                    ( int )_worldPos.y < ( int )nodeList[ i ] -> getRect().lowerBound() )
            {
                Actor* actor = dynamic_cast< Actor* >( nodeList[ i ] -> getCollidable() );
                _actor = actor;
                _actor -> getDrawable() -> attributeOn( A_BLINK | A_REVERSE );
                mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 2, 1, "[i]:interact" );

                int row = 5;
                mvwprintw( infoWin, row++, 1, "Type: %s", actor -> getType().c_str() );
                mvwprintw( infoWin, row++, 1, "Name: %s", actor -> getName().c_str() );
                if( _actor -> getType() != "building" )
                {
                    Character* character = dynamic_cast< Character* >( actor );
                    mvwprintw( infoWin, row++, 1, "Health: %d/%d", character -> getHealth(), character -> getMaxHealth() );
                    mvwprintw( infoWin, row++, 1, "Gold: %d", character -> getGold() );
                    mvwprintw( infoWin, row++, 1, "Attack: %d", character -> getAttack() );
                    mvwprintw( infoWin, row++, 1, "Speed: %.1f", character -> getSpeed() );
                    mvwprintw( infoWin, row++, 1, "State: %s", character -> getCurrentState().c_str() );
                    if( character -> getTarget() != NULL )
                    {
                        mvwprintw( infoWin, row++, 1, "Target: %s", character -> getTarget() -> getName().c_str() );
                    }
                    else
                    {
                        mvwprintw( infoWin, row++, 1, "Target: None" );
                    }
                    if( character -> getType() == "dwarf" )
                    {
                        Dwarf* dwarf = dynamic_cast< Dwarf* >( character );
                        mvwprintw( infoWin, row++, 1, "Thirst: %d", dwarf -> getThirst() );
                        mvwprintw( infoWin, row++, 1, "Exhaustion: %d", dwarf -> getExhaustion() );
                        mvwprintw( infoWin, row++, 1, "T Threshold: %d", dwarf -> getThirstThreshold() );
                        mvwprintw( infoWin, row++, 1, "E Threshold: %d", dwarf -> getExhaustionThreshold() );
                    }

                }
                else
                {
                    Building* building = dynamic_cast< Building* >( actor );
                    mvwprintw( WindowManager::getInstance() -> getWin( "info" ), 8, 1, "Item list:" );
                    for( unsigned i = 0; i < building -> getItemList().size(); i++ )
                    {
                        mvwprintw( WindowManager::getInstance() -> getWin( "info" ), i + 9, 1, "%s  $%d",
                                    building -> getItemList()[ i ].c_str(),
                                    building -> getPrice( building -> getItemList()[ i ] ) );
                    }
                }
                break;
            }
        }
    }

    // Highlight terrain, if any
    _tile = _owner -> getTerrain() -> getTile( _worldPos.x, _worldPos.y );
    _tile -> getDrawable() -> attributeOn( A_BLINK | A_REVERSE );

    mvwprintw( infoWin, 3, 1, "Tile: %s", _tile -> getName().c_str() );
    mvwprintw( infoWin, 4, 1, "Pos: %.0f, %.0f", _worldPos.x, _worldPos.y );
}


void Level::SelectionState::setMode( std::string mode )
{
    _mode = mode;
}

void Level::InteractionState::tick( float dt )
{
    IEvent* event = NULL;
    char c = InputHandler::getInstance() -> getCurrentKey();
    if( c == 's' )
    {
        event = new SelectionEvent;
        dynamic_cast< SelectionEvent* >( event ) -> selectionMode = "select";
        _dispatcher.dispatch( event );
        wclear( WindowManager::getInstance() -> getWin( "help" ) );
    }
    else if( c == 'g' )
    {
        if( _target != NULL )
        {
            _target -> getDrawable() -> attributeOff( A_BLINK | A_REVERSE );
            _target = NULL;
        }
        event = new GameplayEvent;
        _dispatcher.dispatch( event );
        wclear( WindowManager::getInstance() -> getWin( "help" ) );
    }
    else if( c == 'm' && _target -> getType() == "dwarf" )
    {
        event = new SelectionEvent;
        dynamic_cast< SelectionEvent* >( event ) -> selectionMode = "interact";
        _dispatcher.dispatch( event );
        wclear( WindowManager::getInstance() -> getWin( "help" ) );
    }
    if( event != NULL )
    {
        delete event;
    }
}

void Level::InteractionState::preTick( float dt )
{
    char c = InputHandler::getInstance() -> getCurrentKey();
    if( _target -> getType() == "building" )
    {
        Building* building = dynamic_cast< Building* >( _target );
        mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 1, 1, "[u]:Upgrade($%d)", building -> getUpgradeCost() );

        if( c == 'u' )
        {
            // Should test if treasure is enough
            if( _owner -> getTreasure() >= building -> getUpgradeCost() )
            {
                building -> upgrade( "" );
            }
        }

        if( building -> getName() == "great_hall" )
        {
            mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 1, 24, "[d]:Recruit dwarf($300)" );
            mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 2, 1, "[b]:Black smith($%d)  [a]:Apothecary($%d)  [s]:Selection",
                        building -> getConstructionCost( "black_smith" ), building -> getConstructionCost( "apothecary" ) );

            if( c == 'd' )
            {
                int x = building -> getSceneNode() -> getTopLeft().x;
                int y = building -> getSceneNode() -> getTopLeft().y;
                Character* dwarf = CharacterFactory::spawnCharacter( "dwarf", x, y, WindowManager::getInstance() -> getWin( "main" ) );

                wprintw( WindowManager::getInstance() -> getWin( "log" ), "Dwarf %s spawned at ( %d, %d )\n", dwarf -> getName().c_str(), x, y );
                Debug::getInstance() -> log( "main", GAMEPLAY, "Dwarf %s spawned at ( %d, %d )", dwarf -> getName().c_str(), x, y );

                dwarf -> getDispatcher() -> addListener( "death", _owner );
                _owner -> addCharacter( dwarf );
                _owner -> changeTreasure( -300 );
            }
            else if( c == 'b' )
            {
                if( _owner -> getTreasure() >= building -> getConstructionCost( "black_smith" ) &&
                    _owner -> getBuilding( "black_smith" ) == NULL )
                {
                    // Build black smith
                    Debug::getInstance() -> log( "main", GAMEPLAY, "Building black smith" );
                    BuildEvent* event = new BuildEvent;
                    event -> buildingName = "black_smith";
                    event -> location.x = 5;
                    event -> location.y = 15;
                    building -> getDispatcher() -> dispatch( event );
                    TransactionEvent* trans = new TransactionEvent;
                    trans -> amount = building -> getConstructionCost( "black_smith" );
                    trans -> transactionType = "build";
                    building -> getDispatcher() -> dispatch( trans );
                }
            }
            else if( c == 'a' )
            {
                if( _owner -> getTreasure() >= building -> getConstructionCost( "apothecary" ) &&
                    _owner -> getBuilding( "apothecary" ) == NULL )
                {
                    // Build apothecary
                    Debug::getInstance() -> log( "main", GAMEPLAY, "Building apothecary" );
                    BuildEvent* event = new BuildEvent;
                    event -> buildingName = "apothecary";
                    event -> location.x = 5;
                    event -> location.y = 6;
                    building -> getDispatcher() -> dispatch( event );
                    TransactionEvent* trans = new TransactionEvent;
                    trans -> amount = building -> getConstructionCost( "apothecary" );
                    trans -> transactionType = "build";
                    building -> getDispatcher() -> dispatch( trans );
                }
            }
        }

        mvwprintw( WindowManager::getInstance() -> getWin( "info" ), 8, 1, "Item list:" );
        for( unsigned i = 0; i < building -> getItemList().size(); i++ )
        {
            mvwprintw( WindowManager::getInstance() -> getWin( "info" ), i + 9, 1, "%s  $%d",
                       building -> getItemList()[ i ].c_str(),
                       building -> getPrice( building -> getItemList()[ i ] ) );
        }
    }
    else if( _target -> getType() == "dwarf" )
    {
        mvwprintw( WindowManager::getInstance() -> getWin( "help" ), 1, 1, "[m]:move" );
    }
}

void Level::GameStateMachine::onStateTransition( IEvent* event )
{
    if( event -> getType() == "selection" )
    {
        _currentState = _stateMap[ "selection" ];
        dynamic_cast< SelectionState* >( _currentState ) -> setMode( dynamic_cast< SelectionEvent* >( event ) -> selectionMode );
    }
    else if( event -> getType() == "gameplay" )
    {
        _currentState = _stateMap[ "gameplay" ];
    }
    else if( event -> getType() == "interaction" )
    {
        _currentState = _stateMap[ "interaction" ];
        Actor* target = dynamic_cast< InteractionEvent* >( event ) -> target;
        dynamic_cast< InteractionState* >( _currentState ) -> setTarget( target );
        Debug::getInstance() -> log( "main", GAMEPLAY, "Interacting with %s", target -> getName().c_str() );
    }
}


