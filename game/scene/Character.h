#pragma once
#include "Actor.h"
#include "../../engine/event/Dispatcher.h"
#include "../../engine/state/IState.h"
#include "../../engine/timer/Timer.h"
#include "item/Weapon.h"
#include "item/Armor.h"
#include "item/Potion.h"

class Character : public Actor
{
public:

    Character();
    virtual ~Character();

    const int getHealth();
    const int getMaxHealth();
    const int getGold();
    const int getAttack();
    const int getAttackRange();
    const int getDirection();
    const int getSearchRange();
    const float getSpeed();
    const bool isAlive();
    const bool isInCommand();
    std::string getTargetType();
    std::string getCurrentState();
    Point2D getDestination();
    Character* getTarget();
    Weapon* getWeapon();
    Armor* getArmor();
    Potion* getPotion();
    Timer* getTimer( std::string name );


    void setHealth( int health );
    void setMaxHealth( int health );
    void setGold( int gold );
    void setAttack( int attack );
    void setAttackRange( int range );
    void setDirection( int direction );
    void setSpeed( float speed );
    void setDestination( Point2D destination );
    void setTarget( Character* target );
    void setCurrentState( std::string state );
    void setWeapon( Weapon* weapon );
    void setArmor( Armor* armor );
    void setPotion( Potion* potion );
    void setSearchRange( int range );
    void setTargetType( std::string targetType );

    void addTimer( std::string name, unsigned time );

    void usePotion();
    void moveTo( int x, int y );
    void beingAttack( int amount );
    void increaseGold( int amount );
    void releaseCommand();


private:

    int              _health;        // Health of character
    int              _maxHealth;     // Max health of character
    int              _attack;        // Attack of character
    int              _attackRange;   // Attack range of character
    int              _gold;          // Amount of gold of this actor
    int              _direction;     // Direction of the actor
    int              _searchRange;   // Search range of the actor
    bool             _inCommand;     // If the actor is in command
    bool             _alive;         // If the character is alive
    float            _speed;         // Speed of character
    Point2D          _destination;   // Destination of the character
    Character*       _target;        // Target of the character
    std::string      _state;         // Current state of character
    Dispatcher       _dispatcher;    // Dispatcher of character
    Weapon*          _weapon;        // Weapon of the character
    Armor*           _armor;         // Armor of the character
    Potion*          _potion;        // Potion of the character
    std::string      _targetType;    // Type of the enemy target

	Hashmap< Timer > _timers;        // Map of timers

};
