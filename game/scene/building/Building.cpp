#include "Building.h"

Building::~Building()
{
    for( unsigned i = 0; i < _items.getKeys().size(); i++ )
    {
        delete _items[ _items.getKeys()[ i ] ];
    }
}

void Building::addItem( std::string name )
{
    _items.insert( name, ItemFactory::getItem( name ) );
    Debug::getInstance() -> log( "main", GAMEPLAY, "%s added a new item:%s", _name.c_str(), name.c_str() );
}

void Building::addConstruction( std::string name, int cost )
{
    _constructions.insert( name, cost );
}

void Building::sell( Character* buyer, std::string name )
{
    Item* item = _items[ name ];
    if( item -> getType() == "weapon" )
    {
        Weapon* weapon = new Weapon;
        *weapon = *( dynamic_cast< Weapon* >( item ) );
        buyer -> setWeapon( weapon );
    }
    else if( item -> getType() == "armor" )
    {
        Armor* armor = new Armor;
        *armor = *( dynamic_cast< Armor* >( item ) );
        buyer -> setArmor( armor );
    }
    else if( item -> getType() == "potion" )
    {
        Potion* potion = new Potion;
        *potion = *( dynamic_cast< Potion* >( item ) );
        buyer -> setPotion( potion );
    }
    else if( item -> getType() == "ale" )
    {
        Potion* potion = new Potion;
        *potion = *( dynamic_cast< Potion* >( item ) );
        Dwarf* dwarf = dynamic_cast< Dwarf* >( buyer );
        dwarf -> setThirst( dwarf -> getThirst() - potion -> getPotionValue() );
    }
    TransactionEvent* transaction = new TransactionEvent;
    transaction -> amount = item -> getPrice();
    transaction -> transactionType = "purchase";
    _dispatcher.dispatch( transaction );
    delete transaction;
}

int Building::getPrice( std::string name )
{
    return _items[ name ] -> getPrice();
}

int Building::getConstructionCost( std::string name )
{
    return _constructions[ name ];
}

int Building::getUpgradeCost()
{
    if( _level < ( unsigned )_config -> getValueMap()[ "top_level" ] -> getNumberValue() )
    {
        return ( int )_config -> getValueMap()[ "upgrade_cost" ] -> getValueArray()[ _level - 1 ] -> getNumberValue();
    }
    else
    {
        return -1;
    }
}

void Building::setConfig( JsonValue* config )
{
    _config = config;
}

void Building::setLevel( int level )
{
    _level = level;
}

void Building::upgrade( std::string type )
{
    if( _level < ( unsigned )_config -> getValueMap()[ "top_level" ] -> getNumberValue() )
    {
        Debug::getInstance() -> log( "main", GAMEPLAY, "Upgrading %s", _name.c_str() );
        TransactionEvent* transaction = new TransactionEvent;
        transaction -> amount = getUpgradeCost();
        transaction -> transactionType = "upgrade";
        _dispatcher.dispatch( transaction );
        delete transaction;
        _level++;

        // Item to add
        JsonValue* item;
        for( unsigned i = 0; i < _config -> getValueMap()[ "levelup_item" ] -> getValueArray().size(); i++ )
        {
            item = _config -> getValueMap()[ "levelup_item" ] -> getValueArray()[ i ];
            if( _level == ( unsigned )item -> getValueMap()[ "level" ] -> getNumberValue() )
            {
                addItem( item -> getValueMap()[ "item" ] -> getStringValue() );
            }
        }
    }
}

Item* Building::getItem( std::string name )
{
    return _items[ name ];
}

DynamicArray< std::string > Building::getItemList()
{
    return _items.getKeys();
}
