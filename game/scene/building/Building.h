#pragma once
#include "../Actor.h"
#include "../Character.h"
#include "../Dwarf.h"
#include "../item/Item.h"
#include "../../../engine/structures/Hashmap.h"
#include "../../../engine/event/Dispatcher.h"
#include "../../../engine/debug/Debug.h"
#include "../../factory/ItemFactory.h"
#include "../../event/TransactionEvent.h"
#include "../../event/BuildEvent.h"

class Building : public Actor
{

public:

    Building(){}
    ~Building();

	/*
	Function:	Add an item to the building
	Return:		void
	Parameters:	name - name of the item
	*/
    void addItem( std::string name );

	/*
	Function:	Add construction to the building
	Return:		void
	Parameters:	name - name of the building
                cost - cost of the building
	*/
    void addConstruction( std::string name, int cost );

	/*
	Function:	Set configuration of the building
	Return:		void
	Parameters:	config - config of the building
	*/
    void setConfig( JsonValue* config );

	/*
	Function:	Set level of the building
	Return:		void
	Parameters:	config - config of the building
	*/
    void setLevel( int level );

	/*
	Function:	Sell an item to a character
	Return:		If the transaction is successful
	Parameters:	buyer - Pointer to the character who buy this item
                name - name of the item
	*/
    void sell( Character* buyer, std::string name );

	/*
	Function:	Get a price of an item
	Return:		Price of the item
	Parameters:	name - name of the item
	*/
    int getPrice( std::string name );

	/*
	Function:	Get construction cost of a building
	Return:		Price of the building
	Parameters:	name - name of the building
	*/
    int getConstructionCost( std::string name );

    /*
	Function:	Upgrade this building
	Return:		void
	Parameters:	type - type of the item to be upgraded
    */
    void upgrade( std::string type);

    /*
	Function:	Get upgrade cost
	Return:		Upgrade cost
	Parameters:	void
    */
    int getUpgradeCost();

    /*
	Function:	Get an item in this building
	Return:     Pointer to the item
	Parameters:	nae - name of the item
    */
    Item* getItem( std::string name );

    /*
	Function:	Get a list of items sell in this building
	Return:		List of items
	Parameters:	void
    */
    DynamicArray< std::string > getItemList();

private:

    Hashmap< Item* >  _items;         // Map of the items available in the building
    Hashmap< int >    _constructions; // Map of building cost that can be constructed by this building
    JsonValue*        _config;        // Configurations of this building
    unsigned          _level;         // Level of the building
};
