#pragma once

#include "../../engine/scene/ASCIIDrawable.h"
#include "../../engine/structures/DynamicArray.h"
#include "../../engine/json/JsonParser.h"
#include "../../engine/asset/AssetManager.h"
#include "../../engine/scene/SceneNodeFactory.h"
#include "Actor.h"

class Terrain
{
public:

    Terrain();
    ~Terrain();

    void setTerrain( std::string filePath, WINDOW* win );
    void render( Camera camera );

    const unsigned getWidth();
    const unsigned getHeight();

    Actor* getTile( int x, int y );

private:

    DynamicArray< Actor* >          _terrainData;
    DynamicArray< ASCIIDrawable >   _tileData;
    unsigned                        _height;
    unsigned                        _width;
    WINDOW*                         _win;

    JsonValue*                      _terrainObject;

};
