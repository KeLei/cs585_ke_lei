#pragma once
#include <string>
#include <ncurses.h>
#include "../../engine/input/InputHandler.h"
#include "../../engine/debug/Debug.h"
#include "../../engine/structures/DynamicArray.h"
#include "../../engine/event/Dispatcher.h"
#include "../event/MenuSelectionEvent.h"

class Menu
{

public:

    Menu();
    ~Menu(){}

	/*
	Function:	Set the window which this menu will appear in
	Return:		void
	Parameters:	win - window of the menu
	*/
    void setWindow( WINDOW* win );

	/*
	Function:	Show the menu
	Return:		void
	Parameters:	void
	*/
    void show();

	/*
	Function:	Hide the menu
	Return:		void
	Parameters:	void
	*/
    void hide();

	/*
	Function:	Update the menu
	Return:		void
	Parameters:	void
	*/
    void update();

	/*
	Function:	Add option to the menu
	Return:		void
	Parameters:	option - new option
	*/
    void addOption( std::string option );

	/*
	Function:	Set attribute of the menu
	Return:		void
	Parameters:	attribute
	*/
    void setAttribute( int attribute );

	/*
	Function:	Get the selected option
	Return:		Selected option index
	Parameters:	void
	*/
    unsigned selected();

    Dispatcher* getDispatcher();

private:

    WINDOW*                     _win;       // Window of this menu
    int                         _attribute; // Font attribute of the menu
    unsigned                    _selection; // Selected
    unsigned                    _longest;   // Longest selection
    DynamicArray< std::string > _options;   // List of options
    Dispatcher                  _dispatcher;
};
