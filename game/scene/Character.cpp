#include "Character.h"

Character::Character()
{
    _health = 0;
    _attack = 0;
    _attackRange = 0;
    _speed = 0.0f;
    _gold = 0;
    _searchRange = 0;
    _searchRange = 0;
    _target = NULL;
    _alive = TRUE;
    _weapon = NULL;
    _armor = NULL;
    _potion = NULL;
}

Character::~Character()
{
    if( _weapon != NULL )
    {
        delete _weapon;
    }
    if( _armor != NULL )
    {
        delete _armor;
    }
    if( _potion != NULL )
    {
        delete _potion;
    }
}

const int Character::getHealth()
{
    return _health;
}

const int Character::getMaxHealth()
{
    return _maxHealth;
}

const int Character::getGold()
{
    return _gold;
}

const int Character::getAttack()
{
    return _attack;
}

const int Character::getAttackRange()
{
    return _attackRange;
}

const int Character::getDirection()
{
    return _direction;
}

const int Character::getSearchRange()
{
    return _searchRange;
}

const float Character::getSpeed()
{
    return _speed;
}

const bool Character::isAlive()
{
    if( _health <= 0 )
    {
        _alive = false;
    }
    else
    {
        _alive = true;
    }
    return _alive;
}

const bool Character::isInCommand()
{
    return _inCommand;
}

std::string Character::getTargetType()
{
    return _targetType;
}

std::string Character::getCurrentState()
{
    return _state;
}

Point2D Character::getDestination()
{
    return _destination;
}

Character* Character::getTarget()
{
    return _target;
}

Weapon* Character::getWeapon()
{
    return _weapon;
}

Armor* Character::getArmor()
{
    return _armor;
}

Potion* Character::getPotion()
{
    return _potion;
}

Timer* Character::getTimer( std::string name )
{
    return &_timers[ name ];
}


void Character::setHealth( int health )
{
    _health = health;
}

void Character::setMaxHealth( int health )
{
    _maxHealth = health;
}

void Character::setGold( int gold )
{
    _gold = gold;
}

void Character::setAttack( int attack )
{
    _attack = attack;
}

void Character::setAttackRange( int range )
{
    _attackRange = range;
}

void Character::setDirection( int direction )
{
    _direction = direction;
}

void Character::setDestination( Point2D destination )
{
    _destination = destination;
}

void Character::setTarget( Character* target )
{
    _target = target;
}

void Character::setCurrentState( std::string state )
{
    _state = state;
}

void Character::setSpeed( float speed )
{
    _speed  = speed;
}

void Character::setWeapon( Weapon* weapon )
{
    if( _weapon != NULL )
    {
        delete _weapon;
    }

    _weapon = weapon;
}

void Character::setArmor( Armor* armor )
{
    if( _armor != NULL )
    {
        delete _armor;
    }

    _armor = armor;
}

void Character::setPotion( Potion* potion )
{
    if( _potion != NULL )
    {
        delete _potion;
    }

    _potion= potion;
}

void Character::addTimer( std::string name, unsigned time )
{
    _timers.insert( name, Timer( time ) );
}

void Character::beingAttack( int amount )
{
    if( _armor != NULL )
    {
        _health += _armor -> getDefense();
        _armor -> setDurability( _armor -> getDurability() - 1 );
        if( _armor ->getDurability() == 0 )
        {
            delete _armor;
            _armor = NULL;
        }
    }
    _health -= amount;
}

void Character::usePotion()
{
    _health += _potion -> getPotionValue();
    if( _health > _maxHealth )
    {
        _health = _maxHealth;
    }
    delete _potion;
    _potion = NULL;
}

void Character::moveTo( int x, int y )
{
    _inCommand = TRUE;
    _destination.x = x;
    _destination.y = y;
}

void Character::increaseGold( int amount )
{
    _gold += amount;
}

void Character::setSearchRange( int range )
{
    _searchRange = range;
}

void Character::setTargetType( std::string targetType )
{
    _targetType = targetType;
}

void Character::releaseCommand()
{
    _inCommand = false;
}

