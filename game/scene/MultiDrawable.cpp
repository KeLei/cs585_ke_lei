#include "MultiDrawable.h"

void MultiDrawable::render( Camera camera )
{
    for( unsigned i = 0; i < _bricks.size(); i++ )
    {
        _tiles[ _bricks[ i ] ].setPosition( _topLeft.x + i % ( int )_size.x, _topLeft.y + i / ( int )_size.x );
        _tiles[ _bricks[ i ] ].render( camera );
    }
}

void MultiDrawable::setPosition( int x, int y )
{
    _topLeft.x = x;
    _topLeft.y = y;
}

void MultiDrawable::setWindow( WINDOW* win )
{
    _win = win;
}

void MultiDrawable::insertTile( ASCIIDrawable drawable )
{
    _tiles.pushBack( drawable );
}

void MultiDrawable::insertBrick( int index )
{
    _bricks.pushBack( index );
}

void MultiDrawable::setSize( int width, int height )
{
    _size.x = width;
    _size.y = height;
    _tiles.reserve( width * height );
}

void MultiDrawable::attributeOn( int attribute )
{
    for( unsigned i = 0; i < _tiles.size(); i++ )
    {
        _tiles[ i ].attributeOn( attribute );
    }
}

void MultiDrawable::attributeOff( int attribute )
{
    for( unsigned i = 0; i < _tiles.size(); i++ )
    {
        _tiles[ i ].attributeOff( attribute );
    }
}

