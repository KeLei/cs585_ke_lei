#include "Armor.h"

Armor::Armor()
{
    _defense = 0;
    _durability = 0;
}

unsigned Armor::getDefense()
{
    return _defense;
}

unsigned Armor::getDurability()
{
    return _durability;
}

void Armor::setDefense( unsigned defense )
{
    _defense = defense;
}

void Armor::setDurability( unsigned durability )
{
    _durability = durability;
}

Armor& Armor::operator=( const Armor& armor )
{
    Item::operator=( armor );
    _defense = armor._defense;
    _durability = armor._durability;
    return *this;
}
