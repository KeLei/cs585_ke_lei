#include "Potion.h"

Potion::Potion()
{
    _value = 0;
}

const int Potion::getPotionValue()
{
    return _value;
}

void Potion::setPotionValue( int value )
{
    _value = value;
}

Potion& Potion::operator=( const Potion& potion )
{
    Item::operator=( potion );
    _value = potion._value;
    return *this;
}
