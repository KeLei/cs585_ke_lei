#pragma once
#include "Item.h"

class Potion : public Item
{

public:

    Potion();
    ~Potion(){}

	/*
	Function:	Get amount of health restore of the potion
	Return:		Value of the potion
	Parameters:	void
	*/
    const int getPotionValue();

	/*
	Function:	Set amoutn of health restore of the potion
	Return:		void
	Parameters:	Value of the potion
	*/
    void setPotionValue( int value );

    Potion& operator=( const Potion& potion );

private:

    int _value;  // How much health restored by this potion
};
