#pragma once
#include "Item.h"

class Weapon : public Item
{

public:

    Weapon();
    ~Weapon(){}

	/*
	Function:	Set attack of the weapon
	Return:		void
	Parameters:	attack - Attack of the weapon
	*/
	void setAttack( unsigned attack );

	/*
	Function:	Set critical attack of the weapon
	Return:		void
	Parameters:	criticalAttack - Critical attack of the weapon
	*/
    void setCriticalAttack( unsigned criticalAttack );

	/*
	Function:	Set durability of the weapon
	Return:		void
	Parameters:	durability - Durability of the weapon
	*/
    void setDurability( unsigned durability );

	/*
	Function:	Set critical rate of the weapon
	Return:		void
	Parameters:	criticalRate - Critical rate of the weapon
	*/
    void setCriticalRate( float criticalRate );

	/*
	Function:	Set hit rate of the weapon
	Return:		void
	Parameters:	hitRate - Hit rate of the weapon
	*/
    void setHitRate( float hitRate );

	/*
	Function:	Get attack of the weapon
	Return:		Attack of the weapon
	Parameters:	void
	*/
    unsigned getAttack();

	/*
	Function:	Get critical attack of the weapon
	Return:		Critical attack of the weapon
	Parameters:	void
	*/
    unsigned getCriticalAttack();

	/*
	Function:	Get durability of the weapon
	Return:		Durability of the weapon
	Parameters:	void
	*/
    unsigned getDurability();

	/*
	Function:	Get critical rate of the weapon
	Return:		Critical rate of the weapon
	Parameters:	void
	*/
    float getCriticalRate();

	/*
	Function:	Get hit rate of the weapon
	Return:		Hit rate of the weapon
	Parameters:	void
	*/
    float getHitRate();

    Weapon& operator=( const Weapon& weapon );

private:

    unsigned _attack;           // Attack of the weapon
    unsigned _criticalAttack;   // Critical attack of the weapon
    unsigned _durability;       // Durability of the weapon
    float    _criticalRate;     // Critical chance of the weapon
    float    _hitRate;          // Accuracy of the weapon
};
