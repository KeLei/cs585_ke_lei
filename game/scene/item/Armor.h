#pragma once
#include "Item.h"

class Armor : public Item
{

public:

    Armor();
    ~Armor(){}

	/*
	Function:	Get defense value of the armor
	Return:		Defense Value of the armor
	Parameters:	void
	*/
    unsigned getDefense();

	/*
	Function:	Get durability of the armor
	Return:		Durability of the armor
	Parameters:	void
	*/
    unsigned getDurability();

	/*
	Function:	Set defense value of the armor
	Return:		void
	Parameters:	defense - Defense value of the armor
	*/
    void setDefense( unsigned defense );

	/*
	Function:	Set attack of the armor
	Return:		void
	Parameters:	durability - Durability of the armor
	*/
    void setDurability( unsigned durability );

    Armor& operator= ( const Armor& armor );

private:

    unsigned _defense;      // Defense of the armor
    unsigned _durability;   // Durability of the armor
};
