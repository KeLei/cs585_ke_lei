#include "Weapon.h"

Weapon::Weapon()
{
    _attack = 0;
    _criticalAttack = 0;
    _durability = 0;
    _criticalRate = 0.0f;
    _hitRate = 0.0f;
}

void Weapon::setAttack( unsigned attack )
{
    _attack = attack;
}

void Weapon::setCriticalAttack( unsigned criticalAttack )
{
    _criticalAttack = criticalAttack;
}

void Weapon::setDurability( unsigned durability )
{
    _durability = durability;
}

void Weapon::setCriticalRate( float criticalRate )
{
    _criticalRate = criticalRate;
}

void Weapon::setHitRate( float hitRate )
{
    _hitRate = hitRate;
}

unsigned Weapon::getAttack()
{
    return _attack;
}

unsigned Weapon::getCriticalAttack()
{
    return _criticalAttack;
}

unsigned Weapon::getDurability()
{
    return _durability;
}

float Weapon::getCriticalRate()
{
    return _criticalRate;
}

float Weapon::getHitRate()
{
    return _hitRate;
}

Weapon& Weapon::operator=( const Weapon& weapon )
{
    Item::operator=( weapon );
    _attack = weapon._attack;
    _criticalAttack = weapon._criticalAttack;
    _durability = weapon._durability;
    _hitRate = weapon._hitRate;
    _criticalRate = weapon._criticalRate;
    return *this;
}

