#include "Item.h"

Item::Item()
{
    _price = 0;
    _type = "";
}

void Item::setPrice( int price )
{
    _price = price;
}

void Item::setType( std::string type )
{
    _type = type;
}

const int Item::getPrice()
{
    return _price;
}

const std::string Item::getType()
{
    return _type;
}

Item& Item::operator=( const Item& item )
{
    _price = item._price;
    _type = item._type;
    return *this;
}
