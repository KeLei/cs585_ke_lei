#pragma once
#include <string>

class Item
{

public:

    Item();
    virtual ~Item(){}

	/*
	Function:	Set price of the item
	Return:		void
	Parameters:	price - price of the item
	*/
    void setPrice( int price );

	/*
	Function:	Set type of the item
	Return:		void
	Parameters:	type - type of the item
	*/
    void setType( std::string type );

	/*
	Function:	Get price of the item
	Return:		price of the item
	Parameters:	void
	*/
    const int getPrice();

	/*
	Function:	Get type of the item
	Return:		Type of the item
	Parameters:	void
	*/
    const std::string getType();

    Item& operator= ( const Item& item );

private:

    int         _price; // Price of the item
    std::string _type;  // Type of the item
};
