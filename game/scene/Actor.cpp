#include "Actor.h"

Actor::Actor()
{
	_node = NULL;
}

void Actor::setSceneNode( SceneNode* node )
{
	_node = node;
}

void Actor::setName( std::string name )
{
	_name = name;
}

void Actor::setDrawable( IDrawable* drawable )
{
    _node -> setDrawable( drawable );
}

void Actor::setType( std::string type )
{
    _type = type;
}

SceneNode* Actor::getSceneNode()
{
	return _node;
}

std::string Actor::getName()
{
    return _name;
}

std::string Actor::getType()
{
    return _type;
}

IDrawable* Actor::getDrawable()
{
	return _node -> getDrawable();
}

Dispatcher* Actor::getDispatcher()
{
    return &_dispatcher;
}
