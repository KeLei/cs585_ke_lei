#include "Terrain.h"

Terrain::Terrain()
{
}

Terrain::~Terrain()
{
    unsigned tileCount = _terrainData.size();
    for( unsigned i = 0; i < tileCount; i++ )
    {
        delete _terrainData[ i ] -> getDrawable();
        delete _terrainData[ i ];
    }
}

void Terrain::setTerrain( std::string filePath, WINDOW* win )
{
    JsonParser parser( filePath );

    _terrainObject = parser.parse();

    unsigned tileCount = _terrainObject -> getValueMap()[ "tiles" ] -> getValueArray().size();
    ASCIIDrawable tileDrawable;
    std::string name;
    JsonValue* config;
    for( unsigned i = 0; i < tileCount; i++ )
    {
        name = _terrainObject -> getValueMap()[ "tiles" ] -> getValueArray()[ i ] ->getStringValue();
        config = AssetManager::getInstance() -> getConfig( name );
        tileDrawable.setCharacter( config -> getValueMap()[ "character" ] -> getStringValue()[ 0 ] );
        tileDrawable.setColor( COLOR_PAIR( ( int )config->getValueMap()[ "color" ] -> getNumberValue() ) );
        tileDrawable.setAttribute( AssetManager::getInstance() -> getAttribute( config->getValueMap()[ "attribute" ] -> getStringValue() ) );
        _tileData.pushBack( tileDrawable );
    }

    _height = _terrainObject -> getValueMap()[ "height" ] -> getNumberValue();
    _width = _terrainObject -> getValueMap()[ "width" ] -> getNumberValue();

    unsigned numTiles = _terrainObject -> getValueMap()[ "map" ] -> getValueArray().size();
    assert( _height * _width == numTiles );

    Actor* tile;
    int tileIndex;
    for( unsigned i = 0; i < numTiles; i++ )
    {
        tile = new Actor;
        tile -> setSceneNode( SceneNodeFactory::getInstance() -> getSceneNode( ( i % _width ), ( i / _width ), 0, 0 ) );
        tileIndex = ( int )( _terrainObject -> getValueMap()[ "map" ] -> getValueArray()[ i ] -> getNumberValue() );
        tile -> setDrawable( new ASCIIDrawable( _tileData[ tileIndex ] ) );

        // Set position
        tile -> getDrawable() -> setPosition( i % _width , i / _width );
        tile -> getDrawable() -> setWindow( win );
        tile -> setName( _terrainObject -> getValueMap()[ "tiles" ] -> getValueArray()[ tileIndex ] ->getStringValue() );
        _terrainData.pushBack( tile );
    }

}

void Terrain::render( Camera camera )
{
    unsigned tileCount = _terrainData.size();
    for( unsigned i = 0; i < tileCount; i++ )
    {
        _terrainData[ i ] -> getDrawable() -> render( camera );
    }
}

const unsigned Terrain::getWidth()
{
    return _width;
}

const unsigned Terrain::getHeight()
{
    return _height;
}

Actor* Terrain::getTile( int x, int y )
{
    return _terrainData[ y * _width + x ];
}
