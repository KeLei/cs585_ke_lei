#pragma once

#include <iostream>
#include <string>
#include "../../engine/scene/ASCIIDrawable.h"
#include "../../engine/scene/ICollidable.h"
#include "../../engine/scene/SceneNode.h"
#include "../../engine/event/Dispatcher.h"

class Actor : public ICollidable
{

protected:

	SceneNode*	    _node;	      // Scene node of the actor
	std::string	    _name;	      // Name of the actor
	std::string     _type;        // Type of the actor
	Dispatcher      _dispatcher;  // Dispatcher of the actor

public:

	/*
	Function:	Constructor, set pointers to NULL
	Parameters:	void
	*/
	Actor();

	/*
	Function:	Destructor, delete all pointers
	*/
	virtual ~Actor(){}


	/*
	Function:	Set scene node of the actor
	Return:		void
	Parameters:	node - Scene node of the actor
	*/
	void setSceneNode( SceneNode* node );

	/*
	Function:	Set name of the actor
	Return:		void
	Parameters:	deadOrLive - true if alive, else false
	*/
	void setName( std::string name );

	/*
	Function:	Set type of the actor
	Return:		void
	Parameters:	type - type of the actor
	*/
    void setType( std::string type);

	/*
	Function:	Set the live status of the actor
	Return:		void
	Parameters:	deadOrLive - true if alive, else false
	*/
	void setDrawable( IDrawable* drawable );

	/*
	Function:	Get scene node of the actor
	Return:		Scene node of the actor
	Parameters:	void
	*/
	SceneNode* getSceneNode();

	/*
	Function:	Get name of the actor
	Return:		Name of the actor
	Parameters:	void
	*/
	std::string getName();

	/*
	Function:	Get type of the actor
	Return:		Type of the actor
	Parameters:	void
	*/
    std::string getType();

	/*
	Function:	Get drawable part of the actor
	Return:		Drawable part of the actor
	Parameters:	void
	*/
	IDrawable* getDrawable();

	/*
	Function:	Get dispatcher of the actor
	Return:		Dispatcher of the actor
	Parameters:	void
	*/
	Dispatcher* getDispatcher();

};
