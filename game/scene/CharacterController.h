#pragma once

#include "../../engine/scene/ITickable.h"
#include "../../engine/debug/Debug.h"
#include "../../engine/random/Random.h"
#include "../../engine/scene/SceneManager.h"
#include "../../engine/state/IStateMachine.h"
#include "../../engine/math/Math.h"
#include "../event/DeathEvent.h"
#include "../event/TargetEvent.h"
#include "../event/ThirstEvent.h"
#include "../event/ExhaustedEvent.h"
#include "../event/FinishEvent.h"
#include "../event/TransactionEvent.h"
#include "../event/LowHealthEvent.h"
#include "../event/NeedEquipmentEvent.h"
#include "../LevelManager.h"
#include "building/Building.h"
#include "Character.h"
#include "Dwarf.h"

class CharacterController : public ITickable, public ICallback
{

public:

    CharacterController();
    virtual ~CharacterController();

    void tick( float dt );
    void preTick( float dt );
    void postTick( float dt );
    void setCharacter( Character* character );
    void searchNearby();

    bool isCollideWith( SceneNode* node );
    void execute( IEvent* event );

    Character* getCharacter();

protected:

    class CharacterStateMachine : public IStateMachine
    {

    public:

        CharacterController* controller;
        void onStateTransition( IEvent* event );

    }_stateMachine;


private:

    Character*                 _character;  // Character to be controlled
	DynamicArray< SceneNode* > _colliders;  // A list of possible colliders


    class IdleState : public State
    {

    public:

        IdleState()
        {
            _name = "idle";
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt );

        void setOwner( Character* owner );

    private:

        Character* _owner;

    }_idleState;

    class HuntState : public State
    {

    public:

        HuntState()
        {
            _name = "hunt";
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt ){}

        void setOwner( Character* owner );

    private:

        Character* _owner;

    }_huntState;

    class DeadState : public State
    {

    public:

        DeadState()
        {
            _name = "dead";
        }

        void tick( float dt ){}
        void preTick( float dt ){}
        void postTick( float dt );

        void setOwner( Character* owner );

    private:

        Character* _owner;

    }_deadState;

    class ThirstState : public State
    {

    public:

        ThirstState()
        {
            _name = "thirst";
            _affordable = false;
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt ){}

        void setOwner( Character* owner );

    private:

        Character*  _owner;
        Building*   _hall;
        bool        _affordable;
        std::string _ale;

    }_thirstState;

    class ExhaustedState : public State
    {

    public:

        ExhaustedState()
        {
            _name = "exhausted";
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt );
        void wakeUp();

        void setOwner( Character* owner );

    private:

        Character* _owner;
        int        _lastHealth;

    }_exhaustedState;

    class LowHealthState : public State
    {

    public:

        LowHealthState()
        {
            _name = "low_health";
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt ){}
        void wakeUp();

        void setOwner( Character* owner );

    private:

        Character*  _owner;
        Building*   _apothecary;
        bool        _affordable;
        std::string _potion;

    }_lowHealthState;

    class BuyEquipmentState : public State
    {

    public:

        BuyEquipmentState()
        {
            _name = "buy_equipment";
            _affordable = false;
        }

        void tick( float dt );
        void preTick( float dt );
        void postTick( float dt ){}

        void setOwner( Character* owner );
        void setEquipmentType( std::string type );

    private:

        Character*  _owner;
        Building*   _blackSmith;
        std::string _equipmentType;
        std::string _equipment;
        bool        _affordable;

    }_buyEquipmentState;
};
