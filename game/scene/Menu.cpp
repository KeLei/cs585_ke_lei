#include "Menu.h"

Menu::Menu()
{
    _selection = 0;
    _longest = 0;
    _win = NULL;
}

void Menu::setWindow( WINDOW* win )
{
    _win = win;
    keypad( _win, true );
}

void Menu::show()
{
    unsigned x, y;
    getmaxyx( _win, y, x );
    if( y < _options.size() + 2 )
    {
        y = _options.size() + 2;
    }
    if( x < _longest + 2 )
    {
        x = _longest + 2;
    }

    wresize( _win, y, x );
    box( _win, 0, 0 );
    wattron( _win, _attribute );
    for( unsigned i = 0; i < _options.size(); i++ )
    {
        if( i != _selection )
        {
            mvwprintw( _win, i + 1, 1, _options[ i ].c_str() );
        }
        else
        {
            wattron( _win, A_REVERSE );
            mvwprintw( _win, i + 1, 1, _options[ i ].c_str() );
            wattroff( _win, A_REVERSE );
        }
    }
    wattroff( _win, _attribute );
    wrefresh( _win );
}

void Menu::hide()
{

}

void Menu::update()
{
    int c = InputHandler::getInstance() -> getCurrentKey();

    if( c == KEY_UP )
    {
        _selection--;
        if( _selection < 0 )
        {
            _selection = _options.size() - 1;
        }
    }
    if( c == KEY_DOWN )
    {
        _selection++;
        if( _selection > _options.size() - 1 )
        {
            _selection = 0;
        }
    }
    if( c == 'y' )
    {
        MenuSelectionEvent* event = new MenuSelectionEvent;
        event -> selection = _selection;
        _dispatcher.dispatch( event );
        delete event;
    }
}

void Menu::addOption( std::string option )
{
    if( option.length() > _longest )
    {
        _longest = option.length();
    }
    _options.pushBack( option );
}
void Menu::setAttribute( int attribute )
{
    _attribute = attribute;
}

unsigned Menu::selected()
{
    return _selection;
}

Dispatcher* Menu::getDispatcher()
{
    return &_dispatcher;
}
