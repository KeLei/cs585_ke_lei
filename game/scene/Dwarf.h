#pragma once

#include "Character.h"

class Dwarf : public Character
{

public:

    Dwarf(){}
    ~Dwarf(){}

    unsigned getExhaustion();
    unsigned getExhaustionThreshold();
    unsigned getThirst();
    unsigned getThirstThreshold();
    unsigned getStepCost();
    unsigned getGoldThreshold();

    void setExhaustion(unsigned exhaustion);
    void setExhaustionThreshold( unsigned exhaustionThreshold );
    void setThirst( unsigned thirst );
    void setThirstThreshold( unsigned thirstThreshold );
    void setStepCost( unsigned stepCost );
    void setGoldThreshold( unsigned goldThreshold );

private:

    unsigned _exhaustion;
    unsigned _exhaustionThreshold;
    unsigned _thirst;
    unsigned _thirstThreshold;
    unsigned _stepCost;
    unsigned _goldThreshold;

    unsigned _team;
};
