#pragma once

#include "../../engine/scene/IDrawable.h"
#include "../../engine/scene/ASCIIDrawable.h"
#include "../../engine/structures/DynamicArray.h"


class MultiDrawable : public IDrawable
{
public:

    MultiDrawable() {}
    ~MultiDrawable() {}

    void render( Camera camera );
    void setPosition( int x, int y );
    void setSize( int width, int height );
    void setWindow( WINDOW* win );
    void insertTile( ASCIIDrawable drawable );
    void insertBrick( int index );

    /*
    Function:	Add an attribute to this drawable
    Return:		void
    Parameters:	attribute - attribute to be added
    */
    void attributeOn( int attribute );

    /*
    Function:	Remove an attribute from this drawable
    Return:		void
    Parameters:	attribute - attribute to be removed
    */
    void attributeOff( int attribute );

private:

    DynamicArray< ASCIIDrawable > _tiles;
    DynamicArray< int >           _bricks;
    Point2D                       _size;
    Point2D                       _topLeft;
    WINDOW*                       _win;
};
