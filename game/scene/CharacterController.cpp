#include "CharacterController.h"

CharacterController::CharacterController()
{
    _stateMachine.addState( "idle", &_idleState );
    _stateMachine.addState( "hunt", &_huntState );
    _stateMachine.addState( "dead", &_deadState );
    _stateMachine.addState( "thirst", &_thirstState );
    _stateMachine.addState( "exhausted", &_exhaustedState );
    _stateMachine.addState( "low_health", &_lowHealthState );
    _stateMachine.addState( "buy_equipment", &_buyEquipmentState );
    _stateMachine.setInitialState( "idle" );
    _stateMachine.controller = this;

    _idleState.getDispatcher() -> addListener( "target_assigned", &_stateMachine );
    _idleState.getDispatcher() -> addListener( "low_health", &_stateMachine );
    _idleState.getDispatcher() -> addListener( "need_equipment", &_stateMachine );
    _idleState.getDispatcher() -> addListener( "thirsty", &_stateMachine );
    _idleState.getDispatcher() -> addListener( "exhausted", &_stateMachine );

    _huntState.getDispatcher() -> addListener( "death", &_stateMachine );
    _huntState.getDispatcher() -> addListener( "low_health", &_stateMachine );
    _huntState.getDispatcher() -> addListener( "death", this );

    _thirstState.getDispatcher() -> addListener( "finish", &_stateMachine );
    _thirstState.getDispatcher() -> addListener( "death", &_stateMachine );
    _thirstState.getDispatcher() -> addListener( "death", this );

    _exhaustedState.getDispatcher() -> addListener( "finish", &_stateMachine );

    _lowHealthState.getDispatcher() -> addListener( "finish", &_stateMachine );
    _lowHealthState.getDispatcher() -> addListener( "death", &_stateMachine );
    _lowHealthState.getDispatcher() -> addListener( "death", this );

    _buyEquipmentState.getDispatcher() -> addListener( "finish", &_stateMachine );
}

CharacterController::~CharacterController()
{
}

void CharacterController::preTick( float dt )
{
    _stateMachine.preTick( dt );

    if( _character -> getHealth() <= _character -> getMaxHealth() / 2 && _character -> getPotion() != NULL )
    {
        Debug::getInstance() -> log( "main", GAMEPLAY, "%s used a potion, health increased %d", _character -> getName().c_str(), _character -> getPotion() -> getPotionValue() );
        _character -> usePotion();
    }

    // Get a list of possible colliders
    _colliders = SceneManager::getInstance() -> getSceneGraph() -> getColliders( _character -> getSceneNode() );
    searchNearby();
    Point2D position = _character -> getSceneNode() -> getCenter();
    Point2D destination = _character -> getDestination();

    // Move towards destination
    int direction = -1;
    if( ( int )Math::abs( position.x - destination.x ) > ( int )Math::abs( position.y - destination.y ) )
    {
        if( ( int )position.x - ( int )destination.x > 0 )
        {
            direction = 2;
        }
        else
        {
            direction = 0;
        }
    }
    else
    {
        if( ( int )position.y - ( int )destination.y > 0 )
        {
            direction = 3;
        }
        else if( ( int )position.y - ( int )destination.y < 0 )
        {
            direction = 1;
        }
        else
        {
            direction = -1;
        }
    }

    switch( direction )
    {
    case 0: position.x += _character -> getSpeed(); break;
    case 1: position.y += _character -> getSpeed(); break;
    case 2: position.x -= _character -> getSpeed(); break;
    case 3: position.y -= _character -> getSpeed(); break;
    default: break;
    }

    position.x = Math::cast( 0.0f, 44.0f, position.x );
    position.y = Math::cast( 0.0f, 24.0f, position.y );

    // Update node to a new position
    SceneManager::getInstance() -> getSceneGraph() -> updateSceneNode( _character -> getSceneNode(), position );
    _character -> setDirection( direction );

    // Update exhaustion and thirst
    if( _character -> getType() == "dwarf" )
    {
        Dwarf* dwarf = dynamic_cast< Dwarf* >( _character );
        if( dwarf -> getTimer( "thirst_timer" ) -> isTimeout() )
        {
            dwarf -> setThirst( dwarf -> getThirst() + 1 );
            dwarf -> getTimer( "thirst_timer" ) -> restart();
        }
        if( dwarf -> getTimer( "exhaust_timer" ) -> isTimeout() )
        {
            dwarf -> setExhaustion( dwarf -> getExhaustion() + 1 );
            dwarf -> getTimer( "exhaust_timer" ) -> restart();
        }
    }
}

void CharacterController::tick( float dt )
{
    _stateMachine.tick( 0.0f );
    _character -> setCurrentState( _stateMachine.getCurrentState() -> getName() );
}

void CharacterController::postTick( float dt )
{
    _stateMachine.postTick( dt );
}

void CharacterController::setCharacter( Character* character )
{
    _character = character;
    _idleState.setOwner( _character );
    _huntState.setOwner( _character );
    _deadState.setOwner( _character );
    _thirstState.setOwner( _character );
    _exhaustedState.setOwner( _character );
    _lowHealthState.setOwner( _character );
    _buyEquipmentState.setOwner( _character );

    _character -> setCurrentState( _idleState.getName() );

    if( _character -> getType() == "dwarf" )
    {
        _character -> getTimer( "thirst_timer" ) -> start();
        _character -> getTimer( "exhaust_timer" ) -> start();
    }
}

void CharacterController::searchNearby()
{
    if( _colliders.isEmpty() )
    {
        _character -> setTarget( NULL );
        return;
    }

    Actor* target = NULL;
    float _distance = 0.0f;
    float _minDis = MAX_FLOAT;

	Point2D position = _character -> getSceneNode() -> getCenter();
    for( unsigned i = 0; i < _colliders.size(); i++ )
    {
        target = dynamic_cast< Actor* > ( _colliders[ i ] -> getCollidable() );
        Point2D targetPos = target -> getSceneNode() -> getCenter();
        if( target -> getType() == _character -> getTargetType() )
        {
            _distance = Math::abs( position.x - targetPos.x ) + Math::abs( position.y - targetPos.y );
            if( _minDis > _distance )
            {
                _minDis = _distance;
                _character -> setTarget( dynamic_cast< Character* >( target ) );
            }
        }
    }
}

bool CharacterController::isCollideWith( SceneNode* node )
{
    AABB actorRect = _character -> getSceneNode() -> getRect();
    AABB targetRect = node -> getRect();

    return ( actorRect.rightBound() > targetRect.leftBound() &&
             actorRect.leftBound() < targetRect.rightBound() &&
             actorRect.lowerBound() > targetRect.upperBound() &&
             actorRect.upperBound() < targetRect.lowerBound() );
}

Character* CharacterController::getCharacter()
{
    return _character;
}

void CharacterController::CharacterStateMachine::onStateTransition( IEvent* event )
{
    if( event -> getType() == "target_assigned" )
    {
        _currentState = _stateMap[ "hunt" ];
    }
    else if( event -> getType() == "death" )
    {
        DeathEvent* death = dynamic_cast< DeathEvent* >( event );
        Character* instigator = dynamic_cast< Character* >( death -> instigator );
        if( instigator != controller -> getCharacter() )
        {
            _currentState = _stateMap[ "dead" ];
        }
        else
        {
            _currentState = _stateMap[ "idle" ];
        }
    }
    else if( event -> getType() == "thirsty" )
    {
        _currentState = _stateMap[ "thirst" ];
    }
    else if( event -> getType() == "exhausted" )
    {
        _currentState = _stateMap[ "exhausted" ];
    }
    else if( event -> getType() == "finish" )
    {
        _currentState = _stateMap[ "idle" ];
    }
    else if( event -> getType() == "low_health" )
    {
        _currentState = _stateMap[ "low_health" ];
    }
    else if( event -> getType() == "need_equipment" )
    {
        NeedEquimentEvent* need = dynamic_cast< NeedEquimentEvent* >( event );
        _currentState = _stateMap[ "buy_equipment" ];
        dynamic_cast< BuyEquipmentState* >( _currentState ) -> setEquipmentType( need -> equipmentType );
    }
}

void CharacterController::execute( IEvent* event )
{
    if( event -> getType() == "death")
    {
        DeathEvent* death = dynamic_cast< DeathEvent* >( event );
        Character* instigator = dynamic_cast< Character* >( death -> instigator );
        if( instigator != _character )
        {
            Debug::getInstance()->log( "main", GAMEPLAY, "%s %s's controller is going to be deleted", _character->getType().c_str(), _character->getName().c_str() );
            SceneManager::getInstance() -> removeTickable( this );
        }
    }
}



/*
    State ticks
*/

void CharacterController::IdleState::preTick( float dt )
{
    if( !_owner -> isInCommand() )
    {
        // No enemy, no command, Brownie motion
        Point2D destination = _owner -> getSceneNode() -> getCenter();

        int direction = ( int )Random::getInstance() -> getRandom( 4.0f );
        switch( direction )
        {
        case 0:
            destination.x += 1.0f;
            break;
        case 1:
            destination.y += 1.0f;
            break;
        case 2:
            destination.x -= 1.0f;
            break;
        case 3:
            destination.y -= 1.0f;
            break;
        }
        _owner -> setDestination( destination );
    }
    else
    {
        // Moved to destination
        if( _owner -> getDirection() == -1 )
        {
            _owner -> releaseCommand();
        }
    }
}

void CharacterController::IdleState::tick( float dt )
{
    if( _owner -> getType() == "dwarf" )
    {
        if( !_owner -> isInCommand() )
        {
            Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );
            if( dwarf -> getHealth() <= dwarf -> getMaxHealth() / 2 &&
                LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "apothecary" ) != NULL &&
                dwarf -> getGold() >= LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "apothecary" ) -> getPrice( "Lesser_Potion" ) )
            {
                Debug::getInstance() -> log( "main", GAMEPLAY, "Dwarf %s is in low health!", dwarf -> getName().c_str() );

                // Change to low health state
                LowHealthEvent* event = new LowHealthEvent;
                event -> owner = _owner;
                _dispatcher.dispatch( event );
                delete event;
            }
            else if( dwarf -> getExhaustion() >= dwarf -> getExhaustionThreshold() )
            {
                // Exhausted, need sleep
                ExhaustedEvent* exhaustedEvent = new ExhaustedEvent;
                exhaustedEvent -> owner = _owner;
                _dispatcher.dispatch( exhaustedEvent );
                delete exhaustedEvent;

                Debug::getInstance() -> log( "main", GAMEPLAY, "Dwarf %s is exhausted", dwarf -> getName().c_str() );
                return;
            }
            else if( dwarf -> getGold() >= 100 && dwarf -> getThirst() >= dwarf -> getThirstThreshold() )
            {
                // Get enough gold to buy a drink and is thirsty
                ThirstEvent* thirstEvent = new ThirstEvent;
                thirstEvent -> owner = _owner;
                _dispatcher.dispatch( thirstEvent );
                delete thirstEvent;

                Debug::getInstance() -> log( "main", GAMEPLAY, "Dwarf %s is going to get some drink", dwarf -> getName().c_str() );
                return;
            }
            else if( _owner -> getTarget() != NULL )
            {
                TargetEvent* targetEvent = new TargetEvent;
                targetEvent -> target = _owner -> getTarget();
                _dispatcher.dispatch( targetEvent );
                delete targetEvent;
            }
            else if( dwarf -> getWeapon() == NULL &&
                    LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "black_smith" ) != NULL )
            {
                Debug::getInstance() -> log( "main", GAMEPLAY, "Dwarf %s want a weapon", dwarf -> getName().c_str() );
                NeedEquimentEvent* event = new NeedEquimentEvent;
                event -> equipmentType = "weapon";
                _dispatcher.dispatch( event );
                delete event;
            }
            else if( dwarf -> getArmor() == NULL &&
                    LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "black_smith" ) != NULL )
            {
                Debug::getInstance() -> log( "main", GAMEPLAY, "Dwarf %s want an armor", dwarf -> getName().c_str() );
                NeedEquimentEvent* event = new NeedEquimentEvent;
                event -> equipmentType = "armor";
                _dispatcher.dispatch( event );
                delete event;
            }
        }
    }

    // If its an ogre or a dwarf, find target
    if( _owner -> getTarget() != NULL && _owner -> getType() == "ogre" )
    {
        TargetEvent* targetEvent = new TargetEvent;
        targetEvent -> target = _owner -> getTarget();
        _dispatcher.dispatch( targetEvent );
        delete targetEvent;
    }
}

void CharacterController::IdleState::postTick( float dt )
{
    _owner -> setTarget( NULL );
}

void CharacterController::IdleState::setOwner( Character* owner )
{
    _owner = owner;
}

void CharacterController::HuntState::preTick( float dt )
{
	Point2D position = _owner -> getSceneNode() -> getCenter();
    Point2D targetPos = _owner -> getTarget() -> getSceneNode() -> getCenter();

    if( _owner -> getDirection() == -1 )
    {
        // Target in range
        Timer* attackTimer = _owner -> getTimer( "attack" );
        if( attackTimer -> isStarted() )
        {
            if( attackTimer -> isTimeout() )
            {
                if( _owner -> getWeapon() != NULL )
                {
                    _owner -> getTarget() -> beingAttack( _owner -> getWeapon() -> getAttack() );
                    _owner -> getWeapon() -> setDurability( _owner -> getWeapon() -> getDurability() - 1 );
                    if( _owner -> getWeapon() -> getDurability() == 0 )
                    {
                        // Delete broken weapon
                        delete _owner -> getWeapon();
                        _owner -> setWeapon( NULL );
                    }
                }
                else
                {
                    _owner -> getTarget() -> beingAttack( _owner -> getAttack() );
                }

                if( _owner -> getTarget() -> isAlive() == false )
                {
                    // I killed the character
                    _owner -> increaseGold( _owner -> getTarget() -> getGold() );
                    attackTimer -> stop();
                }
                attackTimer -> restart();
            }
        }
        else
        {
            attackTimer -> start();
        }

        // Don't move,
        _owner -> setDestination( position );
    }
    else
    {
        // Set destination to target
        _owner -> setDestination( targetPos );
    }
}

void CharacterController::HuntState::tick( float dt )
{
    if( _owner -> getTarget() -> isAlive() == false )
    {
        DeathEvent* deathEvent = new DeathEvent;
        deathEvent -> instigator = _owner;
        deathEvent -> owner = _owner -> getTarget();
        _owner -> setTarget( NULL );
        Debug::getInstance() -> log( "main", GAMEPLAY, "%s's target died",_owner -> getName().c_str() );
        _dispatcher.dispatch( deathEvent );
        delete deathEvent;
    }
    if( _owner -> isAlive() == false )
    {
        DeathEvent* deathEvent = new DeathEvent;
        deathEvent -> instigator= _owner -> getTarget();
        deathEvent -> owner = _owner;

        _owner -> getDispatcher() -> dispatch( deathEvent );
        _dispatcher.dispatch( deathEvent );
        delete deathEvent;
    }
//    if( _owner -> getType() == "dwarf" &&
//        _owner ->getHealth() < _owner -> getMaxHealth() / 2 &&
//        LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "apothecary" ) != NULL )
//    {
//        LowHealthEvent* event = new LowHealthEvent;
//        event -> owner = _owner;
//        _dispatcher.dispatch( event );
//        delete event;
//    }
}

void CharacterController::HuntState::setOwner( Character* owner )
{
    _owner = owner;
}

void CharacterController::DeadState::postTick( float dt )
{
    Debug::getInstance() -> log( "main", GAMEPLAY, "%s is removed from scene", _owner -> getName().c_str() );
    SceneManager::getInstance() -> getSceneGraph() -> removeSceneNode( _owner -> getSceneNode() );
    delete _owner -> getSceneNode();
}

void CharacterController::DeadState::setOwner( Character* owner )
{
    _owner = owner;
}

void CharacterController::ThirstState::preTick( float dt )
{
    Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );
    _hall = LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "great_hall" );
    _owner -> setDestination( _hall -> getSceneNode() -> getCenter() );

    int price;
    if( _owner -> getDirection() == -1 )
    {
        // Reach great hall
        Timer* drinkTimer = _owner -> getTimer( "drink" );
        if( !_affordable )
        {
            for( unsigned i = _hall -> getItemList().size() - 1; i >= 0; i-- )
            {
                price = _hall -> getPrice( _hall -> getItemList()[ i ] );
                if( dwarf -> getGold() >= price )
                {
                    _affordable = true;
                    _ale = _hall -> getItemList()[ i ];
                    break;
                }
            }
        }
        if( _affordable )
        {
            if( drinkTimer -> isStarted() )
            {
                if( drinkTimer -> isTimeout() )
                {
                    Debug::getInstance() -> log( "main", GAMEPLAY, "%s bought %s, used %d!", dwarf -> getName().c_str(), _ale.c_str(), _hall -> getPrice( _ale ) );
                    dwarf -> increaseGold( -_hall -> getPrice( _ale ) );
                    _hall -> sell( dwarf, _ale );
                    drinkTimer -> restart();
                    _affordable = false;
                }
            }
            else
            {
                drinkTimer -> start();
            }
        }
    }
}

void CharacterController::ThirstState::tick( float dt )
{
    Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );

    if( !dwarf -> isAlive() )
    {
        DeathEvent* deathEvent = new DeathEvent;
        deathEvent -> owner = _owner;
        _owner -> getDispatcher() -> dispatch( deathEvent );
        _dispatcher.dispatch( deathEvent );
        delete deathEvent;
    }
    else if( dwarf -> getThirst() <= 0 || dwarf -> getGold() < _hall -> getPrice( _hall -> getItemList()[ 0 ] ) )
    {
        Timer* drinkTimer = _owner -> getTimer( "drink" );
        FinishEvent* finishEvent = new FinishEvent;
        finishEvent -> owner = dwarf;
        finishEvent -> objective = "drink";
        _dispatcher.dispatch( finishEvent );
        delete finishEvent;
        drinkTimer->stop();
    }
}

void CharacterController::ThirstState::setOwner( Character* owner )
{
    _owner = owner;
}

void CharacterController::ExhaustedState::preTick( float dt )
{
    // Sleeping, don't move
    _owner -> setDestination( _owner -> getSceneNode() -> getCenter() );
}

void CharacterController::ExhaustedState::tick( float dt )
{
    Timer* sleepTimer = _owner -> getTimer( "sleep" );
    if( _owner -> getHealth() < _lastHealth )
    {
        wakeUp();
        sleepTimer -> stop();
    }
    if( !sleepTimer -> isStarted() )
    {
        sleepTimer -> start();
    }
    else
    {
        if( sleepTimer -> isTimeout() )
        {
            // Switch to idle state
            wakeUp();
            sleepTimer -> stop();
        }
    }
}

void CharacterController::ExhaustedState::postTick( float dt )
{
    _lastHealth = _owner -> getHealth();
}

void CharacterController::ExhaustedState::wakeUp()
{
    FinishEvent* finishEvent = new FinishEvent;
    finishEvent -> owner = _owner;
    finishEvent -> objective = "sleep";
    _dispatcher.dispatch( finishEvent );
    delete finishEvent;
    Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );
    dwarf -> setExhaustion( 0 );
}

void CharacterController::ExhaustedState::setOwner( Character* owner )
{
    _owner = owner;
}

void CharacterController::LowHealthState::tick( float dt )
{
    Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );

    if( !dwarf -> isAlive() )
    {
        DeathEvent* deathEvent = new DeathEvent;
        deathEvent -> owner = _owner;
        deathEvent -> instigator = NULL;
        _owner -> getDispatcher() -> dispatch( deathEvent );
        _dispatcher.dispatch( deathEvent );
        delete deathEvent;
    }
    else if( ( dwarf -> getHealth() > dwarf -> getMaxHealth() / 2 && dwarf -> getPotion() != NULL )
            || dwarf -> getGold() < _apothecary -> getPrice( _apothecary -> getItemList()[ 0 ] )
            || dwarf -> isInCommand() )
    {
        FinishEvent* finishEvent = new FinishEvent;
        finishEvent -> owner = dwarf;
        finishEvent -> objective = "buy potion";
        _dispatcher.dispatch( finishEvent );
        delete finishEvent;
    }
}

void CharacterController::LowHealthState::preTick( float dt )
{
    Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );

    if( !dwarf -> isInCommand() )
    {
        _apothecary = LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "apothecary" );
        _owner -> setDestination( _apothecary -> getSceneNode() -> getCenter() );
        int price;
        if( _owner -> getDirection() == -1 )
        {
            // Reach apothecary
            if( !_affordable )
            {
                for( unsigned i = _apothecary -> getItemList().size() - 1; i >= 0; i-- )
                {
                    price = _apothecary -> getPrice( _apothecary -> getItemList()[ i ] );
                    if( dwarf -> getGold() >= price )
                    {
                        _affordable = true;
                        _potion = _apothecary -> getItemList()[ i ];
                        break;
                    }
                }
            }
            if( _affordable )
            {
                Debug::getInstance() -> log( "main", GAMEPLAY, "%s bought %s, used %d!", dwarf -> getName().c_str(), _potion.c_str(), _apothecary -> getPrice( _potion ) );
                dwarf -> increaseGold( -_apothecary -> getPrice( _potion ) );
                _apothecary -> sell( dwarf, _potion );
                _affordable = false;
            }
        }
    }
}

void CharacterController::LowHealthState::setOwner( Character* owner )
{
    _owner = owner;
}


void CharacterController::BuyEquipmentState::tick( float dt )
{
    Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );
    bool willbuy = true;
    if( _equipmentType == "weapon" &&
       ( dwarf -> getGold() < _blackSmith -> getPrice( _blackSmith -> getItemList()[ 0 ] ) || dwarf -> getWeapon() != NULL ) )
    {
        // Can't afford or no need to buy
        willbuy = false;
    }

    if( _equipmentType == "armor" &&
       ( dwarf -> getGold() < _blackSmith -> getPrice( _blackSmith -> getItemList()[ 1 ] ) || dwarf -> getArmor() != NULL ) )
    {
        // Can't afford or no need to buy
        willbuy = false;
    }

    if( !willbuy || dwarf -> isInCommand() )
    {
        FinishEvent* finishEvent = new FinishEvent;
        finishEvent -> owner = dwarf;
        finishEvent -> objective = "buy equipment";
        _dispatcher.dispatch( finishEvent );
        delete finishEvent;
    }
}

void CharacterController::BuyEquipmentState::preTick( float dt )
{
    Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );

    if( !dwarf -> isInCommand() )
    {
        _blackSmith = LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "black_smith" );
        _owner -> setDestination( _blackSmith -> getSceneNode() -> getCenter() );
        int price;
        if( _owner -> getDirection() == -1 )
        {
            // Reach black smith
            if( !_affordable )
            {
                for( unsigned i = _blackSmith -> getItemList().size() - 1; i >= 0; i-- )
                {
                    price = _blackSmith -> getPrice( _blackSmith -> getItemList()[ i ] );
                    if( _blackSmith -> getItem( _blackSmith -> getItemList()[ i ] ) -> getType() == _equipmentType &&
                        dwarf -> getGold() >= price )
                    {
                        _affordable = true;
                        _equipment = _blackSmith -> getItemList()[ i ];
                        break;
                    }
                }
            }
            if( _affordable )
            {
                Debug::getInstance() -> log( "main", GAMEPLAY, "%s bought %s, used %d!", dwarf -> getName().c_str(), _equipment.c_str(), _blackSmith -> getPrice( _equipment ) );
                dwarf -> increaseGold( -_blackSmith -> getPrice( _equipment ) );
                _blackSmith -> sell( dwarf, _equipment );
                _affordable = false;
            }
        }
    }
}

void CharacterController::BuyEquipmentState::setOwner( Character* owner )
{
    _owner = owner;
}

void CharacterController::BuyEquipmentState::setEquipmentType( std::string type )
{
    _equipmentType = type;
}
