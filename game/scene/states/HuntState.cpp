#include "HuntState.h"

void HuntState::preTick( float dt )
{
	Point2D position = _owner -> getSceneNode() -> getCenter();
    Point2D targetPos = _owner -> getTarget() -> getSceneNode() -> getCenter();

    if( _owner -> getDirection() == -1 )
    {
        // Target in range
        Timer* attackTimer = _owner -> getTimer( "attack" );
        if( attackTimer -> isStarted() )
        {
            if( attackTimer -> isTimeout() )
            {
                if( _owner -> getWeapon() != NULL )
                {
                    _owner -> getTarget() -> beingAttack( _owner -> getWeapon() -> getAttack() );
                    _owner -> getWeapon() -> setDurability( _owner -> getWeapon() -> getDurability() - 1 );
                    if( _owner -> getWeapon() -> getDurability() == 0 )
                    {
                        // Delete broken weapon
                        delete _owner -> getWeapon();
                        _owner -> setWeapon( NULL );
                    }
                }
                else
                {
                    _owner -> getTarget() -> beingAttack( _owner -> getAttack() );
                }

                if( _owner -> getTarget() -> isAlive() == false )
                {
                    // I killed the character
                    _owner -> increaseGold( _owner -> getTarget() -> getGold() );
                    attackTimer -> stop();
                }
                attackTimer -> restart();
            }
        }
        else
        {
            attackTimer -> start();
        }

        // Don't move
        _owner -> setDestination( position );
    }
    else
    {
        // Set destination to target
        _owner -> setDestination( targetPos );
    }
}

void HuntState::tick( float dt )
{
    if( _owner -> getTarget() -> isAlive() == false )
    {
        DeathEvent* deathEvent = new DeathEvent;
        deathEvent -> instigator = _owner ;
        deathEvent -> owner = _owner -> getTarget();
        _owner -> setTarget( NULL );
        Debug::getInstance() -> log( "main", GAMEPLAY, "%s's target died",_owner -> getName().c_str() );
        _dispatcher.dispatch( deathEvent );
        delete deathEvent;
    }
    if( _owner -> isAlive() == false )
    {
        DeathEvent* deathEvent = new DeathEvent;
        deathEvent -> instigator= _owner -> getTarget();
        deathEvent -> owner = _owner;

        _owner -> getDispatcher() -> dispatch( deathEvent );
        _dispatcher.dispatch( deathEvent );
        delete deathEvent;
    }
    if( _owner ->getHealth() < _owner -> getMaxHealth() / 2 && LevelManager::getInstance() -> getCurrentLevel() -> getBuilding( "apothecary" ) != NULL )
    {
        LowHealthEvent* event = new LowHealthEvent;
        event -> owner = _owner;
        _dispatcher.dispatch( event );
        delete event;
    }
}

void HuntState::setOwner( Character* owner )
{
    _owner = owner;
}
