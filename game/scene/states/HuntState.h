#pragma once
#include "../../../engine/random/Random.h"
#include "../../../engine/debug/Debug.h"
#include "../../event/TargetEvent.h"
#include "../../event/FinishEvent.h"
#include "../../LevelManager.h"
#include "../Dwarf.h"

class HuntState : public State
{

public:

    HuntState()
    {
        _name = "hunt";
    }

    void tick( float dt );
    void preTick( float dt );
    void postTick( float dt ) {}

    void setOwner( Character* owner );

private:

    Character* _owner;

};
