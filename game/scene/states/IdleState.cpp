#include "IdleState.h"

void IdleState::preTick( float dt )
{
    // No enemy, Brownie motion
    Point2D destination = _owner -> getSceneNode() -> getCenter();

    int direction = ( int )Random::getInstance() -> getRandom( 4.0f );
    switch( direction )
    {
    case 0: destination.x += 1.0f; break;
    case 1: destination.y += 1.0f; break;
    case 2: destination.x -= 1.0f; break;
    case 3: destination.y -= 1.0f; break;
    }
    _owner -> setDestination( destination );
}

void IdleState::tick( float dt )
{
    if( _owner -> getType() == "dwarf" )
    {
        Dwarf* dwarf = dynamic_cast< Dwarf* >( _owner );
        if( dwarf -> getExhaustion() >= dwarf -> getExhaustionThreshold() )
        {
            // Exhausted, need sleep
            ExhaustedEvent* exhaustedEvent = new ExhaustedEvent;
            exhaustedEvent -> owner = _owner;
            _dispatcher.dispatch( exhaustedEvent );
            delete exhaustedEvent;

            Debug::getInstance() -> log( "main", GAMEPLAY, "Dwarf %s exhausted", dwarf -> getName().c_str() );
            return;
        }
        else if( dwarf -> getGold() >= 100 && dwarf -> getThirst() >= dwarf -> getThirstThreshold() )
        {
            // Get enough gold to buy a drink and is thirsty
            ThirstEvent* thirstEvent = new ThirstEvent;
            thirstEvent -> owner = _owner;
            _dispatcher.dispatch( thirstEvent );
            delete thirstEvent;

            Debug::getInstance() -> log( "main", GAMEPLAY, "Dwarf %s is going to get some drink", dwarf -> getName().c_str() );
            return;
        }
    }

    // If its an ogre or a dwarf
    if( _owner -> getTarget() != NULL )
    {
        TargetEvent* targetEvent = new TargetEvent;
        targetEvent -> target = _owner -> getTarget();
        _dispatcher.dispatch( targetEvent );
        delete targetEvent;
    }
}

void IdleState::postTick( float dt )
{
    _owner -> setTarget( NULL );
}

void IdleState::setOwner( Character* owner )
{
    _owner = owner;
}

