#pragma once
#include "../../../engine/random/Random.h"
#include "../../../engine/debug/Debug.h"
#include "../../event/TargetEvent.h"
#include "../../event/ThirstEvent.h"
#include "../../event/ExhaustedEvent.h"
#include "../../event/FinishEvent.h"
#include "../Character.h"
#include "../Dwarf.h"

class IdleState : public State
{

public:

    IdleState()
    {
        _name = "idle";
    }

    void tick( float dt );
    void preTick( float dt );
    void postTick( float dt );

    void setOwner( Character* owner );

private:

    Character* _owner;

};
