#include "Dwarf.h"

unsigned Dwarf::getExhaustion()
{
    return _exhaustion;
}

unsigned Dwarf::getExhaustionThreshold()
{
    return _exhaustionThreshold;
}

unsigned Dwarf::getThirst()
{
    return _thirst;
}

unsigned Dwarf::getThirstThreshold()
{
    return _thirstThreshold;
}

unsigned Dwarf::getStepCost()
{
    return _stepCost;
}

unsigned Dwarf::getGoldThreshold()
{
    return _goldThreshold;
}

void Dwarf::setExhaustion(unsigned exhaustion)
{
    _exhaustion = exhaustion;
}

void Dwarf::setExhaustionThreshold( unsigned exhaustionThreshold )
{
    _exhaustionThreshold = exhaustionThreshold;
}

void Dwarf::setThirst( unsigned thirst )
{
    _thirst = thirst;
}

void Dwarf::setThirstThreshold( unsigned thirstThreshold )
{
    _thirstThreshold = thirstThreshold;
}

void Dwarf::setStepCost( unsigned stepCost )
{
    _stepCost = stepCost;
}

void Dwarf::setGoldThreshold( unsigned goldThreshold )
{
    _goldThreshold = goldThreshold;
}
