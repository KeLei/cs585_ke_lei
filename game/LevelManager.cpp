#include "LevelManager.h"

LevelManager* LevelManager::_instance = NULL;

LevelManager::LevelManager()
{
    _level = NULL;
}

LevelManager* LevelManager::getInstance()
{
    if( _instance == NULL )
    {
        _instance = new LevelManager;
    }

    return _instance;
}


Level* LevelManager::getCurrentLevel()
{
    return _level;
}

void LevelManager::setLevel( Level* level )
{
    _level = level;
}
