#pragma once
#include "Level.h"

/*
Simple Level manager with only one level, singleton
*/
class Level;
class LevelManager
{

public:

    // Get an instance of level manager
    static LevelManager* getInstance();

	/*
	Function:	Get current level
	Return:		Current level
	Parameters:	void
	*/
    Level* getCurrentLevel();

  	/*
	Function:	Set current level
	Return:		void
	Parameters:	level - current level
	*/
    void setLevel( Level* level );

private:

    LevelManager();

    static LevelManager* _instance; // Instance of the manager
    Level*               _level;    // Reference to the level

};
