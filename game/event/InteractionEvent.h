#pragma once
#include "../../engine/event/IEvent.h"

class InteractionEvent : public IEvent
{

public:

    Actor* target;

    std::string getType()
    {
        return "interaction";
    }
};
