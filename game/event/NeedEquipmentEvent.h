#pragma once
#include "../../engine/event/IEvent.h"
#include "../scene/Character.h"

class NeedEquimentEvent : public IEvent
{

public:

    Character* owner;
    std::string equipmentType;

    std::string getType()
    {
        return "need_equipment";
    }
};

