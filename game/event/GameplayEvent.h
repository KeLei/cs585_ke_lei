#pragma once
#include "../../engine/event/IEvent.h"

class GameplayEvent : public IEvent
{

public:

    std::string getType()
    {
        return "gameplay";
    }
};
