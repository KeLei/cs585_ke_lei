#pragma once
#include "../../engine/event/IEvent.h"
#include "../scene/Actor.h"

class ThirstEvent : public IEvent
{

public:

    Actor* owner;

    std::string getType()
    {
        return "thirsty";
    }
};
