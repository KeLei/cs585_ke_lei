#pragma once
#include "../../engine/event/IEvent.h"
#include "../scene/Actor.h"

class BuildEvent : public IEvent
{

public:

    std::string buildingName;
    Point2D     location;

    std::string getType()
    {
        return "build";
    }
};
