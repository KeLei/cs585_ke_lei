#pragma once
#include "../../engine/event/IEvent.h"
#include "../scene/Character.h"

class LowHealthEvent : public IEvent{

public:

    Character* owner;

    std::string getType()
    {
        return "low_health";
    }
};
