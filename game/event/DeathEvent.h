#pragma once
#include "../../engine/event/IEvent.h"
#include "../scene/Actor.h"

class FinishEvent : public IEvent
{

public:

    Actor* owner;
    std::string objective;

    std::string getType()
    {
        return "finish";
    }
};
