#pragma once
#include "../../engine/event/IEvent.h"

class SelectionEvent : public IEvent
{

public:

    std::string selectionMode;

    std::string getType()
    {
        return "selection";
    }
};
