#pragma once
#include "../../engine/event/IEvent.h"

class MenuSelectionEvent : public IEvent
{

public:

    unsigned selection;

    std::string getType()
    {
        return "menu_selection";
    }

};
