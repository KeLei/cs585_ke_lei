#pragma once
#include "../../engine/event/IEvent.h"

class TransactionEvent : public IEvent
{

public:

    int amount;
    std::string transactionType;

    std::string getType()
    {
        return "transaction";
    }
};
