#pragma once
#include "../../engine/event/IEvent.h"
#include "../scene/Actor.h"

class ExhaustedEvent : public IEvent
{

public:

    Actor* owner;

    std::string getType()
    {
        return "exhausted";
    }
};
