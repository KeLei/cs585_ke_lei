#pragma once
#include "../../engine/event/IEvent.h"
#include "../scene/Actor.h"

class TargetEvent : public IEvent
{

public:

    Actor* target;

    std::string getType()
    {
        return "target_assigned";
    }
};
