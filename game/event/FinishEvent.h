#pragma once
#include "../../engine/event/IEvent.h"
#include "../scene/Actor.h"

class DeathEvent : public IEvent
{

public:

    Actor* owner;
    Actor* instigator;

    std::string getType()
    {
        return "death";
    }
};
