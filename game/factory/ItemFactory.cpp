#include "ItemFactory.h"

Item* ItemFactory::getItem( std::string name )
{
    Item* item = NULL;
    JsonValue* config = AssetManager::getInstance() -> getConfig( name );
    std::string type = config -> getValueMap()[ "type" ] -> getStringValue();
    if( type == "weapon" )
    {
        item = new Weapon;
        Weapon* weapon = dynamic_cast< Weapon* >( item );
        weapon -> setAttack( ( unsigned )config -> getValueMap()[ "attack" ] -> getNumberValue() );
        weapon -> setCriticalAttack( ( unsigned )config -> getValueMap()[ "critical_attack" ] -> getNumberValue() );
        weapon -> setDurability( ( unsigned )config -> getValueMap()[ "durability" ] -> getNumberValue() );
        weapon -> setHitRate( config -> getValueMap()[ "hit_rate" ] -> getNumberValue() );
        weapon -> setCriticalRate( config -> getValueMap()[ "critical_rate" ] -> getNumberValue() );
    }
    else if( type == "armor" )
    {
        item = new Armor;
        Armor* armor = dynamic_cast< Armor* >( item );
        armor -> setDefense( ( unsigned )config -> getValueMap()[ "defense" ] -> getNumberValue() );
        armor -> setDurability( ( unsigned )config -> getValueMap()[ "durability" ] -> getNumberValue() );
    }
    else if( type == "potion" || type == "ale" )
    {
        item = new Potion;
        Potion* potion = dynamic_cast< Potion* >( item );
        potion -> setPotionValue( ( unsigned )config -> getValueMap()[ "value" ] -> getNumberValue() );
    }

    if( item != NULL )
    {
        item -> setType( type );
        item -> setPrice( ( unsigned )config -> getValueMap()[ "price" ] -> getNumberValue() );
    }

    return item;
}
