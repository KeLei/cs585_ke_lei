#pragma once

#include "../../engine/asset/AssetManager.h"
#include "../../engine/scene/SceneNodeFactory.h"
#include "../../engine/scene/SceneManager.h"
#include "../scene/Actor.h"
#include "../scene/building/Building.h"
#include "../scene/MultiDrawable.h"

class BuildingFactory
{

public:

    static Building* spawnBuilding( std::string name, int x, int y, WINDOW* win );

};
