#include "BuildingFactory.h"

Building* BuildingFactory::spawnBuilding( std::string name, int x, int y, WINDOW* win )
{
    Building* building;
    MultiDrawable* drawable = new MultiDrawable;
    JsonValue* config = AssetManager::getInstance() -> getConfig( name );

    building = new Building;
    building -> setName( name );

    // Add items to shop
    unsigned size = config -> getValueMap()[ "items" ] -> getValueArray().size();

    for( unsigned i = 0; i < size; i++ )
    {
        building -> addItem( config -> getValueMap()[ "items" ] -> getValueArray()[ i ] -> getStringValue() );
    }

    int width = ( int )config -> getValueMap()[ "width" ] -> getNumberValue();
    int height = ( int )config -> getValueMap()[ "height" ] -> getNumberValue();
    SceneNode* node = SceneNodeFactory::getInstance() -> getSceneNode( x, y, width, height );
    building -> setSceneNode( node );
    drawable -> setSize( width, height );

    unsigned tileSize = config -> getValueMap()[ "tiles" ] -> getValueArray().size();
    ASCIIDrawable tile;
    JsonValue* tileConfig;
    std::string tileName;
    for( unsigned i = 0; i < tileSize; i++ )
    {
        tileName = config -> getValueMap()[ "tiles" ] -> getValueArray()[ i ] -> getStringValue();
        tileConfig = AssetManager::getInstance() -> getConfig( tileName );

        tile.setCharacter( tileConfig -> getValueMap()[ "character" ] -> getStringValue()[ 0 ] );
        tile.setColor( COLOR_PAIR( ( int )tileConfig -> getValueMap()[ "color" ] -> getNumberValue() ) );
        tile.setAttribute( AssetManager::getInstance() -> getAttribute( tileConfig -> getValueMap()[ "attribute" ] -> getStringValue() ) );
        tile.setWindow( win );
        drawable -> insertTile( tile );
    }

    unsigned buildingSize = config -> getValueMap()[ "bricks" ] -> getValueArray().size();
    for( unsigned i = 0; i < buildingSize; i++ )
    {
        drawable -> insertBrick( ( int )config -> getValueMap()[ "bricks" ] -> getValueArray()[ i ] -> getNumberValue() );
    }
    building -> setDrawable( drawable );

    if( config -> getValueMap()[ "can_construct" ] -> getBooleanValue() == TRUE )
    {
        unsigned constructions = config -> getValueMap()[ "construction&cost" ] -> getValueArray().size();
        for( unsigned i = 0; i < constructions; i++ )
        {
            building ->addConstruction( config -> getValueMap()[ "construction&cost" ] -> getValueArray()[ i ] -> getValueMap()[ "name" ] -> getStringValue(),
                             ( int )config -> getValueMap()[ "construction&cost" ] -> getValueArray()[ i ] -> getValueMap()[ "cost" ] -> getNumberValue() );
        }
    }

    building -> setConfig( config );
    building -> setType( "building" );
    building -> setLevel( ( int )config -> getValueMap()[ "level" ] -> getNumberValue() );
	SceneManager::getInstance() -> addSceneNode( node );
	node -> setCollidable( building );

    return building;
}
