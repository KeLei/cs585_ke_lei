
#pragma once

#include "../../engine/asset/AssetManager.h"
#include "../scene/item/Item.h"
#include "../scene/item/Weapon.h"
#include "../scene/item/Potion.h"
#include "../scene/item/Armor.h"

class ItemFactory
{

public:

    static Item* getItem( std::string name );
};
