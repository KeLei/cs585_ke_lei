#include "CharacterFactory.h"

Character* CharacterFactory::spawnCharacter( std::string name, int x, int y, WINDOW* win )
{
    Character* actor;
    SceneNode* node = SceneNodeFactory::getInstance() -> getSceneNode(  ( float )x, ( float )y, 1.0f, 1.0f );
    if( name == "dwarf" )
    {
        actor = new Dwarf;
    }
    else
    {
        actor = new Character;
    }
	actor -> setSceneNode( node );
	ASCIIDrawable* drawable = new ASCIIDrawable;

    // Set drawable part of the actor
    JsonValue* config = AssetManager::getInstance() -> getConfig( name );
	drawable -> setColor( COLOR_PAIR( ( int )config->getValueMap()[ "color" ] -> getNumberValue() ) );
	drawable -> setAttribute( AssetManager::getInstance() -> getAttribute( config -> getValueMap()[ "attribute" ] -> getStringValue() ) );
	drawable -> setWindow( win );
	drawable -> setCharacter( config->getValueMap()[ "character" ] -> getStringValue()[ 0 ] );
	actor -> setDrawable( drawable );

	actor -> setType( config->getValueMap()[ "type" ] -> getStringValue() );
	actor -> setTargetType( config->getValueMap()[ "target_type" ] -> getStringValue() );
	actor -> setHealth( ( int )( config->getValueMap()[ "health" ] -> getNumberValue() ) );
	actor -> setMaxHealth( ( int )( config->getValueMap()[ "max_health" ] -> getNumberValue() ) );
	actor -> setAttack( ( int )( config->getValueMap()[ "attack" ] -> getNumberValue() ) );
	actor -> setAttackRange( ( int )( config->getValueMap()[ "attack_range" ] -> getNumberValue() ) );
	actor -> setSearchRange( ( int )( config->getValueMap()[ "search_range" ] -> getNumberValue() ) );
	actor -> setSpeed( config -> getValueMap()[ "speed" ] -> getNumberValue() );
    actor -> setGold( ( int )( config -> getValueMap()[ "gold" ] -> getNumberValue() ) );

    actor -> addTimer( "attack", ( int )( config->getValueMap()[ "attack_interval" ] -> getNumberValue() ) );

	char a = ( char )( Random::getInstance() -> getRandom( 26.0f ) + 'a' );
	char b = ( char )( Random::getInstance() -> getRandom( 26.0f ) + 'a' );
	std::string str;
	str += a;
	str += b;
	actor -> setName( str );

    CharacterController* controller;
    if( name == "dwarf" )
    {
        Dwarf* dwarf = dynamic_cast< Dwarf* >( actor );
        dwarf -> setExhaustion( ( unsigned )( config -> getValueMap()[ "exhaustion" ] -> getNumberValue() ) );
        dwarf -> setExhaustionThreshold( ( unsigned )( config -> getValueMap()[ "exhaustion_threshold" ] -> getNumberValue() ) );
        dwarf -> setThirst( ( unsigned )( config -> getValueMap()[ "thirst" ] -> getNumberValue() ) );
        dwarf -> setThirstThreshold( ( unsigned )( config -> getValueMap()[ "thirst_threshold" ] -> getNumberValue() ) );
        dwarf -> setStepCost( ( unsigned )( config -> getValueMap()[ "step_cost" ] -> getNumberValue() ) );
        dwarf -> setGoldThreshold( ( unsigned )( config -> getValueMap()[ "gold_threshold" ] -> getNumberValue() ) );

        dwarf -> addTimer( "drink", ( unsigned )( config -> getValueMap()[ "drink_time" ] -> getNumberValue() ) );
        dwarf -> addTimer( "sleep", ( unsigned )( config -> getValueMap()[ "sleep_time" ] -> getNumberValue() ) );
        dwarf -> addTimer( "thirst_timer", ( unsigned )( config -> getValueMap()[ "thirst_increase_time" ] -> getNumberValue() ) );
        dwarf -> addTimer( "exhaust_timer", ( unsigned )( config -> getValueMap()[ "exhaustion_increase_time" ] -> getNumberValue() ) );
    }

    controller = new CharacterController;
	controller -> setCharacter( actor );

    SceneManager::getInstance() -> addTickable( controller );
	SceneManager::getInstance() -> addSceneNode( node );
	node -> setCollidable( actor );

	return actor;
}
