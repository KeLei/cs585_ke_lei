#pragma once
#include "../../engine/asset/AssetManager.h"
#include "../../engine/random/Random.h"
#include "../../engine/scene/SceneNodeFactory.h"
#include "CharacterController.h"
#include "Character.h"
#include "Dwarf.h"

class CharacterFactory
{

public:

    static Character* spawnCharacter( std::string name, int x, int y, WINDOW* win );

};
